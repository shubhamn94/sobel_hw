## Running Sobel Filter Design over FPGA
* Clone FPGA Hardware design to local machine.
  git clone https://shubhamn94@bitbucket.org/shubhamn94/sobel_hw.git 

* Please check whether your FPGA has minimum configuration mentioned below so as to synthesize and implement the design successfully.

* Install riffa drivers to enable PCI Express link to FPGA. This link is required for the communication and data transfer between CPU and FPGA. More information will be available in README.md of riffa.
    * git clone https://github.com/KastnerRG/riffa.git 
    * cd riffa
    * git checkout devel/RIFFA/2.2.2

* Open Vivado and open project sobel_final.xpr. Run Synthesis, Implementation and Generate Bitstream. Open Hardware manager and dump the design to FPGA. 

* Run polymage/sandbox/video_demo/sobel_edge/pci_refresh.sh script to activate the PCIe link. 

## Resource usage of sobel design on VC709 
* Sobel filter Design is tested and verified on Xilinx Virtex-7 VC709 kit. Below is the utilization % post-implementation on VC709
    * LUT = 4.05%
    * LUTRAM = 0.3%
    * FF = 2.8%
    * BRAM = 3.4%
    * DSP = 0.22%
    * IO = 1.06%
    * GT = 11.11%
    * BUFG = 15.63%
    * MMCM = 5%
    * PCIe = 33.33%
