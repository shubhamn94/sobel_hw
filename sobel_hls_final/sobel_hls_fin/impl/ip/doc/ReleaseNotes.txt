# ==============================================================
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# ==============================================================

Family       : virtex7
Device       : xc7vx690t
Package      : -ffg1761
Speed Grade  : -2
Clock Period : 4.000 ns
