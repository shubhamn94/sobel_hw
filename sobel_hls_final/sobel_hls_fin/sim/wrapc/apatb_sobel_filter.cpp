// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

#define AP_INT_MAX_W 32678

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;


// [dump_struct_tree [build_nameSpaceTree] dumpedStructList] ---------->


// [dump_enumeration [get_enumeration_list]] ---------->


// wrapc file define: "INPUT_STREAM_V_V"
#define AUTOTB_TVIN_INPUT_STREAM_V_V  "../tv/cdatafile/c.sobel_filter.autotvin_INPUT_STREAM_V_V.dat"
#define WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V  "../tv/stream_size/stream_size_in_INPUT_STREAM_V_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V  "../tv/stream_size/stream_ingress_status_INPUT_STREAM_V_V.dat"
// wrapc file define: "OUTPUT_STREAM_V_V"
#define AUTOTB_TVOUT_OUTPUT_STREAM_V_V  "../tv/cdatafile/c.sobel_filter.autotvout_OUTPUT_STREAM_V_V.dat"
#define AUTOTB_TVIN_OUTPUT_STREAM_V_V  "../tv/cdatafile/c.sobel_filter.autotvin_OUTPUT_STREAM_V_V.dat"
#define WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V  "../tv/stream_size/stream_size_out_OUTPUT_STREAM_V_V.dat"
#define WRAPC_STREAM_EGRESS_STATUS_OUTPUT_STREAM_V_V  "../tv/stream_size/stream_egress_status_OUTPUT_STREAM_V_V.dat"

#define INTER_TCL  "../tv/cdatafile/ref.tcl"

// tvout file define: "OUTPUT_STREAM_V_V"
#define AUTOTB_TVOUT_PC_OUTPUT_STREAM_V_V  "../tv/rtldatafile/rtl.sobel_filter.autotvout_OUTPUT_STREAM_V_V.dat"

class INTER_TCL_FILE {
	public:
		INTER_TCL_FILE(const char* name) {
			mName = name;
			INPUT_STREAM_V_V_depth = 0;
			OUTPUT_STREAM_V_V_depth = 0;
			trans_num =0;
		}

		~INTER_TCL_FILE() {
			mFile.open(mName);
			if (!mFile.good()) {
				cout << "Failed to open file ref.tcl" << endl;
				exit (1);
			}
			string total_list = get_depth_list();
			mFile << "set depth_list {\n";
			mFile << total_list;
			mFile << "}\n";
			mFile << "set trans_num "<<trans_num<<endl;
			mFile.close();
		}

		string get_depth_list () {
			stringstream total_list;
			total_list << "{INPUT_STREAM_V_V " << INPUT_STREAM_V_V_depth << "}\n";
			total_list << "{OUTPUT_STREAM_V_V " << OUTPUT_STREAM_V_V_depth << "}\n";
			return total_list.str();
		}

		void set_num (int num , int* class_num) {
			(*class_num) = (*class_num) > num ? (*class_num) : num;
		}
	public:
		int INPUT_STREAM_V_V_depth;
		int OUTPUT_STREAM_V_V_depth;
		int trans_num;

	private:
		ofstream mFile;
		const char* mName;
};

extern void sobel_filter (
hls::stream<ap_uint<16> > (&INPUT_STREAM),
hls::stream<ap_uint<16> > (&OUTPUT_STREAM));

void AESL_WRAP_sobel_filter (
hls::stream<ap_uint<16> > (&INPUT_STREAM),
hls::stream<ap_uint<16> > (&OUTPUT_STREAM))
{
	refine_signal_handler();
	fstream wrapc_switch_file_token;
	wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
	int AESL_i;
	if (wrapc_switch_file_token.good())
	{
		CodeState = ENTER_WRAPC_PC;
		static unsigned AESL_transaction_pc = 0;
		string AESL_token;
		string AESL_num;
		static AESL_FILE_HANDLER aesl_fh;

		// pop stream input: "INPUT_STREAM"
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, AESL_token); // pop_size
			int aesl_tmp_1 = atoi(AESL_token.c_str());
			for (int i = 0; i < aesl_tmp_1; i++)
			{
				INPUT_STREAM.read();
			}
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, AESL_token); // [[/transaction]]
		}

		// define output stream variables: "OUTPUT_STREAM"
		std::vector<ap_uint<16> > aesl_tmp_3;
		int aesl_tmp_4;
		int aesl_tmp_5 = 0;

		// read output stream size: "OUTPUT_STREAM"
		aesl_fh.read(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, AESL_token); // pop_size
			aesl_tmp_4 = atoi(AESL_token.c_str());
			aesl_fh.read(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, AESL_token); // [[/transaction]]
		}

		// output port post check: "OUTPUT_STREAM_V_V"
		aesl_fh.read(AUTOTB_TVOUT_PC_OUTPUT_STREAM_V_V, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_OUTPUT_STREAM_V_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_OUTPUT_STREAM_V_V, AESL_token); // data

			std::vector<sc_bv<16> > OUTPUT_STREAM_V_V_pc_buffer;
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'OUTPUT_STREAM_V_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port 'OUTPUT_STREAM_V_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					OUTPUT_STREAM_V_V_pc_buffer.push_back(AESL_token.c_str());
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_OUTPUT_STREAM_V_V, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_OUTPUT_STREAM_V_V))
				{
					exit(1);
				}
			}

			// correct the buffer size the current transaction
			if (i != aesl_tmp_4)
			{
				aesl_tmp_4 = i;
			}

			if (aesl_tmp_4 > 0 && aesl_tmp_3.size() < aesl_tmp_4)
			{
				int aesl_tmp_3_size = aesl_tmp_3.size();

				for (int tmp_aesl_tmp_3 = 0; tmp_aesl_tmp_3 < aesl_tmp_4 - aesl_tmp_3_size; tmp_aesl_tmp_3++)
				{
					ap_uint<16> tmp;
					aesl_tmp_3.push_back(tmp);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: OUTPUT_STREAM_V_V
				{
					// bitslice(15, 0)
					// {
						// celement: OUTPUT_STREAM.V.V(15, 0)
						// {
							sc_lv<16>* OUTPUT_STREAM_V_V_lv0_0_0_1 = new sc_lv<16>[aesl_tmp_4 - aesl_tmp_5];
						// }
					// }

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: OUTPUT_STREAM.V.V(15, 0)
						{
							// carray: (aesl_tmp_5) => (aesl_tmp_4 - 1) @ (1)
							for (int i_0 = aesl_tmp_5; i_0 <= aesl_tmp_4 - 1; i_0 += 1)
							{
								if (&(aesl_tmp_3[0]) != NULL) // check the null address if the c port is array or others
								{
									OUTPUT_STREAM_V_V_lv0_0_0_1[hls_map_index].range(15, 0) = sc_bv<16>(OUTPUT_STREAM_V_V_pc_buffer[hls_map_index].range(15, 0));
									hls_map_index++;
								}
							}
						}
					}

					// bitslice(15, 0)
					{
						int hls_map_index = 0;
						// celement: OUTPUT_STREAM.V.V(15, 0)
						{
							// carray: (aesl_tmp_5) => (aesl_tmp_4 - 1) @ (1)
							for (int i_0 = aesl_tmp_5; i_0 <= aesl_tmp_4 - 1; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : aesl_tmp_3[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : aesl_tmp_3[0]
								// output_left_conversion : aesl_tmp_3[i_0]
								// output_type_conversion : (OUTPUT_STREAM_V_V_lv0_0_0_1[hls_map_index]).to_string(SC_BIN).c_str()
								if (&(aesl_tmp_3[0]) != NULL) // check the null address if the c port is array or others
								{
									aesl_tmp_3[i_0] = (OUTPUT_STREAM_V_V_lv0_0_0_1[hls_map_index]).to_string(SC_BIN).c_str();
									hls_map_index++;
								}
							}
						}
					}
				}
			}
		}

		// push back output stream: "OUTPUT_STREAM"
		for (int i = 0; i < aesl_tmp_4; i++)
		{
			OUTPUT_STREAM.write(aesl_tmp_3[i]);
		}

		AESL_transaction_pc++;
	}
	else
	{
		CodeState = ENTER_WRAPC;
		static unsigned AESL_transaction;

		static AESL_FILE_HANDLER aesl_fh;

		// "INPUT_STREAM_V_V"
		char* tvin_INPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_INPUT_STREAM_V_V);
		char* wrapc_stream_size_in_INPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V);
		char* wrapc_stream_ingress_status_INPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V);

		// "OUTPUT_STREAM_V_V"
		char* tvin_OUTPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_OUTPUT_STREAM_V_V);
		char* tvout_OUTPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_OUTPUT_STREAM_V_V);
		char* wrapc_stream_size_out_OUTPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V);
		char* wrapc_stream_egress_status_OUTPUT_STREAM_V_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_EGRESS_STATUS_OUTPUT_STREAM_V_V);

		CodeState = DUMP_INPUTS;
		static INTER_TCL_FILE tcl_file(INTER_TCL);
		int leading_zero;

		// dump stream tvin: "INPUT_STREAM"
		std::vector<ap_uint<16> > aesl_tmp_0;
		int aesl_tmp_1 = 0;
		while (!INPUT_STREAM.empty())
		{
			aesl_tmp_0.push_back(INPUT_STREAM.read());
			aesl_tmp_1++;
		}

		// dump stream tvin: "OUTPUT_STREAM"
		std::vector<ap_uint<16> > aesl_tmp_3;
		int aesl_tmp_4 = 0;
		while (!OUTPUT_STREAM.empty())
		{
			aesl_tmp_3.push_back(OUTPUT_STREAM.read());
			aesl_tmp_4++;
		}

		// push back input stream: "INPUT_STREAM"
		for (int i = 0; i < aesl_tmp_1; i++)
		{
			INPUT_STREAM.write(aesl_tmp_0[i]);
		}

		// push back input stream: "OUTPUT_STREAM"
		for (int i = 0; i < aesl_tmp_4; i++)
		{
			OUTPUT_STREAM.write(aesl_tmp_3[i]);
		}

// [call_c_dut] ---------->

		CodeState = CALL_C_DUT;
		sobel_filter(INPUT_STREAM, OUTPUT_STREAM);

		CodeState = DUMP_OUTPUTS;
		// record input size to tv3: "INPUT_STREAM"
		int aesl_tmp_2 = INPUT_STREAM.size();

		// pop output stream: "OUTPUT_STREAM"
		int aesl_tmp_5 = aesl_tmp_4;
		aesl_tmp_4 = 0;
     aesl_tmp_3.clear();
		while (!OUTPUT_STREAM.empty())
		{
			aesl_tmp_3.push_back(OUTPUT_STREAM.read());
			aesl_tmp_4++;
		}

		// [[transaction]]
		sprintf(tvin_INPUT_STREAM_V_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_INPUT_STREAM_V_V, tvin_INPUT_STREAM_V_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, tvin_INPUT_STREAM_V_V);

		sc_bv<16>* INPUT_STREAM_V_V_tvin_wrapc_buffer = new sc_bv<16>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: INPUT_STREAM_V_V
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: INPUT_STREAM.V.V(15, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : aesl_tmp_0[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : aesl_tmp_0[0]
						// regulate_c_name       : INPUT_STREAM_V_V
						// input_type_conversion : (aesl_tmp_0[i_0]).to_string(2).c_str()
						if (&(aesl_tmp_0[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> INPUT_STREAM_V_V_tmp_mem;
							INPUT_STREAM_V_V_tmp_mem = (aesl_tmp_0[i_0]).to_string(2).c_str();
							INPUT_STREAM_V_V_tvin_wrapc_buffer[hls_map_index].range(15, 0) = INPUT_STREAM_V_V_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_INPUT_STREAM_V_V, "%s\n", (INPUT_STREAM_V_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_INPUT_STREAM_V_V, tvin_INPUT_STREAM_V_V);
		}

		// dump stream ingress status to file
     if (aesl_tmp_1 > aesl_tmp_2)
     {
		sc_int<32> stream_ingress_size_INPUT_STREAM_V_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, stream_ingress_size_INPUT_STREAM_V_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_INPUT_STREAM_V_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, stream_ingress_size_INPUT_STREAM_V_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, "\n");
		}
     }
     else {
		    sc_int<32> stream_ingress_size_INPUT_STREAM_V_V = 0;
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, stream_ingress_size_INPUT_STREAM_V_V.to_string().c_str());
		    aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, "\n");
     }

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.INPUT_STREAM_V_V_depth);
		sprintf(tvin_INPUT_STREAM_V_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_INPUT_STREAM_V_V, tvin_INPUT_STREAM_V_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_INPUT_STREAM_V_V, tvin_INPUT_STREAM_V_V);

		// release memory allocation
		delete [] INPUT_STREAM_V_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_INPUT_STREAM_V_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, wrapc_stream_size_in_INPUT_STREAM_V_V);
		sprintf(wrapc_stream_size_in_INPUT_STREAM_V_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, wrapc_stream_size_in_INPUT_STREAM_V_V);
		sprintf(wrapc_stream_size_in_INPUT_STREAM_V_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_INPUT_STREAM_V_V, wrapc_stream_size_in_INPUT_STREAM_V_V);

		// [[transaction]]
		sprintf(tvout_OUTPUT_STREAM_V_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_OUTPUT_STREAM_V_V, tvout_OUTPUT_STREAM_V_V);

		sc_bv<16>* OUTPUT_STREAM_V_V_tvout_wrapc_buffer = new sc_bv<16>[aesl_tmp_4 - aesl_tmp_5];

		// RTL Name: OUTPUT_STREAM_V_V
		{
			// bitslice(15, 0)
			{
				int hls_map_index = 0;
				// celement: OUTPUT_STREAM.V.V(15, 0)
				{
					// carray: (aesl_tmp_5) => (aesl_tmp_4 - 1) @ (1)
					for (int i_0 = aesl_tmp_5; i_0 <= aesl_tmp_4 - 1; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : aesl_tmp_3[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : aesl_tmp_3[0]
						// regulate_c_name       : OUTPUT_STREAM_V_V
						// input_type_conversion : (aesl_tmp_3[i_0]).to_string(2).c_str()
						if (&(aesl_tmp_3[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<16> OUTPUT_STREAM_V_V_tmp_mem;
							OUTPUT_STREAM_V_V_tmp_mem = (aesl_tmp_3[i_0]).to_string(2).c_str();
							OUTPUT_STREAM_V_V_tvout_wrapc_buffer[hls_map_index].range(15, 0) = OUTPUT_STREAM_V_V_tmp_mem.range(15, 0);
                                 	       hls_map_index++;
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_4 - aesl_tmp_5; i++)
		{
			sprintf(tvout_OUTPUT_STREAM_V_V, "%s\n", (OUTPUT_STREAM_V_V_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_OUTPUT_STREAM_V_V, tvout_OUTPUT_STREAM_V_V);
		}

		tcl_file.set_num(aesl_tmp_4 - aesl_tmp_5, &tcl_file.OUTPUT_STREAM_V_V_depth);
		sprintf(tvout_OUTPUT_STREAM_V_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_OUTPUT_STREAM_V_V, tvout_OUTPUT_STREAM_V_V);

		// release memory allocation
		delete [] OUTPUT_STREAM_V_V_tvout_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_out_OUTPUT_STREAM_V_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, wrapc_stream_size_out_OUTPUT_STREAM_V_V);
		sprintf(wrapc_stream_size_out_OUTPUT_STREAM_V_V, "%d\n", aesl_tmp_4 - aesl_tmp_5);
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, wrapc_stream_size_out_OUTPUT_STREAM_V_V);
		sprintf(wrapc_stream_size_out_OUTPUT_STREAM_V_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_OUTPUT_STREAM_V_V, wrapc_stream_size_out_OUTPUT_STREAM_V_V);

		// push back output stream: "OUTPUT_STREAM"
		for (int i = 0; i < aesl_tmp_4; i++)
		{
			OUTPUT_STREAM.write(aesl_tmp_3[i]);
		}

		CodeState = DELETE_CHAR_BUFFERS;
		// release memory allocation: "INPUT_STREAM_V_V"
		delete [] tvin_INPUT_STREAM_V_V;
		delete [] wrapc_stream_size_in_INPUT_STREAM_V_V;
		// release memory allocation: "OUTPUT_STREAM_V_V"
		delete [] tvout_OUTPUT_STREAM_V_V;
		delete [] tvin_OUTPUT_STREAM_V_V;
		delete [] wrapc_stream_size_out_OUTPUT_STREAM_V_V;

		AESL_transaction++;

		tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
	}
}

