// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern void AESL_WRAP_sobel_filter (
hls::stream<ap_uint<16> > (&INPUT_STREAM),
hls::stream<ap_uint<16> > (&OUTPUT_STREAM));
