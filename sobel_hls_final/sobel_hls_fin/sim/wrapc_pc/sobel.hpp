#ifndef _TOP_H_
#define _TOP_H_

#include "hls_video.h"
#include <ap_fixed.h>
#include <ap_axi_sdata.h>

#define MAX_WIDTH  1920
#define MAX_HEIGHT 1080
#define ROWS 3
typedef ap_uint<16> int16;
typedef hls::stream<int16> Instream;
typedef hls::stream<int16> Outstream;
//typedef hls::LineBuffer<ROWS, COLS, int16> linbuff;
//typedef hls::LineBuffer<ROWS, COLS, float> linbuff_float;

void sobel_filter(Instream&, Outstream&);

#endif
