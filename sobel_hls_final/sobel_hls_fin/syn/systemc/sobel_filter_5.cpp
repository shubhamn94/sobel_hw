#include "sobel_filter.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void sobel_filter::thread_xor_ln330_1_fu_3393_p2() {
    xor_ln330_1_fu_3393_p2 = (or_ln330_1_fu_3389_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_2_fu_3551_p2() {
    xor_ln330_2_fu_3551_p2 = (or_ln330_2_fu_3547_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_3_fu_3709_p2() {
    xor_ln330_3_fu_3709_p2 = (or_ln330_3_fu_3705_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_4_fu_3867_p2() {
    xor_ln330_4_fu_3867_p2 = (or_ln330_4_fu_3863_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_5_fu_4025_p2() {
    xor_ln330_5_fu_4025_p2 = (or_ln330_5_fu_4021_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_6_fu_4274_p2() {
    xor_ln330_6_fu_4274_p2 = (or_ln330_6_fu_4270_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_7_fu_4436_p2() {
    xor_ln330_7_fu_4436_p2 = (or_ln330_7_fu_4432_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln330_fu_3235_p2() {
    xor_ln330_fu_3235_p2 = (or_ln330_fu_3231_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_1_fu_3423_p2() {
    xor_ln332_1_fu_3423_p2 = (or_ln332_1_fu_3418_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_2_fu_3581_p2() {
    xor_ln332_2_fu_3581_p2 = (or_ln332_2_fu_3576_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_3_fu_3739_p2() {
    xor_ln332_3_fu_3739_p2 = (or_ln332_3_fu_3734_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_4_fu_3897_p2() {
    xor_ln332_4_fu_3897_p2 = (or_ln332_4_fu_3892_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_5_fu_4055_p2() {
    xor_ln332_5_fu_4055_p2 = (or_ln332_5_fu_4050_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_6_fu_4304_p2() {
    xor_ln332_6_fu_4304_p2 = (or_ln332_6_fu_4299_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_7_fu_4466_p2() {
    xor_ln332_7_fu_4466_p2 = (or_ln332_7_fu_4461_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln332_fu_3265_p2() {
    xor_ln332_fu_3265_p2 = (or_ln332_fu_3260_p2.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_1_fu_4518_p2() {
    xor_ln333_1_fu_4518_p2 = (icmp_ln333_1_reg_6762.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_2_fu_4604_p2() {
    xor_ln333_2_fu_4604_p2 = (icmp_ln333_2_reg_6830.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_3_fu_4690_p2() {
    xor_ln333_3_fu_4690_p2 = (icmp_ln333_3_reg_6898.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_4_fu_4776_p2() {
    xor_ln333_4_fu_4776_p2 = (icmp_ln333_4_reg_6966.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_5_fu_4862_p2() {
    xor_ln333_5_fu_4862_p2 = (icmp_ln333_5_reg_7034.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_6_fu_4948_p2() {
    xor_ln333_6_fu_4948_p2 = (icmp_ln333_6_reg_7118.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_7_fu_5034_p2() {
    xor_ln333_7_fu_5034_p2 = (icmp_ln333_7_reg_7186.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln333_fu_4107_p2() {
    xor_ln333_fu_4107_p2 = (icmp_ln333_reg_6694.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_zext_ln1353_10_fu_1692_p1() {
    zext_ln1353_10_fu_1692_p1 = esl_zext<18,16>(reg_819.read());
}

void sobel_filter::thread_zext_ln1353_11_fu_2092_p1() {
    zext_ln1353_11_fu_2092_p1 = esl_zext<18,17>(shl_ln1352_s_fu_2084_p3.read());
}

void sobel_filter::thread_zext_ln1353_12_fu_2101_p1() {
    zext_ln1353_12_fu_2101_p1 = esl_zext<18,16>(LineBuffer_V_load_24_reg_5895.read());
}

void sobel_filter::thread_zext_ln1353_13_fu_1785_p1() {
    zext_ln1353_13_fu_1785_p1 = esl_zext<18,17>(shl_ln1352_11_fu_1773_p3.read());
}

void sobel_filter::thread_zext_ln1353_14_fu_1795_p1() {
    zext_ln1353_14_fu_1795_p1 = esl_zext<18,16>(LineBuffer_V_load_29_reg_5519.read());
}

void sobel_filter::thread_zext_ln1353_15_fu_2243_p1() {
    zext_ln1353_15_fu_2243_p1 = esl_zext<18,17>(shl_ln1352_13_fu_2235_p3.read());
}

void sobel_filter::thread_zext_ln1353_16_fu_2252_p1() {
    zext_ln1353_16_fu_2252_p1 = esl_zext<18,16>(reg_778.read());
}

void sobel_filter::thread_zext_ln1353_17_fu_1881_p1() {
    zext_ln1353_17_fu_1881_p1 = esl_zext<18,17>(shl_ln1352_15_fu_1870_p3.read());
}

void sobel_filter::thread_zext_ln1353_18_fu_1891_p1() {
    zext_ln1353_18_fu_1891_p1 = esl_zext<18,16>(LineBuffer_V_load_35_reg_5569.read());
}

void sobel_filter::thread_zext_ln1353_19_fu_2283_p1() {
    zext_ln1353_19_fu_2283_p1 = esl_zext<18,17>(shl_ln1352_17_fu_2275_p3.read());
}

void sobel_filter::thread_zext_ln1353_1_fu_1438_p1() {
    zext_ln1353_1_fu_1438_p1 = esl_zext<18,17>(shl_ln_fu_1426_p3.read());
}

void sobel_filter::thread_zext_ln1353_20_fu_2292_p1() {
    zext_ln1353_20_fu_2292_p1 = esl_zext<18,16>(reg_813.read());
}

void sobel_filter::thread_zext_ln1353_21_fu_2006_p1() {
    zext_ln1353_21_fu_2006_p1 = esl_zext<18,17>(shl_ln1352_19_fu_1996_p3.read());
}

void sobel_filter::thread_zext_ln1353_22_fu_2016_p1() {
    zext_ln1353_22_fu_2016_p1 = esl_zext<18,16>(LineBuffer_V_load_41_reg_5618.read());
}

void sobel_filter::thread_zext_ln1353_23_fu_2417_p1() {
    zext_ln1353_23_fu_2417_p1 = esl_zext<18,17>(shl_ln1352_21_fu_2409_p3.read());
}

void sobel_filter::thread_zext_ln1353_24_fu_2426_p1() {
    zext_ln1353_24_fu_2426_p1 = esl_zext<18,16>(reg_783.read());
}

void sobel_filter::thread_zext_ln1353_25_fu_2185_p1() {
    zext_ln1353_25_fu_2185_p1 = esl_zext<18,17>(shl_ln1352_23_fu_2175_p3.read());
}

void sobel_filter::thread_zext_ln1353_26_fu_2195_p1() {
    zext_ln1353_26_fu_2195_p1 = esl_zext<18,16>(LineBuffer_V_load_47_reg_5677.read());
}

void sobel_filter::thread_zext_ln1353_27_fu_2461_p1() {
    zext_ln1353_27_fu_2461_p1 = esl_zext<18,17>(shl_ln1352_25_fu_2453_p3.read());
}

void sobel_filter::thread_zext_ln1353_28_fu_2470_p1() {
    zext_ln1353_28_fu_2470_p1 = esl_zext<18,16>(reg_825.read());
}

void sobel_filter::thread_zext_ln1353_29_fu_2372_p1() {
    zext_ln1353_29_fu_2372_p1 = esl_zext<18,17>(shl_ln1352_27_fu_2362_p3.read());
}

void sobel_filter::thread_zext_ln1353_2_fu_1448_p1() {
    zext_ln1353_2_fu_1448_p1 = esl_zext<18,16>(reg_789.read());
}

void sobel_filter::thread_zext_ln1353_30_fu_2382_p1() {
    zext_ln1353_30_fu_2382_p1 = esl_zext<18,16>(LineBuffer_V_load_53_reg_5719.read());
}

void sobel_filter::thread_zext_ln1353_31_fu_2584_p1() {
    zext_ln1353_31_fu_2584_p1 = esl_zext<18,17>(shl_ln1352_29_fu_2576_p3.read());
}

void sobel_filter::thread_zext_ln1353_32_fu_2593_p1() {
    zext_ln1353_32_fu_2593_p1 = esl_zext<18,16>(reg_836.read());
}

void sobel_filter::thread_zext_ln1353_3_fu_1908_p1() {
    zext_ln1353_3_fu_1908_p1 = esl_zext<18,17>(shl_ln1352_2_fu_1900_p3.read());
}

void sobel_filter::thread_zext_ln1353_4_fu_1917_p1() {
    zext_ln1353_4_fu_1917_p1 = esl_zext<18,16>(LineBuffer_V_load_9_reg_5730.read());
}

void sobel_filter::thread_zext_ln1353_5_fu_1586_p1() {
    zext_ln1353_5_fu_1586_p1 = esl_zext<18,17>(shl_ln1352_4_fu_1574_p3.read());
}

void sobel_filter::thread_zext_ln1353_6_fu_1596_p1() {
    zext_ln1353_6_fu_1596_p1 = esl_zext<18,16>(reg_807.read());
}

void sobel_filter::thread_zext_ln1353_7_fu_2051_p1() {
    zext_ln1353_7_fu_2051_p1 = esl_zext<18,17>(shl_ln1352_6_fu_2043_p3.read());
}

void sobel_filter::thread_zext_ln1353_8_fu_2060_p1() {
    zext_ln1353_8_fu_2060_p1 = esl_zext<18,16>(reg_795.read());
}

void sobel_filter::thread_zext_ln1353_9_fu_1682_p1() {
    zext_ln1353_9_fu_1682_p1 = esl_zext<18,17>(shl_ln1352_8_fu_1670_p3.read());
}

void sobel_filter::thread_zext_ln1353_fu_866_p1() {
    zext_ln1353_fu_866_p1 = esl_zext<17,16>(reg_774.read());
}

void sobel_filter::thread_zext_ln1354_10_fu_2636_p1() {
    zext_ln1354_10_fu_2636_p1 = esl_zext<19,17>(shl_ln1352_7_fu_2628_p3.read());
}

void sobel_filter::thread_zext_ln1354_11_fu_1736_p1() {
    zext_ln1354_11_fu_1736_p1 = esl_zext<19,16>(LineBuffer_V_load_24_reg_5895.read());
}

void sobel_filter::thread_zext_ln1354_12_fu_1753_p1() {
    zext_ln1354_12_fu_1753_p1 = esl_zext<19,17>(shl_ln1352_9_fu_1745_p3.read());
}

void sobel_filter::thread_zext_ln1354_13_fu_1763_p1() {
    zext_ln1354_13_fu_1763_p1 = esl_zext<19,16>(reg_836.read());
}

void sobel_filter::thread_zext_ln1354_14_fu_2114_p1() {
    zext_ln1354_14_fu_2114_p1 = esl_zext<19,16>(reg_819.read());
}

void sobel_filter::thread_zext_ln1354_15_fu_2658_p1() {
    zext_ln1354_15_fu_2658_p1 = esl_zext<19,17>(shl_ln1352_10_fu_2650_p3.read());
}

void sobel_filter::thread_zext_ln1354_16_fu_1828_p1() {
    zext_ln1354_16_fu_1828_p1 = esl_zext<19,16>(reg_778.read());
}

void sobel_filter::thread_zext_ln1354_17_fu_1846_p1() {
    zext_ln1354_17_fu_1846_p1 = esl_zext<19,17>(shl_ln1352_12_fu_1838_p3.read());
}

void sobel_filter::thread_zext_ln1354_18_fu_1856_p1() {
    zext_ln1354_18_fu_1856_p1 = esl_zext<19,16>(LineBuffer_V_load_31_reg_5791.read());
}

void sobel_filter::thread_zext_ln1354_19_fu_2266_p1() {
    zext_ln1354_19_fu_2266_p1 = esl_zext<19,16>(LineBuffer_V_load_29_reg_5519.read());
}

void sobel_filter::thread_zext_ln1354_1_fu_1479_p1() {
    zext_ln1354_1_fu_1479_p1 = esl_zext<19,16>(LineBuffer_V_load_9_reg_5730.read());
}

void sobel_filter::thread_zext_ln1354_20_fu_2717_p1() {
    zext_ln1354_20_fu_2717_p1 = esl_zext<19,17>(shl_ln1352_14_fu_2709_p3.read());
}

void sobel_filter::thread_zext_ln1354_21_fu_1955_p1() {
    zext_ln1354_21_fu_1955_p1 = esl_zext<19,16>(reg_813.read());
}

void sobel_filter::thread_zext_ln1354_22_fu_1972_p1() {
    zext_ln1354_22_fu_1972_p1 = esl_zext<19,17>(shl_ln1352_16_fu_1965_p3.read());
}

void sobel_filter::thread_zext_ln1354_23_fu_1982_p1() {
    zext_ln1354_23_fu_1982_p1 = esl_zext<19,16>(LineBuffer_V_load_37_reg_5832.read());
}

void sobel_filter::thread_zext_ln1354_24_fu_2306_p1() {
    zext_ln1354_24_fu_2306_p1 = esl_zext<19,16>(LineBuffer_V_load_35_reg_5569.read());
}

void sobel_filter::thread_zext_ln1354_25_fu_2739_p1() {
    zext_ln1354_25_fu_2739_p1 = esl_zext<19,17>(shl_ln1352_18_fu_2731_p3.read());
}

void sobel_filter::thread_zext_ln1354_26_fu_2135_p1() {
    zext_ln1354_26_fu_2135_p1 = esl_zext<19,16>(reg_783.read());
}

void sobel_filter::thread_zext_ln1354_27_fu_2152_p1() {
    zext_ln1354_27_fu_2152_p1 = esl_zext<19,17>(shl_ln1352_20_fu_2145_p3.read());
}

void sobel_filter::thread_zext_ln1354_28_fu_2162_p1() {
    zext_ln1354_28_fu_2162_p1 = esl_zext<19,16>(LineBuffer_V_load_43_reg_5838.read());
}

void sobel_filter::thread_zext_ln1354_29_fu_2440_p1() {
    zext_ln1354_29_fu_2440_p1 = esl_zext<19,16>(LineBuffer_V_load_41_reg_5618.read());
}

void sobel_filter::thread_zext_ln1354_2_fu_1496_p1() {
    zext_ln1354_2_fu_1496_p1 = esl_zext<19,17>(shl_ln1352_1_fu_1488_p3.read());
}

void sobel_filter::thread_zext_ln1354_30_fu_2789_p1() {
    zext_ln1354_30_fu_2789_p1 = esl_zext<19,17>(shl_ln1352_22_fu_2781_p3.read());
}

void sobel_filter::thread_zext_ln1354_31_fu_2322_p1() {
    zext_ln1354_31_fu_2322_p1 = esl_zext<19,16>(reg_825.read());
}

void sobel_filter::thread_zext_ln1354_32_fu_2339_p1() {
    zext_ln1354_32_fu_2339_p1 = esl_zext<19,17>(shl_ln1352_24_fu_2332_p3.read());
}

void sobel_filter::thread_zext_ln1354_33_fu_2349_p1() {
    zext_ln1354_33_fu_2349_p1 = esl_zext<19,16>(LineBuffer_V_load_49_reg_5874.read());
}

void sobel_filter::thread_zext_ln1354_34_fu_2484_p1() {
    zext_ln1354_34_fu_2484_p1 = esl_zext<19,16>(LineBuffer_V_load_47_reg_5677.read());
}

void sobel_filter::thread_zext_ln1354_35_fu_2811_p1() {
    zext_ln1354_35_fu_2811_p1 = esl_zext<19,17>(shl_ln1352_26_fu_2803_p3.read());
}

void sobel_filter::thread_zext_ln1354_36_fu_2496_p1() {
    zext_ln1354_36_fu_2496_p1 = esl_zext<19,16>(reg_836.read());
}

void sobel_filter::thread_zext_ln1354_37_fu_2513_p1() {
    zext_ln1354_37_fu_2513_p1 = esl_zext<19,17>(shl_ln1352_28_fu_2506_p3.read());
}

void sobel_filter::thread_zext_ln1354_38_fu_2523_p1() {
    zext_ln1354_38_fu_2523_p1 = esl_zext<19,16>(LineBuffer_V_load_55_reg_5880.read());
}

void sobel_filter::thread_zext_ln1354_39_fu_2607_p1() {
    zext_ln1354_39_fu_2607_p1 = esl_zext<19,16>(LineBuffer_V_load_53_reg_5719.read());
}

void sobel_filter::thread_zext_ln1354_3_fu_1506_p1() {
    zext_ln1354_3_fu_1506_p1 = esl_zext<19,16>(reg_778.read());
}

void sobel_filter::thread_zext_ln1354_40_fu_2857_p1() {
    zext_ln1354_40_fu_2857_p1 = esl_zext<19,17>(shl_ln1352_30_fu_2849_p3.read());
}

void sobel_filter::thread_zext_ln1354_4_fu_1930_p1() {
    zext_ln1354_4_fu_1930_p1 = esl_zext<19,16>(reg_789.read());
}

void sobel_filter::thread_zext_ln1354_5_fu_2540_p1() {
    zext_ln1354_5_fu_2540_p1 = esl_zext<19,17>(shl_ln1352_3_fu_2532_p3.read());
}

void sobel_filter::thread_zext_ln1354_6_fu_1632_p1() {
    zext_ln1354_6_fu_1632_p1 = esl_zext<19,16>(reg_795.read());
}

void sobel_filter::thread_zext_ln1354_7_fu_1650_p1() {
    zext_ln1354_7_fu_1650_p1 = esl_zext<19,17>(shl_ln1352_5_fu_1642_p3.read());
}

void sobel_filter::thread_zext_ln1354_8_fu_1660_p1() {
    zext_ln1354_8_fu_1660_p1 = esl_zext<19,16>(reg_783.read());
}

void sobel_filter::thread_zext_ln1354_9_fu_2074_p1() {
    zext_ln1354_9_fu_2074_p1 = esl_zext<19,16>(reg_807.read());
}

void sobel_filter::thread_zext_ln1354_fu_891_p1() {
    zext_ln1354_fu_891_p1 = esl_zext<17,16>(tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_zext_ln215_10_fu_1825_p1() {
    zext_ln215_10_fu_1825_p1 = esl_zext<19,18>(add_ln1353_17_reg_5996.read());
}

void sobel_filter::thread_zext_ln215_11_fu_2262_p1() {
    zext_ln215_11_fu_2262_p1 = esl_zext<19,18>(add_ln1353_19_fu_2256_p2.read());
}

void sobel_filter::thread_zext_ln215_12_fu_1877_p1() {
    zext_ln215_12_fu_1877_p1 = esl_zext<18,16>(reg_831.read());
}

void sobel_filter::thread_zext_ln215_13_fu_1952_p1() {
    zext_ln215_13_fu_1952_p1 = esl_zext<19,18>(add_ln1353_22_reg_6046.read());
}

void sobel_filter::thread_zext_ln215_14_fu_2302_p1() {
    zext_ln215_14_fu_2302_p1 = esl_zext<19,18>(add_ln1353_24_fu_2296_p2.read());
}

void sobel_filter::thread_zext_ln215_15_fu_2003_p1() {
    zext_ln215_15_fu_2003_p1 = esl_zext<18,16>(LineBuffer_V_load_40_reg_5613.read());
}

void sobel_filter::thread_zext_ln215_16_fu_2132_p1() {
    zext_ln215_16_fu_2132_p1 = esl_zext<19,18>(add_ln1353_27_reg_6091.read());
}

void sobel_filter::thread_zext_ln215_17_fu_2436_p1() {
    zext_ln215_17_fu_2436_p1 = esl_zext<19,18>(add_ln1353_29_fu_2430_p2.read());
}

void sobel_filter::thread_zext_ln215_18_fu_2182_p1() {
    zext_ln215_18_fu_2182_p1 = esl_zext<18,16>(LineBuffer_V_load_46_reg_5672.read());
}

void sobel_filter::thread_zext_ln215_19_fu_2319_p1() {
    zext_ln215_19_fu_2319_p1 = esl_zext<19,18>(add_ln1353_32_reg_6151.read());
}

void sobel_filter::thread_zext_ln215_1_fu_1476_p1() {
    zext_ln215_1_fu_1476_p1 = esl_zext<19,18>(add_ln1353_2_reg_5756.read());
}

void sobel_filter::thread_zext_ln215_20_fu_2480_p1() {
    zext_ln215_20_fu_2480_p1 = esl_zext<19,18>(add_ln1353_34_fu_2474_p2.read());
}

void sobel_filter::thread_zext_ln215_21_fu_2369_p1() {
    zext_ln215_21_fu_2369_p1 = esl_zext<18,16>(LineBuffer_V_load_52_reg_5714.read());
}

void sobel_filter::thread_zext_ln215_22_fu_2493_p1() {
    zext_ln215_22_fu_2493_p1 = esl_zext<19,18>(add_ln1353_37_reg_6221.read());
}

void sobel_filter::thread_zext_ln215_23_fu_2603_p1() {
    zext_ln215_23_fu_2603_p1 = esl_zext<19,18>(add_ln1353_39_fu_2597_p2.read());
}

void sobel_filter::thread_zext_ln215_2_fu_1926_p1() {
    zext_ln215_2_fu_1926_p1 = esl_zext<19,18>(add_ln1353_4_fu_1920_p2.read());
}

void sobel_filter::thread_zext_ln215_3_fu_1582_p1() {
    zext_ln215_3_fu_1582_p1 = esl_zext<18,16>(reg_795.read());
}

void sobel_filter::thread_zext_ln215_4_fu_1629_p1() {
    zext_ln215_4_fu_1629_p1 = esl_zext<19,18>(add_ln1353_7_reg_5890.read());
}

void sobel_filter::thread_zext_ln215_5_fu_2070_p1() {
    zext_ln215_5_fu_2070_p1 = esl_zext<19,18>(add_ln1353_9_fu_2064_p2.read());
}

void sobel_filter::thread_zext_ln215_6_fu_1678_p1() {
    zext_ln215_6_fu_1678_p1 = esl_zext<18,16>(reg_813.read());
}

void sobel_filter::thread_zext_ln215_7_fu_1733_p1() {
    zext_ln215_7_fu_1733_p1 = esl_zext<19,18>(add_ln1353_12_reg_5941.read());
}

void sobel_filter::thread_zext_ln215_8_fu_2110_p1() {
    zext_ln215_8_fu_2110_p1 = esl_zext<19,18>(add_ln1353_14_fu_2104_p2.read());
}

void sobel_filter::thread_zext_ln215_9_fu_1781_p1() {
    zext_ln215_9_fu_1781_p1 = esl_zext<18,16>(reg_825.read());
}

void sobel_filter::thread_zext_ln215_fu_1434_p1() {
    zext_ln215_fu_1434_p1 = esl_zext<18,16>(reg_783.read());
}

void sobel_filter::thread_zext_ln314_1_fu_3303_p1() {
    zext_ln314_1_fu_3303_p1 = esl_zext<12,11>(p_Result_s_13_fu_3293_p4.read());
}

void sobel_filter::thread_zext_ln314_2_fu_3461_p1() {
    zext_ln314_2_fu_3461_p1 = esl_zext<12,11>(p_Result_2_fu_3451_p4.read());
}

void sobel_filter::thread_zext_ln314_3_fu_3619_p1() {
    zext_ln314_3_fu_3619_p1 = esl_zext<12,11>(p_Result_3_fu_3609_p4.read());
}

void sobel_filter::thread_zext_ln314_4_fu_3777_p1() {
    zext_ln314_4_fu_3777_p1 = esl_zext<12,11>(p_Result_4_fu_3767_p4.read());
}

void sobel_filter::thread_zext_ln314_5_fu_3935_p1() {
    zext_ln314_5_fu_3935_p1 = esl_zext<12,11>(p_Result_5_fu_3925_p4.read());
}

void sobel_filter::thread_zext_ln314_6_fu_4168_p1() {
    zext_ln314_6_fu_4168_p1 = esl_zext<12,11>(p_Result_6_fu_4158_p4.read());
}

void sobel_filter::thread_zext_ln314_7_fu_4342_p1() {
    zext_ln314_7_fu_4342_p1 = esl_zext<12,11>(p_Result_7_fu_4332_p4.read());
}

void sobel_filter::thread_zext_ln314_fu_3133_p1() {
    zext_ln314_fu_3133_p1 = esl_zext<12,11>(p_Result_s_fu_3123_p4.read());
}

void sobel_filter::thread_zext_ln334_1_fu_3375_p1() {
    zext_ln334_1_fu_3375_p1 = esl_zext<53,32>(sext_ln329_1_fu_3346_p1.read());
}

void sobel_filter::thread_zext_ln334_2_fu_3533_p1() {
    zext_ln334_2_fu_3533_p1 = esl_zext<53,32>(sext_ln329_2_fu_3504_p1.read());
}

void sobel_filter::thread_zext_ln334_3_fu_3691_p1() {
    zext_ln334_3_fu_3691_p1 = esl_zext<53,32>(sext_ln329_3_fu_3662_p1.read());
}

void sobel_filter::thread_zext_ln334_4_fu_3849_p1() {
    zext_ln334_4_fu_3849_p1 = esl_zext<53,32>(sext_ln329_4_fu_3820_p1.read());
}

void sobel_filter::thread_zext_ln334_5_fu_4007_p1() {
    zext_ln334_5_fu_4007_p1 = esl_zext<53,32>(sext_ln329_5_fu_3978_p1.read());
}

void sobel_filter::thread_zext_ln334_6_fu_4256_p1() {
    zext_ln334_6_fu_4256_p1 = esl_zext<53,32>(sext_ln329_6_fu_4227_p1.read());
}

void sobel_filter::thread_zext_ln334_7_fu_4418_p1() {
    zext_ln334_7_fu_4418_p1 = esl_zext<53,32>(sext_ln329_7_fu_4389_p1.read());
}

void sobel_filter::thread_zext_ln334_fu_3217_p1() {
    zext_ln334_fu_3217_p1 = esl_zext<53,32>(sext_ln329_fu_3188_p1.read());
}

void sobel_filter::thread_zext_ln544_10_fu_1304_p1() {
    zext_ln544_10_fu_1304_p1 = esl_zext<64,16>(add_ln1353_26_reg_5551.read());
}

void sobel_filter::thread_zext_ln544_11_fu_1340_p1() {
    zext_ln544_11_fu_1340_p1 = esl_zext<64,16>(add_ln1353_31_reg_5601.read());
}

void sobel_filter::thread_zext_ln544_12_fu_1386_p1() {
    zext_ln544_12_fu_1386_p1 = esl_zext<64,16>(add_ln1353_36_reg_5650.read());
}

void sobel_filter::thread_zext_ln544_1_fu_1064_p1() {
    zext_ln544_1_fu_1064_p1 = esl_zext<64,16>(or_ln700_reg_5274.read());
}

void sobel_filter::thread_zext_ln544_2_fu_1068_p1() {
    zext_ln544_2_fu_1068_p1 = esl_zext<64,16>(or_ln700_1_reg_5294.read());
}

void sobel_filter::thread_zext_ln544_3_fu_1101_p1() {
    zext_ln544_3_fu_1101_p1 = esl_zext<64,16>(ap_phi_mux_t_V_6_0_phi_fu_730_p4.read());
}

void sobel_filter::thread_zext_ln544_4_fu_1120_p1() {
    zext_ln544_4_fu_1120_p1 = esl_zext<64,16>(add_ln1353_1_reg_5359.read());
}

void sobel_filter::thread_zext_ln544_5_fu_1072_p1() {
    zext_ln544_5_fu_1072_p1 = esl_zext<64,16>(or_ln700_2_reg_5315.read());
}

void sobel_filter::thread_zext_ln544_6_fu_1156_p1() {
    zext_ln544_6_fu_1156_p1 = esl_zext<64,16>(add_ln1353_6_reg_5396.read());
}

void sobel_filter::thread_zext_ln544_7_fu_1192_p1() {
    zext_ln544_7_fu_1192_p1 = esl_zext<64,16>(add_ln1353_11_reg_5433.read());
}

void sobel_filter::thread_zext_ln544_8_fu_1228_p1() {
    zext_ln544_8_fu_1228_p1 = esl_zext<64,16>(add_ln1353_16_reg_5470.read());
}

void sobel_filter::thread_zext_ln544_9_fu_1264_p1() {
    zext_ln544_9_fu_1264_p1 = esl_zext<64,16>(add_ln1353_21_reg_5507.read());
}

void sobel_filter::thread_zext_ln544_fu_1059_p1() {
    zext_ln544_fu_1059_p1 = esl_zext<64,16>(t_V_2_0_reg_714.read());
}

void sobel_filter::thread_zext_ln887_1_fu_1082_p1() {
    zext_ln887_1_fu_1082_p1 = esl_zext<17,16>(ap_phi_mux_t_V_6_0_phi_fu_730_p4.read());
}

void sobel_filter::thread_zext_ln887_2_fu_1124_p1() {
    zext_ln887_2_fu_1124_p1 = esl_zext<17,16>(add_ln1353_1_reg_5359.read());
}

void sobel_filter::thread_zext_ln887_3_fu_1160_p1() {
    zext_ln887_3_fu_1160_p1 = esl_zext<17,16>(add_ln1353_6_reg_5396.read());
}

void sobel_filter::thread_zext_ln887_4_fu_1196_p1() {
    zext_ln887_4_fu_1196_p1 = esl_zext<17,16>(add_ln1353_11_reg_5433.read());
}

void sobel_filter::thread_zext_ln887_5_fu_1232_p1() {
    zext_ln887_5_fu_1232_p1 = esl_zext<17,16>(add_ln1353_16_reg_5470.read());
}

void sobel_filter::thread_zext_ln887_6_fu_1268_p1() {
    zext_ln887_6_fu_1268_p1 = esl_zext<17,16>(add_ln1353_21_reg_5507.read());
}

void sobel_filter::thread_zext_ln887_7_fu_1308_p1() {
    zext_ln887_7_fu_1308_p1 = esl_zext<17,16>(add_ln1353_26_reg_5551.read());
}

void sobel_filter::thread_zext_ln887_8_fu_1344_p1() {
    zext_ln887_8_fu_1344_p1 = esl_zext<17,16>(add_ln1353_31_reg_5601.read());
}

void sobel_filter::thread_zext_ln887_fu_876_p1() {
    zext_ln887_fu_876_p1 = esl_zext<17,16>(t_V_reg_668.read());
}

}

