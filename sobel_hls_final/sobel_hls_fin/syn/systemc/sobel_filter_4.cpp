#include "sobel_filter.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void sobel_filter::thread_INPUT_STREAM_V_V_0_ack_in() {
    INPUT_STREAM_V_V_0_ack_in = INPUT_STREAM_V_V_0_state.read()[1];
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_ack_out() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) && 
          esl_seteq<1,1,1>(INPUT_STREAM_V_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) && 
          esl_seteq<1,1,1>(INPUT_STREAM_V_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage4.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage4_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op213_read_state17.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage8_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op221_read_state18.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage9_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()) && 
          !(esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()))) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) && 
          esl_seteq<1,1,1>(INPUT_STREAM_V_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state3.read()) && 
          esl_seteq<1,1,1>(INPUT_STREAM_V_V_0_vld_out.read(), ap_const_logic_1)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state6.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_fu_880_p2.read()) && 
          !(esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_fu_880_p2.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()))) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op190_read_state14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage5_11001.read(), ap_const_boolean_0)))) {
        INPUT_STREAM_V_V_0_ack_out = ap_const_logic_1;
    } else {
        INPUT_STREAM_V_V_0_ack_out = ap_const_logic_0;
    }
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, INPUT_STREAM_V_V_0_sel.read())) {
        INPUT_STREAM_V_V_0_data_out = INPUT_STREAM_V_V_0_payload_B.read();
    } else {
        INPUT_STREAM_V_V_0_data_out = INPUT_STREAM_V_V_0_payload_A.read();
    }
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_load_A() {
    INPUT_STREAM_V_V_0_load_A = (INPUT_STREAM_V_V_0_state_cmp_full.read() & ~INPUT_STREAM_V_V_0_sel_wr.read());
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_load_B() {
    INPUT_STREAM_V_V_0_load_B = (INPUT_STREAM_V_V_0_sel_wr.read() & INPUT_STREAM_V_V_0_state_cmp_full.read());
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_sel() {
    INPUT_STREAM_V_V_0_sel = INPUT_STREAM_V_V_0_sel_rd.read();
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_state_cmp_full() {
    INPUT_STREAM_V_V_0_state_cmp_full =  (sc_logic) ((!INPUT_STREAM_V_V_0_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(INPUT_STREAM_V_V_0_state.read() != ap_const_lv2_1))[0];
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_vld_in() {
    INPUT_STREAM_V_V_0_vld_in = INPUT_STREAM_V_V_TVALID.read();
}

void sobel_filter::thread_INPUT_STREAM_V_V_0_vld_out() {
    INPUT_STREAM_V_V_0_vld_out = INPUT_STREAM_V_V_0_state.read()[0];
}

void sobel_filter::thread_INPUT_STREAM_V_V_TDATA_blk_n() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()) && 
          esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_1)) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state2.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state3.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state4.read()) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state5.read()) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state6.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_fu_880_p2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage4.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage4.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage5.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage8.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage9.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_3_reg_5320.read())))) {
        INPUT_STREAM_V_V_TDATA_blk_n = INPUT_STREAM_V_V_0_state.read()[0];
    } else {
        INPUT_STREAM_V_V_TDATA_blk_n = ap_const_logic_1;
    }
}

void sobel_filter::thread_INPUT_STREAM_V_V_TREADY() {
    INPUT_STREAM_V_V_TREADY = INPUT_STREAM_V_V_0_state.read()[1];
}

void sobel_filter::thread_LineBuffer_V_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage24.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage24.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_24_fu_2672_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage23.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_16_fu_2554_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage22.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_8_fu_2391_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage21.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_4_fu_2209_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage20.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_22_fu_2128_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage19.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_14_fu_1940_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage18.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_6_fu_1808_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage17.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_1_fu_1715_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_21_fu_1702_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_13_fu_1606_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage14.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_5_fu_1556_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage13.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_27_fu_1548_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage12.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_19_fu_1516_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage11.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_11_fu_1458_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage10.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_3_fu_1408_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage9.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage9.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln215_fu_1390_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage8.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage8.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_7_fu_1382_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage7.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_6_fu_1336_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage6.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_5_fu_1300_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage5.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_4_fu_1260_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage4.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_3_fu_1224_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage3.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_2_fu_1188_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage2.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_1_fu_1152_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage1.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (sext_ln544_fu_1116_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (zext_ln544_3_fu_1101_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage9.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 = LineBuffer_V_addr_8_reg_5309.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage8.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (zext_ln544_2_fu_1068_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage7.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (zext_ln544_1_fu_1064_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage6.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 =  (sc_lv<13>) (zext_ln544_fu_1059_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage5.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 = LineBuffer_V_addr_2_reg_5268.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage4.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0)))) {
        LineBuffer_V_address0 = LineBuffer_V_addr_17_reg_5324.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage3.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 = LineBuffer_V_addr_6_reg_5303.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage2.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 = LineBuffer_V_addr_3_reg_5283.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage1.read(), ap_const_boolean_0))) {
        LineBuffer_V_address0 = LineBuffer_V_addr_reg_5262.read();
    } else {
        LineBuffer_V_address0 = "XXXXXXXXXXXXX";
    }
}

void sobel_filter::thread_LineBuffer_V_address1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_32_fu_2753_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage24.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage24.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_28_fu_2676_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage23.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_20_fu_2558_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage22.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_12_fu_2395_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage21.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_30_fu_2358_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage20.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_26_fu_2171_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage19.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_18_fu_1948_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage18.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_10_fu_1812_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage17.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_29_fu_1804_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_25_fu_1706_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_17_fu_1610_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage14.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_9_fu_1560_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage13.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_31_fu_1552_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage12.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_23_fu_1520_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage11.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_15_fu_1462_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage10.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_7_fu_1412_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage9.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage9.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (sext_ln215_2_fu_1394_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage8.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage8.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_12_fu_1386_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage7.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_11_fu_1340_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage6.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_10_fu_1304_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage5.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_9_fu_1264_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage4.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_8_fu_1228_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage3.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_7_fu_1192_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage2.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_6_fu_1156_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage1.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_4_fu_1120_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage9.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 =  (sc_lv<13>) (zext_ln544_5_fu_1072_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage8.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_6_reg_5303.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage7.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_3_reg_5283.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage6.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_reg_5262.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage4.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0)))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_19_reg_5329.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage3.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_8_reg_5309.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage2.read(), ap_const_boolean_0)))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_5_reg_5289.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage1.read(), ap_const_boolean_0))) {
        LineBuffer_V_address1 = LineBuffer_V_addr_2_reg_5268.read();
    } else {
        LineBuffer_V_address1 = "XXXXXXXXXXXXX";
    }
}

void sobel_filter::thread_LineBuffer_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage11_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage16_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage21_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage22_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage23_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage24.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage24_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage17_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage19_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage20_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage15_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage4.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage4_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage8_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage9_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage4_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage5_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage5_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage6_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage18_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage10_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage12_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage7_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage8_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage9_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage13_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage14_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage6_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage7_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)))) {
        LineBuffer_V_ce0 = ap_const_logic_1;
    } else {
        LineBuffer_V_ce0 = ap_const_logic_0;
    }
}

void sobel_filter::thread_LineBuffer_V_ce1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage11_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage16_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage21_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage22_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage23_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage24.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage24_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage17_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage19_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage20_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage15_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage4.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage4_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage8_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage9_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage4_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage5_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage5_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage6_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage18_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage10_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage12_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage7_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage8.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage8_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage9_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage13_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage14_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage6_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage7_11001.read(), ap_const_boolean_0)))) {
        LineBuffer_V_ce1 = ap_const_logic_1;
    } else {
        LineBuffer_V_ce1 = ap_const_logic_0;
    }
}

void sobel_filter::thread_LineBuffer_V_d0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        LineBuffer_V_d0 = reg_825.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage8.read(), ap_const_boolean_0))) {
        LineBuffer_V_d0 = reg_807.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage7.read(), ap_const_boolean_0))) {
        LineBuffer_V_d0 = reg_789.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage6.read(), ap_const_boolean_0))) {
        LineBuffer_V_d0 = reg_778.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp0_stage9.read(), ap_const_boolean_0)))) {
        LineBuffer_V_d0 = reg_801.read();
    } else {
        LineBuffer_V_d0 =  (sc_lv<16>) ("XXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_LineBuffer_V_d1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        LineBuffer_V_d1 = reg_801.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage9.read(), ap_const_boolean_0))) {
        LineBuffer_V_d1 = reg_819.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage8.read(), ap_const_boolean_0))) {
        LineBuffer_V_d1 = reg_813.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage7.read(), ap_const_boolean_0))) {
        LineBuffer_V_d1 = reg_795.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage6.read(), ap_const_boolean_0))) {
        LineBuffer_V_d1 = reg_783.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
                esl_seteq<1,1,1>(ap_block_pp0_stage5.read(), ap_const_boolean_0))) {
        LineBuffer_V_d1 = INPUT_STREAM_V_V_0_data_out.read();
    } else {
        LineBuffer_V_d1 =  (sc_lv<16>) ("XXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_LineBuffer_V_we0() {
    if (((esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_3_reg_5320.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage5_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage6_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage7_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage8_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage9_11001.read(), ap_const_boolean_0)))) {
        LineBuffer_V_we0 = ap_const_logic_1;
    } else {
        LineBuffer_V_we0 = ap_const_logic_0;
    }
}

void sobel_filter::thread_LineBuffer_V_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage9.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_3_reg_5320.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage9_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_3_reg_5320.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage0_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage5.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage5_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage6.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage6_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage7_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage8.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
          esl_seteq<1,1,1>(ap_block_pp0_stage8_11001.read(), ap_const_boolean_0)))) {
        LineBuffer_V_we1 = ap_const_logic_1;
    } else {
        LineBuffer_V_we1 = ap_const_logic_0;
    }
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_ack_in() {
    OUTPUT_STREAM_V_V_1_ack_in = OUTPUT_STREAM_V_V_1_state.read()[1];
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_ack_out() {
    OUTPUT_STREAM_V_V_1_ack_out = OUTPUT_STREAM_V_V_TREADY.read();
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_data_in() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state97.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln87_fu_5180_p2.read()))) {
        OUTPUT_STREAM_V_V_1_data_in = ap_const_lv16_0;
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1450_write_state94.read()) && 
                esl_seteq<1,1,1>(ap_block_pp2_stage1_01001.read(), ap_const_boolean_0))) {
        OUTPUT_STREAM_V_V_1_data_in = tmp_V_7_reg_7349.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1445_write_state93.read()) && 
                esl_seteq<1,1,1>(ap_block_pp2_stage0_01001.read(), ap_const_boolean_0))) {
        OUTPUT_STREAM_V_V_1_data_in = reg_860.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage3.read()) && 
                esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1437_write_state92.read()) && 
                esl_seteq<1,1,1>(ap_block_pp2_stage3_01001.read(), ap_const_boolean_0))) {
        OUTPUT_STREAM_V_V_1_data_in = tmp_V_5_reg_7334.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
                esl_seteq<1,1,1>(ap_block_pp2_stage2_01001.read(), ap_const_boolean_0))) {
        OUTPUT_STREAM_V_V_1_data_in = result_lb_V_q0.read();
    } else {
        OUTPUT_STREAM_V_V_1_data_in =  (sc_lv<16>) ("XXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_data_out() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, OUTPUT_STREAM_V_V_1_sel.read())) {
        OUTPUT_STREAM_V_V_1_data_out = OUTPUT_STREAM_V_V_1_payload_B.read();
    } else {
        OUTPUT_STREAM_V_V_1_data_out = OUTPUT_STREAM_V_V_1_payload_A.read();
    }
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_load_A() {
    OUTPUT_STREAM_V_V_1_load_A = (OUTPUT_STREAM_V_V_1_state_cmp_full.read() & ~OUTPUT_STREAM_V_V_1_sel_wr.read());
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_load_B() {
    OUTPUT_STREAM_V_V_1_load_B = (OUTPUT_STREAM_V_V_1_sel_wr.read() & OUTPUT_STREAM_V_V_1_state_cmp_full.read());
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_sel() {
    OUTPUT_STREAM_V_V_1_sel = OUTPUT_STREAM_V_V_1_sel_rd.read();
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_state_cmp_full() {
    OUTPUT_STREAM_V_V_1_state_cmp_full =  (sc_logic) ((!OUTPUT_STREAM_V_V_1_state.read().is_01() || !ap_const_lv2_1.is_01())? sc_lv<1>(): sc_lv<1>(OUTPUT_STREAM_V_V_1_state.read() != ap_const_lv2_1))[0];
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_vld_in() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage3.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1437_write_state92.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage3_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1445_write_state93.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
          esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1450_write_state94.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage1_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state97.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln87_fu_5180_p2.read()) && 
          esl_seteq<1,1,1>(ap_block_state97_io.read(), ap_const_boolean_0)))) {
        OUTPUT_STREAM_V_V_1_vld_in = ap_const_logic_1;
    } else {
        OUTPUT_STREAM_V_V_1_vld_in = ap_const_logic_0;
    }
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_1_vld_out() {
    OUTPUT_STREAM_V_V_1_vld_out = OUTPUT_STREAM_V_V_1_state.read()[0];
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_TDATA() {
    OUTPUT_STREAM_V_V_TDATA = OUTPUT_STREAM_V_V_1_data_out.read();
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_TDATA_blk_n() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage2.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage3.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage3.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage3.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read())) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage1.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288_pp2_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297_pp2_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage1.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288_pp2_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297_pp2_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_3_reg_7325.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage2.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288_pp2_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297_pp2_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316_pp2_iter1_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_3_reg_7325_pp2_iter1_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state97.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln87_fu_5180_p2.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state98.read()))) {
        OUTPUT_STREAM_V_V_TDATA_blk_n = OUTPUT_STREAM_V_V_1_state.read()[1];
    } else {
        OUTPUT_STREAM_V_V_TDATA_blk_n = ap_const_logic_1;
    }
}

void sobel_filter::thread_OUTPUT_STREAM_V_V_TVALID() {
    OUTPUT_STREAM_V_V_TVALID = OUTPUT_STREAM_V_V_1_state.read()[0];
}

void sobel_filter::thread_add_ln1353_10_fu_1686_p2() {
    add_ln1353_10_fu_1686_p2 = (!zext_ln1353_9_fu_1682_p1.read().is_01() || !zext_ln215_6_fu_1678_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_9_fu_1682_p1.read()) + sc_biguint<18>(zext_ln215_6_fu_1678_p1.read()));
}

void sobel_filter::thread_add_ln1353_11_fu_1178_p2() {
    add_ln1353_11_fu_1178_p2 = (!ap_const_lv16_3.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_3) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_12_fu_1696_p2() {
    add_ln1353_12_fu_1696_p2 = (!add_ln1353_10_fu_1686_p2.read().is_01() || !zext_ln1353_10_fu_1692_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_10_fu_1686_p2.read()) + sc_biguint<18>(zext_ln1353_10_fu_1692_p1.read()));
}

void sobel_filter::thread_add_ln1353_13_fu_2096_p2() {
    add_ln1353_13_fu_2096_p2 = (!zext_ln1353_11_fu_2092_p1.read().is_01() || !zext_ln215_6_reg_5936.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_11_fu_2092_p1.read()) + sc_biguint<18>(zext_ln215_6_reg_5936.read()));
}

void sobel_filter::thread_add_ln1353_14_fu_2104_p2() {
    add_ln1353_14_fu_2104_p2 = (!add_ln1353_13_fu_2096_p2.read().is_01() || !zext_ln1353_12_fu_2101_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_13_fu_2096_p2.read()) + sc_biguint<18>(zext_ln1353_12_fu_2101_p1.read()));
}

void sobel_filter::thread_add_ln1353_15_fu_1789_p2() {
    add_ln1353_15_fu_1789_p2 = (!zext_ln1353_13_fu_1785_p1.read().is_01() || !zext_ln215_9_fu_1781_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_13_fu_1785_p1.read()) + sc_biguint<18>(zext_ln215_9_fu_1781_p1.read()));
}

void sobel_filter::thread_add_ln1353_16_fu_1214_p2() {
    add_ln1353_16_fu_1214_p2 = (!ap_const_lv16_4.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_4) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_17_fu_1798_p2() {
    add_ln1353_17_fu_1798_p2 = (!add_ln1353_15_fu_1789_p2.read().is_01() || !zext_ln1353_14_fu_1795_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_15_fu_1789_p2.read()) + sc_biguint<18>(zext_ln1353_14_fu_1795_p1.read()));
}

void sobel_filter::thread_add_ln1353_18_fu_2247_p2() {
    add_ln1353_18_fu_2247_p2 = (!zext_ln1353_15_fu_2243_p1.read().is_01() || !zext_ln215_9_reg_5991.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_15_fu_2243_p1.read()) + sc_biguint<18>(zext_ln215_9_reg_5991.read()));
}

void sobel_filter::thread_add_ln1353_19_fu_2256_p2() {
    add_ln1353_19_fu_2256_p2 = (!add_ln1353_18_fu_2247_p2.read().is_01() || !zext_ln1353_16_fu_2252_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_18_fu_2247_p2.read()) + sc_biguint<18>(zext_ln1353_16_fu_2252_p1.read()));
}

void sobel_filter::thread_add_ln1353_1_fu_1106_p2() {
    add_ln1353_1_fu_1106_p2 = (!ap_const_lv16_1.is_01() || !ap_phi_mux_t_V_6_0_phi_fu_730_p4.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_1) + sc_biguint<16>(ap_phi_mux_t_V_6_0_phi_fu_730_p4.read()));
}

void sobel_filter::thread_add_ln1353_20_fu_1885_p2() {
    add_ln1353_20_fu_1885_p2 = (!zext_ln1353_17_fu_1881_p1.read().is_01() || !zext_ln215_12_fu_1877_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_17_fu_1881_p1.read()) + sc_biguint<18>(zext_ln215_12_fu_1877_p1.read()));
}

void sobel_filter::thread_add_ln1353_21_fu_1250_p2() {
    add_ln1353_21_fu_1250_p2 = (!ap_const_lv16_5.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_5) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_22_fu_1894_p2() {
    add_ln1353_22_fu_1894_p2 = (!add_ln1353_20_fu_1885_p2.read().is_01() || !zext_ln1353_18_fu_1891_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_20_fu_1885_p2.read()) + sc_biguint<18>(zext_ln1353_18_fu_1891_p1.read()));
}

void sobel_filter::thread_add_ln1353_23_fu_2287_p2() {
    add_ln1353_23_fu_2287_p2 = (!zext_ln1353_19_fu_2283_p1.read().is_01() || !zext_ln215_12_reg_6041.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_19_fu_2283_p1.read()) + sc_biguint<18>(zext_ln215_12_reg_6041.read()));
}

void sobel_filter::thread_add_ln1353_24_fu_2296_p2() {
    add_ln1353_24_fu_2296_p2 = (!add_ln1353_23_fu_2287_p2.read().is_01() || !zext_ln1353_20_fu_2292_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_23_fu_2287_p2.read()) + sc_biguint<18>(zext_ln1353_20_fu_2292_p1.read()));
}

void sobel_filter::thread_add_ln1353_25_fu_2010_p2() {
    add_ln1353_25_fu_2010_p2 = (!zext_ln1353_21_fu_2006_p1.read().is_01() || !zext_ln215_15_fu_2003_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_21_fu_2006_p1.read()) + sc_biguint<18>(zext_ln215_15_fu_2003_p1.read()));
}

void sobel_filter::thread_add_ln1353_26_fu_1286_p2() {
    add_ln1353_26_fu_1286_p2 = (!ap_const_lv16_6.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_6) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_27_fu_2019_p2() {
    add_ln1353_27_fu_2019_p2 = (!add_ln1353_25_fu_2010_p2.read().is_01() || !zext_ln1353_22_fu_2016_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_25_fu_2010_p2.read()) + sc_biguint<18>(zext_ln1353_22_fu_2016_p1.read()));
}

void sobel_filter::thread_add_ln1353_28_fu_2421_p2() {
    add_ln1353_28_fu_2421_p2 = (!zext_ln1353_23_fu_2417_p1.read().is_01() || !zext_ln215_15_reg_6086.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_23_fu_2417_p1.read()) + sc_biguint<18>(zext_ln215_15_reg_6086.read()));
}

void sobel_filter::thread_add_ln1353_29_fu_2430_p2() {
    add_ln1353_29_fu_2430_p2 = (!add_ln1353_28_fu_2421_p2.read().is_01() || !zext_ln1353_24_fu_2426_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_28_fu_2421_p2.read()) + sc_biguint<18>(zext_ln1353_24_fu_2426_p1.read()));
}

void sobel_filter::thread_add_ln1353_2_fu_1452_p2() {
    add_ln1353_2_fu_1452_p2 = (!add_ln1353_fu_1442_p2.read().is_01() || !zext_ln1353_2_fu_1448_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_fu_1442_p2.read()) + sc_biguint<18>(zext_ln1353_2_fu_1448_p1.read()));
}

void sobel_filter::thread_add_ln1353_30_fu_2189_p2() {
    add_ln1353_30_fu_2189_p2 = (!zext_ln1353_25_fu_2185_p1.read().is_01() || !zext_ln215_18_fu_2182_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_25_fu_2185_p1.read()) + sc_biguint<18>(zext_ln215_18_fu_2182_p1.read()));
}

void sobel_filter::thread_add_ln1353_31_fu_1326_p2() {
    add_ln1353_31_fu_1326_p2 = (!ap_const_lv16_7.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_7) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_32_fu_2198_p2() {
    add_ln1353_32_fu_2198_p2 = (!add_ln1353_30_fu_2189_p2.read().is_01() || !zext_ln1353_26_fu_2195_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_30_fu_2189_p2.read()) + sc_biguint<18>(zext_ln1353_26_fu_2195_p1.read()));
}

void sobel_filter::thread_add_ln1353_33_fu_2465_p2() {
    add_ln1353_33_fu_2465_p2 = (!zext_ln1353_27_fu_2461_p1.read().is_01() || !zext_ln215_18_reg_6146.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_27_fu_2461_p1.read()) + sc_biguint<18>(zext_ln215_18_reg_6146.read()));
}

void sobel_filter::thread_add_ln1353_34_fu_2474_p2() {
    add_ln1353_34_fu_2474_p2 = (!add_ln1353_33_fu_2465_p2.read().is_01() || !zext_ln1353_28_fu_2470_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_33_fu_2465_p2.read()) + sc_biguint<18>(zext_ln1353_28_fu_2470_p1.read()));
}

void sobel_filter::thread_add_ln1353_35_fu_2376_p2() {
    add_ln1353_35_fu_2376_p2 = (!zext_ln1353_29_fu_2372_p1.read().is_01() || !zext_ln215_21_fu_2369_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_29_fu_2372_p1.read()) + sc_biguint<18>(zext_ln215_21_fu_2369_p1.read()));
}

void sobel_filter::thread_add_ln1353_36_fu_1362_p2() {
    add_ln1353_36_fu_1362_p2 = (!ap_const_lv16_8.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_8) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_37_fu_2385_p2() {
    add_ln1353_37_fu_2385_p2 = (!add_ln1353_35_fu_2376_p2.read().is_01() || !zext_ln1353_30_fu_2382_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_35_fu_2376_p2.read()) + sc_biguint<18>(zext_ln1353_30_fu_2382_p1.read()));
}

void sobel_filter::thread_add_ln1353_38_fu_2588_p2() {
    add_ln1353_38_fu_2588_p2 = (!zext_ln1353_31_fu_2584_p1.read().is_01() || !zext_ln215_21_reg_6216.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_31_fu_2584_p1.read()) + sc_biguint<18>(zext_ln215_21_reg_6216.read()));
}

void sobel_filter::thread_add_ln1353_39_fu_2597_p2() {
    add_ln1353_39_fu_2597_p2 = (!add_ln1353_38_fu_2588_p2.read().is_01() || !zext_ln1353_32_fu_2593_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_38_fu_2588_p2.read()) + sc_biguint<18>(zext_ln1353_32_fu_2593_p1.read()));
}

void sobel_filter::thread_add_ln1353_3_fu_1912_p2() {
    add_ln1353_3_fu_1912_p2 = (!zext_ln1353_3_fu_1908_p1.read().is_01() || !zext_ln215_reg_5751.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_3_fu_1908_p1.read()) + sc_biguint<18>(zext_ln215_reg_5751.read()));
}

void sobel_filter::thread_add_ln1353_4_fu_1920_p2() {
    add_ln1353_4_fu_1920_p2 = (!add_ln1353_3_fu_1912_p2.read().is_01() || !zext_ln1353_4_fu_1917_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_3_fu_1912_p2.read()) + sc_biguint<18>(zext_ln1353_4_fu_1917_p1.read()));
}

void sobel_filter::thread_add_ln1353_5_fu_1590_p2() {
    add_ln1353_5_fu_1590_p2 = (!zext_ln1353_5_fu_1586_p1.read().is_01() || !zext_ln215_3_fu_1582_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_5_fu_1586_p1.read()) + sc_biguint<18>(zext_ln215_3_fu_1582_p1.read()));
}

void sobel_filter::thread_add_ln1353_6_fu_1142_p2() {
    add_ln1353_6_fu_1142_p2 = (!ap_const_lv16_2.is_01() || !t_V_6_0_reg_726.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_2) + sc_biguint<16>(t_V_6_0_reg_726.read()));
}

void sobel_filter::thread_add_ln1353_7_fu_1600_p2() {
    add_ln1353_7_fu_1600_p2 = (!add_ln1353_5_fu_1590_p2.read().is_01() || !zext_ln1353_6_fu_1596_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_5_fu_1590_p2.read()) + sc_biguint<18>(zext_ln1353_6_fu_1596_p1.read()));
}

void sobel_filter::thread_add_ln1353_8_fu_2055_p2() {
    add_ln1353_8_fu_2055_p2 = (!zext_ln1353_7_fu_2051_p1.read().is_01() || !zext_ln215_3_reg_5885.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_7_fu_2051_p1.read()) + sc_biguint<18>(zext_ln215_3_reg_5885.read()));
}

void sobel_filter::thread_add_ln1353_9_fu_2064_p2() {
    add_ln1353_9_fu_2064_p2 = (!add_ln1353_8_fu_2055_p2.read().is_01() || !zext_ln1353_8_fu_2060_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(add_ln1353_8_fu_2055_p2.read()) + sc_biguint<18>(zext_ln1353_8_fu_2060_p1.read()));
}

void sobel_filter::thread_add_ln1353_fu_1442_p2() {
    add_ln1353_fu_1442_p2 = (!zext_ln1353_1_fu_1438_p1.read().is_01() || !zext_ln215_fu_1434_p1.read().is_01())? sc_lv<18>(): (sc_biguint<18>(zext_ln1353_1_fu_1438_p1.read()) + sc_biguint<18>(zext_ln215_fu_1434_p1.read()));
}

void sobel_filter::thread_add_ln1354_1_fu_1132_p2() {
    add_ln1354_1_fu_1132_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_2_fu_1124_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_2_fu_1124_p1.read()));
}

void sobel_filter::thread_add_ln1354_2_fu_1168_p2() {
    add_ln1354_2_fu_1168_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_3_fu_1160_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_3_fu_1160_p1.read()));
}

void sobel_filter::thread_add_ln1354_3_fu_1204_p2() {
    add_ln1354_3_fu_1204_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_4_fu_1196_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_4_fu_1196_p1.read()));
}

void sobel_filter::thread_add_ln1354_4_fu_1240_p2() {
    add_ln1354_4_fu_1240_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_5_fu_1232_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_5_fu_1232_p1.read()));
}

void sobel_filter::thread_add_ln1354_5_fu_1276_p2() {
    add_ln1354_5_fu_1276_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_6_fu_1268_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_6_fu_1268_p1.read()));
}

void sobel_filter::thread_add_ln1354_6_fu_1316_p2() {
    add_ln1354_6_fu_1316_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_7_fu_1308_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_7_fu_1308_p1.read()));
}

void sobel_filter::thread_add_ln1354_7_fu_1352_p2() {
    add_ln1354_7_fu_1352_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_8_fu_1344_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_8_fu_1344_p1.read()));
}

void sobel_filter::thread_add_ln1354_fu_1091_p2() {
    add_ln1354_fu_1091_p2 = (!ap_const_lv17_1FFFF.is_01() || !zext_ln887_1_fu_1082_p1.read().is_01())? sc_lv<17>(): (sc_bigint<17>(ap_const_lv17_1FFFF) + sc_biguint<17>(zext_ln887_1_fu_1082_p1.read()));
}

void sobel_filter::thread_add_ln162_1_fu_5110_p2() {
    add_ln162_1_fu_5110_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln162_1_fu_5106_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln162_1_fu_5106_p1.read()));
}

void sobel_filter::thread_add_ln162_2_fu_5139_p2() {
    add_ln162_2_fu_5139_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln162_2_fu_5135_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln162_2_fu_5135_p1.read()));
}

void sobel_filter::thread_add_ln162_3_fu_5160_p2() {
    add_ln162_3_fu_5160_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln162_3_fu_5156_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln162_3_fu_5156_p1.read()));
}

void sobel_filter::thread_add_ln162_fu_5089_p2() {
    add_ln162_fu_5089_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln162_fu_5085_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln162_fu_5085_p1.read()));
}

void sobel_filter::thread_add_ln215_10_fu_1728_p2() {
    add_ln215_10_fu_1728_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_5_reg_5427.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_5_reg_5427.read()));
}

void sobel_filter::thread_add_ln215_11_fu_1416_p2() {
    add_ln215_11_fu_1416_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_6_reg_5439.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_6_reg_5439.read()));
}

void sobel_filter::thread_add_ln215_12_fu_2230_p2() {
    add_ln215_12_fu_2230_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_6_reg_5439.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_6_reg_5439.read()));
}

void sobel_filter::thread_add_ln215_13_fu_1564_p2() {
    add_ln215_13_fu_1564_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_7_reg_5464.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_7_reg_5464.read()));
}

void sobel_filter::thread_add_ln215_14_fu_1820_p2() {
    add_ln215_14_fu_1820_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_7_reg_5464.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_7_reg_5464.read()));
}

void sobel_filter::thread_add_ln215_15_fu_1421_p2() {
    add_ln215_15_fu_1421_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_8_reg_5476.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_8_reg_5476.read()));
}

void sobel_filter::thread_add_ln215_16_fu_2399_p2() {
    add_ln215_16_fu_2399_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_8_reg_5476.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_8_reg_5476.read()));
}

void sobel_filter::thread_add_ln215_17_fu_1569_p2() {
    add_ln215_17_fu_1569_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_9_reg_5501.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_9_reg_5501.read()));
}

void sobel_filter::thread_add_ln215_18_fu_1865_p2() {
    add_ln215_18_fu_1865_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_9_reg_5501.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_9_reg_5501.read()));
}

void sobel_filter::thread_add_ln215_19_fu_1466_p2() {
    add_ln215_19_fu_1466_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_10_reg_5513.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_10_reg_5513.read()));
}

void sobel_filter::thread_add_ln215_1_fu_1624_p2() {
    add_ln215_1_fu_1624_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_reg_5348.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_reg_5348.read()));
}

void sobel_filter::thread_add_ln215_20_fu_2404_p2() {
    add_ln215_20_fu_2404_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_10_reg_5513.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_10_reg_5513.read()));
}

void sobel_filter::thread_add_ln215_21_fu_1614_p2() {
    add_ln215_21_fu_1614_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_11_reg_5545.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_11_reg_5545.read()));
}

void sobel_filter::thread_add_ln215_22_fu_1991_p2() {
    add_ln215_22_fu_1991_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_11_reg_5545.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_11_reg_5545.read()));
}

void sobel_filter::thread_add_ln215_23_fu_1471_p2() {
    add_ln215_23_fu_1471_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_12_reg_5557.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_12_reg_5557.read()));
}

void sobel_filter::thread_add_ln215_24_fu_2562_p2() {
    add_ln215_24_fu_2562_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_12_reg_5557.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_12_reg_5557.read()));
}

void sobel_filter::thread_add_ln215_25_fu_1619_p2() {
    add_ln215_25_fu_1619_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_13_reg_5595.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_13_reg_5595.read()));
}

void sobel_filter::thread_add_ln215_26_fu_2025_p2() {
    add_ln215_26_fu_2025_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_13_reg_5595.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_13_reg_5595.read()));
}

void sobel_filter::thread_add_ln215_27_fu_1524_p2() {
    add_ln215_27_fu_1524_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_14_reg_5607.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_14_reg_5607.read()));
}

void sobel_filter::thread_add_ln215_28_fu_2567_p2() {
    add_ln215_28_fu_2567_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_14_reg_5607.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_14_reg_5607.read()));
}

void sobel_filter::thread_add_ln215_29_fu_1710_p2() {
    add_ln215_29_fu_1710_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_15_reg_5644.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_15_reg_5644.read()));
}

void sobel_filter::thread_add_ln215_2_fu_1377_p2() {
    add_ln215_2_fu_1377_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_1_reg_5563.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_1_reg_5563.read()));
}

void sobel_filter::thread_add_ln215_30_fu_2204_p2() {
    add_ln215_30_fu_2204_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_15_reg_5644.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_15_reg_5644.read()));
}

void sobel_filter::thread_add_ln215_31_fu_1529_p2() {
    add_ln215_31_fu_1529_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_16_reg_5656.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_16_reg_5656.read()));
}

void sobel_filter::thread_add_ln215_32_fu_2680_p2() {
    add_ln215_32_fu_2680_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_16_reg_5656.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_16_reg_5656.read()));
}

void sobel_filter::thread_add_ln215_3_fu_1398_p2() {
    add_ln215_3_fu_1398_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_2_reg_5365.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_2_reg_5365.read()));
}

void sobel_filter::thread_add_ln215_4_fu_2030_p2() {
    add_ln215_4_fu_2030_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_2_reg_5365.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_2_reg_5365.read()));
}

void sobel_filter::thread_add_ln215_5_fu_1538_p2() {
    add_ln215_5_fu_1538_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_3_reg_5390.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_3_reg_5390.read()));
}

void sobel_filter::thread_add_ln215_6_fu_1719_p2() {
    add_ln215_6_fu_1719_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_3_reg_5390.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_3_reg_5390.read()));
}

void sobel_filter::thread_add_ln215_7_fu_1403_p2() {
    add_ln215_7_fu_1403_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_4_reg_5402.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_4_reg_5402.read()));
}

void sobel_filter::thread_add_ln215_8_fu_2225_p2() {
    add_ln215_8_fu_2225_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_4_reg_5402.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_4_reg_5402.read()));
}

void sobel_filter::thread_add_ln215_9_fu_1543_p2() {
    add_ln215_9_fu_1543_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_5_reg_5427.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_5_reg_5427.read()));
}

void sobel_filter::thread_add_ln215_fu_1372_p2() {
    add_ln215_fu_1372_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln215_reg_5348.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln215_reg_5348.read()));
}

void sobel_filter::thread_add_ln321_1_fu_926_p2() {
    add_ln321_1_fu_926_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln321_fu_922_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln321_fu_922_p1.read()));
}

void sobel_filter::thread_add_ln321_2_fu_937_p2() {
    add_ln321_2_fu_937_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln321_fu_922_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln321_fu_922_p1.read()));
}

void sobel_filter::thread_add_ln321_3_fu_963_p2() {
    add_ln321_3_fu_963_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln321_1_fu_959_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln321_1_fu_959_p1.read()));
}

void sobel_filter::thread_add_ln321_4_fu_974_p2() {
    add_ln321_4_fu_974_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln321_1_fu_959_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln321_1_fu_959_p1.read()));
}

void sobel_filter::thread_add_ln321_5_fu_1000_p2() {
    add_ln321_5_fu_1000_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln321_2_fu_996_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln321_2_fu_996_p1.read()));
}

void sobel_filter::thread_add_ln321_6_fu_1011_p2() {
    add_ln321_6_fu_1011_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln321_2_fu_996_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln321_2_fu_996_p1.read()));
}

void sobel_filter::thread_add_ln321_7_fu_1037_p2() {
    add_ln321_7_fu_1037_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln321_3_fu_1033_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln321_3_fu_1033_p1.read()));
}

void sobel_filter::thread_add_ln321_8_fu_1048_p2() {
    add_ln321_8_fu_1048_p2 = (!ap_const_lv14_F50.is_01() || !trunc_ln321_3_fu_1033_p1.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_F50) + sc_biguint<14>(trunc_ln321_3_fu_1033_p1.read()));
}

void sobel_filter::thread_add_ln321_fu_4200_p2() {
    add_ln321_fu_4200_p2 = (!ap_const_lv14_7A8.is_01() || !trunc_ln215_1_reg_5563_pp1_iter1_reg.read().is_01())? sc_lv<14>(): (sc_biguint<14>(ap_const_lv14_7A8) + sc_biguint<14>(trunc_ln215_1_reg_5563_pp1_iter1_reg.read()));
}

void sobel_filter::thread_add_ln700_1_fu_5174_p2() {
    add_ln700_1_fu_5174_p2 = (!ap_const_lv16_4.is_01() || !t_V_3_0_reg_738.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_4) + sc_biguint<16>(t_V_3_0_reg_738.read()));
}

void sobel_filter::thread_add_ln700_fu_1076_p2() {
    add_ln700_fu_1076_p2 = (!ap_const_lv16_4.is_01() || !t_V_2_0_reg_714.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_4) + sc_biguint<16>(t_V_2_0_reg_714.read()));
}

void sobel_filter::thread_and_ln330_1_fu_4541_p2() {
    and_ln330_1_fu_4541_p2 = (icmp_ln330_1_reg_6750.read() & xor_ln326_1_fu_4536_p2.read());
}

void sobel_filter::thread_and_ln330_2_fu_4627_p2() {
    and_ln330_2_fu_4627_p2 = (icmp_ln330_2_reg_6818.read() & xor_ln326_2_fu_4622_p2.read());
}

void sobel_filter::thread_and_ln330_3_fu_4713_p2() {
    and_ln330_3_fu_4713_p2 = (icmp_ln330_3_reg_6886.read() & xor_ln326_3_fu_4708_p2.read());
}

void sobel_filter::thread_and_ln330_4_fu_4799_p2() {
    and_ln330_4_fu_4799_p2 = (icmp_ln330_4_reg_6954.read() & xor_ln326_4_fu_4794_p2.read());
}

void sobel_filter::thread_and_ln330_5_fu_4885_p2() {
    and_ln330_5_fu_4885_p2 = (icmp_ln330_5_reg_7022.read() & xor_ln326_5_fu_4880_p2.read());
}

void sobel_filter::thread_and_ln330_6_fu_4971_p2() {
    and_ln330_6_fu_4971_p2 = (icmp_ln330_6_reg_7096.read() & xor_ln326_6_fu_4966_p2.read());
}

void sobel_filter::thread_and_ln330_7_fu_5057_p2() {
    and_ln330_7_fu_5057_p2 = (icmp_ln330_7_reg_7174.read() & xor_ln326_7_fu_5052_p2.read());
}

void sobel_filter::thread_and_ln330_fu_4130_p2() {
    and_ln330_fu_4130_p2 = (icmp_ln330_reg_6677.read() & xor_ln326_fu_4125_p2.read());
}

void sobel_filter::thread_and_ln332_1_fu_3399_p2() {
    and_ln332_1_fu_3399_p2 = (icmp_ln332_1_reg_6756.read() & xor_ln330_1_fu_3393_p2.read());
}

void sobel_filter::thread_and_ln332_2_fu_3557_p2() {
    and_ln332_2_fu_3557_p2 = (icmp_ln332_2_reg_6824.read() & xor_ln330_2_fu_3551_p2.read());
}

void sobel_filter::thread_and_ln332_3_fu_3715_p2() {
    and_ln332_3_fu_3715_p2 = (icmp_ln332_3_reg_6892.read() & xor_ln330_3_fu_3709_p2.read());
}

void sobel_filter::thread_and_ln332_4_fu_3873_p2() {
    and_ln332_4_fu_3873_p2 = (icmp_ln332_4_reg_6960.read() & xor_ln330_4_fu_3867_p2.read());
}

void sobel_filter::thread_and_ln332_5_fu_4031_p2() {
    and_ln332_5_fu_4031_p2 = (icmp_ln332_5_reg_7028.read() & xor_ln330_5_fu_4025_p2.read());
}

void sobel_filter::thread_and_ln332_6_fu_4280_p2() {
    and_ln332_6_fu_4280_p2 = (icmp_ln332_6_reg_7102.read() & xor_ln330_6_fu_4274_p2.read());
}

void sobel_filter::thread_and_ln332_7_fu_4442_p2() {
    and_ln332_7_fu_4442_p2 = (icmp_ln332_7_reg_7180.read() & xor_ln330_7_fu_4436_p2.read());
}

void sobel_filter::thread_and_ln332_fu_3241_p2() {
    and_ln332_fu_3241_p2 = (icmp_ln332_reg_6683.read() & xor_ln330_fu_3235_p2.read());
}

void sobel_filter::thread_and_ln333_10_fu_4036_p2() {
    and_ln333_10_fu_4036_p2 = (and_ln332_5_fu_4031_p2.read() & icmp_ln333_5_fu_3981_p2.read());
}

void sobel_filter::thread_and_ln333_11_fu_4867_p2() {
    and_ln333_11_fu_4867_p2 = (and_ln332_5_reg_7044.read() & xor_ln333_5_fu_4862_p2.read());
}

void sobel_filter::thread_and_ln333_12_fu_4285_p2() {
    and_ln333_12_fu_4285_p2 = (and_ln332_6_fu_4280_p2.read() & icmp_ln333_6_fu_4230_p2.read());
}

void sobel_filter::thread_and_ln333_13_fu_4953_p2() {
    and_ln333_13_fu_4953_p2 = (and_ln332_6_reg_7128.read() & xor_ln333_6_fu_4948_p2.read());
}

void sobel_filter::thread_and_ln333_14_fu_4447_p2() {
    and_ln333_14_fu_4447_p2 = (and_ln332_7_fu_4442_p2.read() & icmp_ln333_7_fu_4392_p2.read());
}

void sobel_filter::thread_and_ln333_15_fu_5039_p2() {
    and_ln333_15_fu_5039_p2 = (and_ln332_7_reg_7196.read() & xor_ln333_7_fu_5034_p2.read());
}

void sobel_filter::thread_and_ln333_1_fu_4112_p2() {
    and_ln333_1_fu_4112_p2 = (and_ln332_reg_6704.read() & xor_ln333_fu_4107_p2.read());
}

void sobel_filter::thread_and_ln333_2_fu_3404_p2() {
    and_ln333_2_fu_3404_p2 = (and_ln332_1_fu_3399_p2.read() & icmp_ln333_1_fu_3349_p2.read());
}

void sobel_filter::thread_and_ln333_3_fu_4523_p2() {
    and_ln333_3_fu_4523_p2 = (and_ln332_1_reg_6772.read() & xor_ln333_1_fu_4518_p2.read());
}

void sobel_filter::thread_and_ln333_4_fu_3562_p2() {
    and_ln333_4_fu_3562_p2 = (and_ln332_2_fu_3557_p2.read() & icmp_ln333_2_fu_3507_p2.read());
}

void sobel_filter::thread_and_ln333_5_fu_4609_p2() {
    and_ln333_5_fu_4609_p2 = (and_ln332_2_reg_6840.read() & xor_ln333_2_fu_4604_p2.read());
}

void sobel_filter::thread_and_ln333_6_fu_3720_p2() {
    and_ln333_6_fu_3720_p2 = (and_ln332_3_fu_3715_p2.read() & icmp_ln333_3_fu_3665_p2.read());
}

void sobel_filter::thread_and_ln333_7_fu_4695_p2() {
    and_ln333_7_fu_4695_p2 = (and_ln332_3_reg_6908.read() & xor_ln333_3_fu_4690_p2.read());
}

void sobel_filter::thread_and_ln333_8_fu_3878_p2() {
    and_ln333_8_fu_3878_p2 = (and_ln332_4_fu_3873_p2.read() & icmp_ln333_4_fu_3823_p2.read());
}

void sobel_filter::thread_and_ln333_9_fu_4781_p2() {
    and_ln333_9_fu_4781_p2 = (and_ln332_4_reg_6976.read() & xor_ln333_4_fu_4776_p2.read());
}

void sobel_filter::thread_and_ln333_fu_3246_p2() {
    and_ln333_fu_3246_p2 = (and_ln332_fu_3241_p2.read() & icmp_ln333_fu_3191_p2.read());
}

void sobel_filter::thread_and_ln343_1_fu_3429_p2() {
    and_ln343_1_fu_3429_p2 = (icmp_ln343_1_fu_3369_p2.read() & xor_ln332_1_fu_3423_p2.read());
}

void sobel_filter::thread_and_ln343_2_fu_3587_p2() {
    and_ln343_2_fu_3587_p2 = (icmp_ln343_2_fu_3527_p2.read() & xor_ln332_2_fu_3581_p2.read());
}

void sobel_filter::thread_and_ln343_3_fu_3745_p2() {
    and_ln343_3_fu_3745_p2 = (icmp_ln343_3_fu_3685_p2.read() & xor_ln332_3_fu_3739_p2.read());
}

void sobel_filter::thread_and_ln343_4_fu_3903_p2() {
    and_ln343_4_fu_3903_p2 = (icmp_ln343_4_fu_3843_p2.read() & xor_ln332_4_fu_3897_p2.read());
}

void sobel_filter::thread_and_ln343_5_fu_4061_p2() {
    and_ln343_5_fu_4061_p2 = (icmp_ln343_5_fu_4001_p2.read() & xor_ln332_5_fu_4055_p2.read());
}

void sobel_filter::thread_and_ln343_6_fu_4310_p2() {
    and_ln343_6_fu_4310_p2 = (icmp_ln343_6_fu_4250_p2.read() & xor_ln332_6_fu_4304_p2.read());
}

void sobel_filter::thread_and_ln343_7_fu_4472_p2() {
    and_ln343_7_fu_4472_p2 = (icmp_ln343_7_fu_4412_p2.read() & xor_ln332_7_fu_4466_p2.read());
}

void sobel_filter::thread_and_ln343_fu_3271_p2() {
    and_ln343_fu_3271_p2 = (icmp_ln343_fu_3211_p2.read() & xor_ln332_fu_3265_p2.read());
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage0() {
    ap_CS_fsm_pp0_stage0 = ap_CS_fsm.read()[8];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage1() {
    ap_CS_fsm_pp0_stage1 = ap_CS_fsm.read()[9];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage2() {
    ap_CS_fsm_pp0_stage2 = ap_CS_fsm.read()[10];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage3() {
    ap_CS_fsm_pp0_stage3 = ap_CS_fsm.read()[11];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage4() {
    ap_CS_fsm_pp0_stage4 = ap_CS_fsm.read()[12];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage5() {
    ap_CS_fsm_pp0_stage5 = ap_CS_fsm.read()[13];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage6() {
    ap_CS_fsm_pp0_stage6 = ap_CS_fsm.read()[14];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage7() {
    ap_CS_fsm_pp0_stage7 = ap_CS_fsm.read()[15];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage8() {
    ap_CS_fsm_pp0_stage8 = ap_CS_fsm.read()[16];
}

void sobel_filter::thread_ap_CS_fsm_pp0_stage9() {
    ap_CS_fsm_pp0_stage9 = ap_CS_fsm.read()[17];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage0() {
    ap_CS_fsm_pp1_stage0 = ap_CS_fsm.read()[19];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage1() {
    ap_CS_fsm_pp1_stage1 = ap_CS_fsm.read()[20];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage10() {
    ap_CS_fsm_pp1_stage10 = ap_CS_fsm.read()[29];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage11() {
    ap_CS_fsm_pp1_stage11 = ap_CS_fsm.read()[30];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage12() {
    ap_CS_fsm_pp1_stage12 = ap_CS_fsm.read()[31];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage13() {
    ap_CS_fsm_pp1_stage13 = ap_CS_fsm.read()[32];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage14() {
    ap_CS_fsm_pp1_stage14 = ap_CS_fsm.read()[33];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage15() {
    ap_CS_fsm_pp1_stage15 = ap_CS_fsm.read()[34];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage16() {
    ap_CS_fsm_pp1_stage16 = ap_CS_fsm.read()[35];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage17() {
    ap_CS_fsm_pp1_stage17 = ap_CS_fsm.read()[36];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage18() {
    ap_CS_fsm_pp1_stage18 = ap_CS_fsm.read()[37];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage19() {
    ap_CS_fsm_pp1_stage19 = ap_CS_fsm.read()[38];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage2() {
    ap_CS_fsm_pp1_stage2 = ap_CS_fsm.read()[21];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage20() {
    ap_CS_fsm_pp1_stage20 = ap_CS_fsm.read()[39];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage21() {
    ap_CS_fsm_pp1_stage21 = ap_CS_fsm.read()[40];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage22() {
    ap_CS_fsm_pp1_stage22 = ap_CS_fsm.read()[41];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage23() {
    ap_CS_fsm_pp1_stage23 = ap_CS_fsm.read()[42];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage24() {
    ap_CS_fsm_pp1_stage24 = ap_CS_fsm.read()[43];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage3() {
    ap_CS_fsm_pp1_stage3 = ap_CS_fsm.read()[22];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage4() {
    ap_CS_fsm_pp1_stage4 = ap_CS_fsm.read()[23];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage5() {
    ap_CS_fsm_pp1_stage5 = ap_CS_fsm.read()[24];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage6() {
    ap_CS_fsm_pp1_stage6 = ap_CS_fsm.read()[25];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage7() {
    ap_CS_fsm_pp1_stage7 = ap_CS_fsm.read()[26];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage8() {
    ap_CS_fsm_pp1_stage8 = ap_CS_fsm.read()[27];
}

void sobel_filter::thread_ap_CS_fsm_pp1_stage9() {
    ap_CS_fsm_pp1_stage9 = ap_CS_fsm.read()[28];
}

void sobel_filter::thread_ap_CS_fsm_pp2_stage0() {
    ap_CS_fsm_pp2_stage0 = ap_CS_fsm.read()[45];
}

void sobel_filter::thread_ap_CS_fsm_pp2_stage1() {
    ap_CS_fsm_pp2_stage1 = ap_CS_fsm.read()[46];
}

void sobel_filter::thread_ap_CS_fsm_pp2_stage2() {
    ap_CS_fsm_pp2_stage2 = ap_CS_fsm.read()[47];
}

void sobel_filter::thread_ap_CS_fsm_pp2_stage3() {
    ap_CS_fsm_pp2_stage3 = ap_CS_fsm.read()[48];
}

void sobel_filter::thread_ap_CS_fsm_state1() {
    ap_CS_fsm_state1 = ap_CS_fsm.read()[0];
}

void sobel_filter::thread_ap_CS_fsm_state2() {
    ap_CS_fsm_state2 = ap_CS_fsm.read()[1];
}

void sobel_filter::thread_ap_CS_fsm_state20() {
    ap_CS_fsm_state20 = ap_CS_fsm.read()[18];
}

void sobel_filter::thread_ap_CS_fsm_state3() {
    ap_CS_fsm_state3 = ap_CS_fsm.read()[2];
}

void sobel_filter::thread_ap_CS_fsm_state4() {
    ap_CS_fsm_state4 = ap_CS_fsm.read()[3];
}

void sobel_filter::thread_ap_CS_fsm_state5() {
    ap_CS_fsm_state5 = ap_CS_fsm.read()[4];
}

void sobel_filter::thread_ap_CS_fsm_state6() {
    ap_CS_fsm_state6 = ap_CS_fsm.read()[5];
}

void sobel_filter::thread_ap_CS_fsm_state7() {
    ap_CS_fsm_state7 = ap_CS_fsm.read()[6];
}

void sobel_filter::thread_ap_CS_fsm_state8() {
    ap_CS_fsm_state8 = ap_CS_fsm.read()[7];
}

void sobel_filter::thread_ap_CS_fsm_state88() {
    ap_CS_fsm_state88 = ap_CS_fsm.read()[44];
}

void sobel_filter::thread_ap_CS_fsm_state96() {
    ap_CS_fsm_state96 = ap_CS_fsm.read()[49];
}

void sobel_filter::thread_ap_CS_fsm_state97() {
    ap_CS_fsm_state97 = ap_CS_fsm.read()[50];
}

void sobel_filter::thread_ap_CS_fsm_state98() {
    ap_CS_fsm_state98 = ap_CS_fsm.read()[51];
}

void sobel_filter::thread_ap_block_pp0_stage0() {
    ap_block_pp0_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage0_11001() {
    ap_block_pp0_stage0_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage0_subdone() {
    ap_block_pp0_stage0_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage1() {
    ap_block_pp0_stage1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage1_11001() {
    ap_block_pp0_stage1_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage1_subdone() {
    ap_block_pp0_stage1_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage2() {
    ap_block_pp0_stage2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage2_11001() {
    ap_block_pp0_stage2_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage2_subdone() {
    ap_block_pp0_stage2_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage3() {
    ap_block_pp0_stage3 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage3_11001() {
    ap_block_pp0_stage3_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage3_subdone() {
    ap_block_pp0_stage3_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage4() {
    ap_block_pp0_stage4 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage4_11001() {
    ap_block_pp0_stage4_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()));
}

void sobel_filter::thread_ap_block_pp0_stage4_subdone() {
    ap_block_pp0_stage4_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()));
}

void sobel_filter::thread_ap_block_pp0_stage5() {
    ap_block_pp0_stage5 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage5_11001() {
    ap_block_pp0_stage5_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op190_read_state14.read()));
}

void sobel_filter::thread_ap_block_pp0_stage5_subdone() {
    ap_block_pp0_stage5_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op190_read_state14.read()));
}

void sobel_filter::thread_ap_block_pp0_stage6() {
    ap_block_pp0_stage6 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage6_11001() {
    ap_block_pp0_stage6_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage6_subdone() {
    ap_block_pp0_stage6_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage7() {
    ap_block_pp0_stage7 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage7_11001() {
    ap_block_pp0_stage7_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage7_subdone() {
    ap_block_pp0_stage7_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage8() {
    ap_block_pp0_stage8 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage8_11001() {
    ap_block_pp0_stage8_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op213_read_state17.read()));
}

void sobel_filter::thread_ap_block_pp0_stage8_subdone() {
    ap_block_pp0_stage8_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op213_read_state17.read()));
}

void sobel_filter::thread_ap_block_pp0_stage9() {
    ap_block_pp0_stage9 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp0_stage9_11001() {
    ap_block_pp0_stage9_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op221_read_state18.read()));
}

void sobel_filter::thread_ap_block_pp0_stage9_subdone() {
    ap_block_pp0_stage9_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter0.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op221_read_state18.read()));
}

void sobel_filter::thread_ap_block_pp1_stage0() {
    ap_block_pp1_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage0_11001() {
    ap_block_pp1_stage0_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage0_subdone() {
    ap_block_pp1_stage0_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage1() {
    ap_block_pp1_stage1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage10() {
    ap_block_pp1_stage10 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage10_11001() {
    ap_block_pp1_stage10_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage10_subdone() {
    ap_block_pp1_stage10_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage11() {
    ap_block_pp1_stage11 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage11_11001() {
    ap_block_pp1_stage11_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage11_subdone() {
    ap_block_pp1_stage11_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage12() {
    ap_block_pp1_stage12 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage12_11001() {
    ap_block_pp1_stage12_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage12_subdone() {
    ap_block_pp1_stage12_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage13() {
    ap_block_pp1_stage13 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage13_11001() {
    ap_block_pp1_stage13_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage13_subdone() {
    ap_block_pp1_stage13_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage14() {
    ap_block_pp1_stage14 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage14_11001() {
    ap_block_pp1_stage14_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage14_subdone() {
    ap_block_pp1_stage14_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage15() {
    ap_block_pp1_stage15 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage15_11001() {
    ap_block_pp1_stage15_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage15_subdone() {
    ap_block_pp1_stage15_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage16() {
    ap_block_pp1_stage16 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage16_11001() {
    ap_block_pp1_stage16_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage16_subdone() {
    ap_block_pp1_stage16_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage17() {
    ap_block_pp1_stage17 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage17_11001() {
    ap_block_pp1_stage17_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage17_subdone() {
    ap_block_pp1_stage17_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage18() {
    ap_block_pp1_stage18 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage18_11001() {
    ap_block_pp1_stage18_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage18_subdone() {
    ap_block_pp1_stage18_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage19() {
    ap_block_pp1_stage19 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage19_11001() {
    ap_block_pp1_stage19_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage19_subdone() {
    ap_block_pp1_stage19_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage1_11001() {
    ap_block_pp1_stage1_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage1_subdone() {
    ap_block_pp1_stage1_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage2() {
    ap_block_pp1_stage2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage20() {
    ap_block_pp1_stage20 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage20_11001() {
    ap_block_pp1_stage20_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage20_subdone() {
    ap_block_pp1_stage20_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage21() {
    ap_block_pp1_stage21 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage21_11001() {
    ap_block_pp1_stage21_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage21_subdone() {
    ap_block_pp1_stage21_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage22() {
    ap_block_pp1_stage22 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage22_11001() {
    ap_block_pp1_stage22_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage22_subdone() {
    ap_block_pp1_stage22_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage23() {
    ap_block_pp1_stage23 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage23_11001() {
    ap_block_pp1_stage23_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage23_subdone() {
    ap_block_pp1_stage23_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage24() {
    ap_block_pp1_stage24 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage24_11001() {
    ap_block_pp1_stage24_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage24_subdone() {
    ap_block_pp1_stage24_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage2_11001() {
    ap_block_pp1_stage2_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage2_subdone() {
    ap_block_pp1_stage2_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage3() {
    ap_block_pp1_stage3 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage3_11001() {
    ap_block_pp1_stage3_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage3_subdone() {
    ap_block_pp1_stage3_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage4() {
    ap_block_pp1_stage4 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage4_11001() {
    ap_block_pp1_stage4_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage4_subdone() {
    ap_block_pp1_stage4_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage5() {
    ap_block_pp1_stage5 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage5_11001() {
    ap_block_pp1_stage5_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage5_subdone() {
    ap_block_pp1_stage5_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage6() {
    ap_block_pp1_stage6 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage6_11001() {
    ap_block_pp1_stage6_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage6_subdone() {
    ap_block_pp1_stage6_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage7() {
    ap_block_pp1_stage7 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage7_11001() {
    ap_block_pp1_stage7_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage7_subdone() {
    ap_block_pp1_stage7_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage8() {
    ap_block_pp1_stage8 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage8_11001() {
    ap_block_pp1_stage8_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage8_subdone() {
    ap_block_pp1_stage8_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage9() {
    ap_block_pp1_stage9 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage9_11001() {
    ap_block_pp1_stage9_11001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp1_stage9_subdone() {
    ap_block_pp1_stage9_subdone = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage0() {
    ap_block_pp2_stage0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage0_01001() {
    ap_block_pp2_stage0_01001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage0_11001() {
    ap_block_pp2_stage0_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state93_io.read()));
}

void sobel_filter::thread_ap_block_pp2_stage0_subdone() {
    ap_block_pp2_stage0_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state93_io.read()));
}

void sobel_filter::thread_ap_block_pp2_stage1() {
    ap_block_pp2_stage1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage1_01001() {
    ap_block_pp2_stage1_01001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage1_11001() {
    ap_block_pp2_stage1_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state94_io.read()));
}

void sobel_filter::thread_ap_block_pp2_stage1_subdone() {
    ap_block_pp2_stage1_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state94_io.read()));
}

void sobel_filter::thread_ap_block_pp2_stage2() {
    ap_block_pp2_stage2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage2_01001() {
    ap_block_pp2_stage2_01001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage2_11001() {
    ap_block_pp2_stage2_11001 = ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state91_io.read())) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state95_io.read())));
}

void sobel_filter::thread_ap_block_pp2_stage2_subdone() {
    ap_block_pp2_stage2_subdone = ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state91_io.read())) || (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state95_io.read())));
}

void sobel_filter::thread_ap_block_pp2_stage3() {
    ap_block_pp2_stage3 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage3_01001() {
    ap_block_pp2_stage3_01001 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_pp2_stage3_11001() {
    ap_block_pp2_stage3_11001 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state92_io.read()));
}

void sobel_filter::thread_ap_block_pp2_stage3_subdone() {
    ap_block_pp2_stage3_subdone = (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_block_state92_io.read()));
}

void sobel_filter::thread_ap_block_state1() {
    ap_block_state1 = (esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) || esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()));
}

void sobel_filter::thread_ap_block_state10_pp0_stage1_iter0() {
    ap_block_state10_pp0_stage1_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state11_pp0_stage2_iter0() {
    ap_block_state11_pp0_stage2_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state12_pp0_stage3_iter0() {
    ap_block_state12_pp0_stage3_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state13_pp0_stage4_iter0() {
    ap_block_state13_pp0_stage4_iter0 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()));
}

void sobel_filter::thread_ap_block_state14_pp0_stage5_iter0() {
    ap_block_state14_pp0_stage5_iter0 = (esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op190_read_state14.read()));
}

void sobel_filter::thread_ap_block_state15_pp0_stage6_iter0() {
    ap_block_state15_pp0_stage6_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state16_pp0_stage7_iter0() {
    ap_block_state16_pp0_stage7_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state17_pp0_stage8_iter0() {
    ap_block_state17_pp0_stage8_iter0 = (esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op213_read_state17.read()));
}

void sobel_filter::thread_ap_block_state18_pp0_stage9_iter0() {
    ap_block_state18_pp0_stage9_iter0 = (esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op221_read_state18.read()));
}

void sobel_filter::thread_ap_block_state19_pp0_stage0_iter1() {
    ap_block_state19_pp0_stage0_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state21_pp1_stage0_iter0() {
    ap_block_state21_pp1_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state22_pp1_stage1_iter0() {
    ap_block_state22_pp1_stage1_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state23_pp1_stage2_iter0() {
    ap_block_state23_pp1_stage2_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state24_pp1_stage3_iter0() {
    ap_block_state24_pp1_stage3_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state25_pp1_stage4_iter0() {
    ap_block_state25_pp1_stage4_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state26_pp1_stage5_iter0() {
    ap_block_state26_pp1_stage5_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state27_pp1_stage6_iter0() {
    ap_block_state27_pp1_stage6_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state28_pp1_stage7_iter0() {
    ap_block_state28_pp1_stage7_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state29_pp1_stage8_iter0() {
    ap_block_state29_pp1_stage8_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state30_pp1_stage9_iter0() {
    ap_block_state30_pp1_stage9_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state31_pp1_stage10_iter0() {
    ap_block_state31_pp1_stage10_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state32_pp1_stage11_iter0() {
    ap_block_state32_pp1_stage11_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state33_pp1_stage12_iter0() {
    ap_block_state33_pp1_stage12_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state34_pp1_stage13_iter0() {
    ap_block_state34_pp1_stage13_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state35_pp1_stage14_iter0() {
    ap_block_state35_pp1_stage14_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state36_pp1_stage15_iter0() {
    ap_block_state36_pp1_stage15_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state37_pp1_stage16_iter0() {
    ap_block_state37_pp1_stage16_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state38_pp1_stage17_iter0() {
    ap_block_state38_pp1_stage17_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state39_pp1_stage18_iter0() {
    ap_block_state39_pp1_stage18_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state40_pp1_stage19_iter0() {
    ap_block_state40_pp1_stage19_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state41_pp1_stage20_iter0() {
    ap_block_state41_pp1_stage20_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state42_pp1_stage21_iter0() {
    ap_block_state42_pp1_stage21_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state43_pp1_stage22_iter0() {
    ap_block_state43_pp1_stage22_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state44_pp1_stage23_iter0() {
    ap_block_state44_pp1_stage23_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state45_pp1_stage24_iter0() {
    ap_block_state45_pp1_stage24_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state46_pp1_stage0_iter1() {
    ap_block_state46_pp1_stage0_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state47_pp1_stage1_iter1() {
    ap_block_state47_pp1_stage1_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state48_pp1_stage2_iter1() {
    ap_block_state48_pp1_stage2_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state49_pp1_stage3_iter1() {
    ap_block_state49_pp1_stage3_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state50_pp1_stage4_iter1() {
    ap_block_state50_pp1_stage4_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state51_pp1_stage5_iter1() {
    ap_block_state51_pp1_stage5_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state52_pp1_stage6_iter1() {
    ap_block_state52_pp1_stage6_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state53_pp1_stage7_iter1() {
    ap_block_state53_pp1_stage7_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state54_pp1_stage8_iter1() {
    ap_block_state54_pp1_stage8_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state55_pp1_stage9_iter1() {
    ap_block_state55_pp1_stage9_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state56_pp1_stage10_iter1() {
    ap_block_state56_pp1_stage10_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state57_pp1_stage11_iter1() {
    ap_block_state57_pp1_stage11_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state58_pp1_stage12_iter1() {
    ap_block_state58_pp1_stage12_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state59_pp1_stage13_iter1() {
    ap_block_state59_pp1_stage13_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state6() {
    ap_block_state6 = (esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_fu_880_p2.read()) && esl_seteq<1,1,1>(ap_const_logic_0, INPUT_STREAM_V_V_0_vld_out.read()));
}

void sobel_filter::thread_ap_block_state60_pp1_stage14_iter1() {
    ap_block_state60_pp1_stage14_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state61_pp1_stage15_iter1() {
    ap_block_state61_pp1_stage15_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state62_pp1_stage16_iter1() {
    ap_block_state62_pp1_stage16_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state63_pp1_stage17_iter1() {
    ap_block_state63_pp1_stage17_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state64_pp1_stage18_iter1() {
    ap_block_state64_pp1_stage18_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state65_pp1_stage19_iter1() {
    ap_block_state65_pp1_stage19_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state66_pp1_stage20_iter1() {
    ap_block_state66_pp1_stage20_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state67_pp1_stage21_iter1() {
    ap_block_state67_pp1_stage21_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state68_pp1_stage22_iter1() {
    ap_block_state68_pp1_stage22_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state69_pp1_stage23_iter1() {
    ap_block_state69_pp1_stage23_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state7() {
    ap_block_state7 = (esl_seteq<1,2,2>(ap_const_lv2_1, OUTPUT_STREAM_V_V_1_state.read()) || (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_TREADY.read()) && 
  esl_seteq<1,2,2>(ap_const_lv2_3, OUTPUT_STREAM_V_V_1_state.read())));
}

void sobel_filter::thread_ap_block_state70_pp1_stage24_iter1() {
    ap_block_state70_pp1_stage24_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state71_pp1_stage0_iter2() {
    ap_block_state71_pp1_stage0_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state72_pp1_stage1_iter2() {
    ap_block_state72_pp1_stage1_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state73_pp1_stage2_iter2() {
    ap_block_state73_pp1_stage2_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state74_pp1_stage3_iter2() {
    ap_block_state74_pp1_stage3_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state75_pp1_stage4_iter2() {
    ap_block_state75_pp1_stage4_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state76_pp1_stage5_iter2() {
    ap_block_state76_pp1_stage5_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state77_pp1_stage6_iter2() {
    ap_block_state77_pp1_stage6_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state78_pp1_stage7_iter2() {
    ap_block_state78_pp1_stage7_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state79_pp1_stage8_iter2() {
    ap_block_state79_pp1_stage8_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state80_pp1_stage9_iter2() {
    ap_block_state80_pp1_stage9_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state81_pp1_stage10_iter2() {
    ap_block_state81_pp1_stage10_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state82_pp1_stage11_iter2() {
    ap_block_state82_pp1_stage11_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state83_pp1_stage12_iter2() {
    ap_block_state83_pp1_stage12_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state84_pp1_stage13_iter2() {
    ap_block_state84_pp1_stage13_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state85_pp1_stage14_iter2() {
    ap_block_state85_pp1_stage14_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state86_pp1_stage15_iter2() {
    ap_block_state86_pp1_stage15_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state87_pp1_stage16_iter2() {
    ap_block_state87_pp1_stage16_iter2 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state89_pp2_stage0_iter0() {
    ap_block_state89_pp2_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state90_pp2_stage1_iter0() {
    ap_block_state90_pp2_stage1_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state91_io() {
    ap_block_state91_io = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()));
}

void sobel_filter::thread_ap_block_state91_pp2_stage2_iter0() {
    ap_block_state91_pp2_stage2_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state92_io() {
    ap_block_state92_io = ((esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
  esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read())) || (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1437_write_state92.read())));
}

void sobel_filter::thread_ap_block_state92_pp2_stage3_iter0() {
    ap_block_state92_pp2_stage3_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state93_io() {
    ap_block_state93_io = ((esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1442_write_state93.read())) || (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1445_write_state93.read())));
}

void sobel_filter::thread_ap_block_state93_pp2_stage0_iter1() {
    ap_block_state93_pp2_stage0_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state94_io() {
    ap_block_state94_io = ((esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1447_write_state94.read())) || (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()) && 
  esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1450_write_state94.read())));
}

void sobel_filter::thread_ap_block_state94_pp2_stage1_iter1() {
    ap_block_state94_pp2_stage1_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state95_io() {
    ap_block_state95_io = (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()) && esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_op1452_write_state95.read()));
}

void sobel_filter::thread_ap_block_state95_pp2_stage2_iter1() {
    ap_block_state95_pp2_stage2_iter1 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_block_state97_io() {
    ap_block_state97_io = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln87_fu_5180_p2.read()) && esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_1_ack_in.read()));
}

void sobel_filter::thread_ap_block_state9_pp0_stage0_iter0() {
    ap_block_state9_pp0_stage0_iter0 = !esl_seteq<1,1,1>(ap_const_boolean_1, ap_const_boolean_1);
}

void sobel_filter::thread_ap_condition_5935() {
    ap_condition_5935 = (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage3.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()) && esl_seteq<1,1,1>(ap_block_pp2_stage3_11001.read(), ap_const_boolean_0));
}

void sobel_filter::thread_ap_condition_pp0_exit_iter0_state18() {
    if (esl_seteq<1,1,1>(ap_const_boolean_1, ap_predicate_tran18to20_state18.read())) {
        ap_condition_pp0_exit_iter0_state18 = ap_const_logic_1;
    } else {
        ap_condition_pp0_exit_iter0_state18 = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_done() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state7.read()) && 
         !(esl_seteq<1,2,2>(ap_const_lv2_1, OUTPUT_STREAM_V_V_1_state.read()) || (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_TREADY.read()) && 
  esl_seteq<1,2,2>(ap_const_lv2_3, OUTPUT_STREAM_V_V_1_state.read()))) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_1_fu_900_p2.read()))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_enable_pp0() {
    ap_enable_pp0 = (ap_idle_pp0.read() ^ ap_const_logic_1);
}

void sobel_filter::thread_ap_enable_pp1() {
    ap_enable_pp1 = (ap_idle_pp1.read() ^ ap_const_logic_1);
}

void sobel_filter::thread_ap_enable_pp2() {
    ap_enable_pp2 = (ap_idle_pp2.read() ^ ap_const_logic_1);
}

void sobel_filter::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state1.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_idle_pp0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp0_iter1.read()))) {
        ap_idle_pp0 = ap_const_logic_1;
    } else {
        ap_idle_pp0 = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_idle_pp1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp1_iter2.read()))) {
        ap_idle_pp1 = ap_const_logic_1;
    } else {
        ap_idle_pp1 = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_idle_pp2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, ap_enable_reg_pp2_iter1.read()))) {
        ap_idle_pp2 = ap_const_logic_1;
    } else {
        ap_idle_pp2 = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_phi_mux_t_V_2_0_phi_fu_718_p4() {
    if ((esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_3_reg_5320.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp0_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp0_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp0_stage0.read(), ap_const_boolean_0))) {
        ap_phi_mux_t_V_2_0_phi_fu_718_p4 = add_ln700_reg_5334.read();
    } else {
        ap_phi_mux_t_V_2_0_phi_fu_718_p4 = t_V_2_0_reg_714.read();
    }
}

void sobel_filter::thread_ap_phi_mux_t_V_3_0_phi_fu_742_p4() {
    if ((esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp2_stage0.read(), ap_const_boolean_0) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_3_reg_7325.read()))) {
        ap_phi_mux_t_V_3_0_phi_fu_742_p4 = add_ln700_1_reg_7354.read();
    } else {
        ap_phi_mux_t_V_3_0_phi_fu_742_p4 = t_V_3_0_reg_738.read();
    }
}

void sobel_filter::thread_ap_phi_mux_t_V_6_0_phi_fu_730_p4() {
    if ((esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_5_reg_5455.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_6_reg_5492.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_7_reg_5536.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_8_reg_5586.read()) && 
         esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_9_reg_5635.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        ap_phi_mux_t_V_6_0_phi_fu_730_p4 = add_ln1353_36_reg_5650.read();
    } else {
        ap_phi_mux_t_V_6_0_phi_fu_730_p4 = t_V_6_0_reg_726.read();
    }
}

void sobel_filter::thread_ap_predicate_op1437_write_state92() {
    ap_predicate_op1437_write_state92 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()));
}

void sobel_filter::thread_ap_predicate_op1442_write_state93() {
    ap_predicate_op1442_write_state93 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()));
}

void sobel_filter::thread_ap_predicate_op1445_write_state93() {
    ap_predicate_op1445_write_state93 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()));
}

void sobel_filter::thread_ap_predicate_op1447_write_state94() {
    ap_predicate_op1447_write_state94 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288_pp2_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297_pp2_iter1_reg.read()));
}

void sobel_filter::thread_ap_predicate_op1450_write_state94() {
    ap_predicate_op1450_write_state94 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288_pp2_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297_pp2_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_3_reg_7325.read()));
}

void sobel_filter::thread_ap_predicate_op1452_write_state95() {
    ap_predicate_op1452_write_state95 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_reg_7288_pp2_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_1_reg_7297_pp2_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_2_reg_7316_pp2_iter1_reg.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln79_3_reg_7325_pp2_iter1_reg.read()));
}

void sobel_filter::thread_ap_predicate_op190_read_state14() {
    ap_predicate_op190_read_state14 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()));
}

void sobel_filter::thread_ap_predicate_op213_read_state17() {
    ap_predicate_op213_read_state17 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()));
}

void sobel_filter::thread_ap_predicate_op221_read_state18() {
    ap_predicate_op221_read_state18 = (esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_reg_5258.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_1_reg_5279.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_2_reg_5299.read()) && esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln30_3_reg_5320.read()));
}

void sobel_filter::thread_ap_predicate_tran18to20_state18() {
    ap_predicate_tran18to20_state18 = (((esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln30_3_reg_5320.read()) || 
   esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln30_2_reg_5299.read())) || 
  esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln30_1_reg_5279.read())) || esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln30_reg_5258.read()));
}

void sobel_filter::thread_ap_predicate_tran86to88_state45() {
    ap_predicate_tran86to88_state45 = (((((((esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_9_reg_5635.read()) || 
       esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_8_reg_5586.read())) || 
      esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_7_reg_5536.read())) || 
     esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_6_reg_5492.read())) || 
    esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_5_reg_5455.read())) || 
   esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_4_reg_5418.read())) || 
  esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_3_reg_5381.read())) || esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_2_reg_5339.read()));
}

void sobel_filter::thread_ap_predicate_tran94to96_state92() {
    ap_predicate_tran94to96_state92 = (((esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln79_3_reg_7325.read()) || 
   esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln79_2_reg_7316.read())) || 
  esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln79_1_reg_7297.read())) || esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln79_reg_7288.read()));
}

void sobel_filter::thread_ap_ready() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_state7.read()) && 
         !(esl_seteq<1,2,2>(ap_const_lv2_1, OUTPUT_STREAM_V_V_1_state.read()) || (esl_seteq<1,1,1>(ap_const_logic_0, OUTPUT_STREAM_V_V_TREADY.read()) && 
  esl_seteq<1,2,2>(ap_const_lv2_3, OUTPUT_STREAM_V_V_1_state.read()))) && 
         esl_seteq<1,1,1>(ap_const_lv1_0, icmp_ln887_1_fu_900_p2.read()))) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void sobel_filter::thread_ap_rst_n_inv() {
    ap_rst_n_inv =  (sc_logic) (~ap_rst_n.read());
}

void sobel_filter::thread_bitcast_ln512_10_fu_2902_p1() {
    bitcast_ln512_10_fu_2902_p1 = p_Result_33_fu_2895_p3.read();
}

void sobel_filter::thread_bitcast_ln512_11_fu_3090_p1() {
    bitcast_ln512_11_fu_3090_p1 = p_Result_34_fu_3083_p3.read();
}

void sobel_filter::thread_bitcast_ln512_12_fu_2926_p1() {
    bitcast_ln512_12_fu_2926_p1 = p_Result_35_fu_2919_p3.read();
}

void sobel_filter::thread_bitcast_ln512_13_fu_3102_p1() {
    bitcast_ln512_13_fu_3102_p1 = p_Result_36_fu_3095_p3.read();
}

void sobel_filter::thread_bitcast_ln512_14_fu_2958_p1() {
    bitcast_ln512_14_fu_2958_p1 = p_Result_37_fu_2951_p3.read();
}

void sobel_filter::thread_bitcast_ln512_15_fu_3172_p1() {
    bitcast_ln512_15_fu_3172_p1 = p_Result_38_fu_3165_p3.read();
}

void sobel_filter::thread_bitcast_ln512_1_fu_3030_p1() {
    bitcast_ln512_1_fu_3030_p1 = p_Result_24_fu_3023_p3.read();
}

void sobel_filter::thread_bitcast_ln512_2_fu_2692_p1() {
    bitcast_ln512_2_fu_2692_p1 = p_Result_25_fu_2685_p3.read();
}

void sobel_filter::thread_bitcast_ln512_3_fu_3042_p1() {
    bitcast_ln512_3_fu_3042_p1 = p_Result_26_fu_3035_p3.read();
}

void sobel_filter::thread_bitcast_ln512_4_fu_2764_p1() {
    bitcast_ln512_4_fu_2764_p1 = p_Result_27_fu_2757_p3.read();
}

void sobel_filter::thread_bitcast_ln512_5_fu_3054_p1() {
    bitcast_ln512_5_fu_3054_p1 = p_Result_28_fu_3047_p3.read();
}

void sobel_filter::thread_bitcast_ln512_6_fu_2832_p1() {
    bitcast_ln512_6_fu_2832_p1 = p_Result_29_fu_2825_p3.read();
}

void sobel_filter::thread_bitcast_ln512_7_fu_3066_p1() {
    bitcast_ln512_7_fu_3066_p1 = p_Result_30_fu_3059_p3.read();
}

void sobel_filter::thread_bitcast_ln512_8_fu_2878_p1() {
    bitcast_ln512_8_fu_2878_p1 = p_Result_31_fu_2871_p3.read();
}

void sobel_filter::thread_bitcast_ln512_9_fu_3078_p1() {
    bitcast_ln512_9_fu_3078_p1 = p_Result_32_fu_3071_p3.read();
}

void sobel_filter::thread_bitcast_ln512_fu_2220_p1() {
    bitcast_ln512_fu_2220_p1 = p_Result_23_fu_2213_p3.read();
}

void sobel_filter::thread_bitcast_ln696_1_fu_3277_p1() {
    bitcast_ln696_1_fu_3277_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_2_fu_3435_p1() {
    bitcast_ln696_2_fu_3435_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_3_fu_3593_p1() {
    bitcast_ln696_3_fu_3593_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_4_fu_3751_p1() {
    bitcast_ln696_4_fu_3751_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_5_fu_3909_p1() {
    bitcast_ln696_5_fu_3909_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_6_fu_4142_p1() {
    bitcast_ln696_6_fu_4142_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_7_fu_4316_p1() {
    bitcast_ln696_7_fu_4316_p1 = reg_856.read();
}

void sobel_filter::thread_bitcast_ln696_fu_3107_p1() {
    bitcast_ln696_fu_3107_p1 = reg_856.read();
}

void sobel_filter::thread_count_V_fu_5192_p2() {
    count_V_fu_5192_p2 = (!p_0243_0_reg_691.read().is_01() || !p_0767_0_reg_679.read().is_01())? sc_lv<16>(): (sc_biguint<16>(p_0243_0_reg_691.read()) + sc_biguint<16>(p_0767_0_reg_679.read()));
}

void sobel_filter::thread_grp_fu_761_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage6.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_7_fu_2963_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage5.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_6_fu_2931_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage4.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_5_fu_2907_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage3.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_4_fu_2883_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage2.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_3_fu_2837_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage1.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_2_fu_2769_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_1_fu_2697_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage24.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage24.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln49_fu_2616_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage23.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_7_fu_2572_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage22.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_6_fu_2449_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage21.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_5_fu_2315_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage20.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_4_fu_2124_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage19.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_3_fu_1944_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage18.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_2_fu_1816_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage17.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_1_fu_1724_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage13.read(), ap_const_boolean_0))) {
        grp_fu_761_p0 = sext_ln47_fu_1534_p1.read();
    } else {
        grp_fu_761_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_grp_fu_764_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage6.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_14_fu_2958_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage5.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_12_fu_2926_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage4.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_10_fu_2902_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage3.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_8_fu_2878_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage2.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_6_fu_2832_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage1.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_4_fu_2764_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_2_fu_2692_p1.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage21.read(), ap_const_boolean_0))) {
        grp_fu_764_p0 = bitcast_ln512_fu_2220_p1.read();
    } else {
        grp_fu_764_p0 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_grp_fu_767_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage21.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage21.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_7_reg_6546.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage20.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_6_reg_6526.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage18.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_5_reg_6501.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage17.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_4_reg_6481.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_3_reg_6461.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_2_reg_6436.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage14.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_1_reg_6406.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage13.read(), ap_const_boolean_0))) {
        grp_fu_767_p0 = temp_reg_6226.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage24.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage24.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage1.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage3.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage3.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage5.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage5.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage7.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage9.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage9.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage11.read(), ap_const_boolean_0)))) {
        grp_fu_767_p0 = reg_846.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage0.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage19.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter0.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage23.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage2.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage2.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage4.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage4.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage6.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage6.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage8.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage8.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage10.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage12.read(), ap_const_boolean_0)))) {
        grp_fu_767_p0 = reg_841.read();
    } else {
        grp_fu_767_p0 =  (sc_lv<32>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_grp_fu_770_p0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
         esl_seteq<1,1,1>(ap_block_pp1_stage22.read(), ap_const_boolean_0))) {
        grp_fu_770_p0 = tmp_2_6_reg_6636.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage20.read(), ap_const_boolean_0))) {
        grp_fu_770_p0 = tmp_2_5_reg_6626.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage19.read(), ap_const_boolean_0))) {
        grp_fu_770_p0 = tmp_2_4_reg_6616.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage18.read(), ap_const_boolean_0))) {
        grp_fu_770_p0 = tmp_2_3_reg_6606.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage17.read(), ap_const_boolean_0))) {
        grp_fu_770_p0 = tmp_2_2_reg_6596.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
        grp_fu_770_p0 = tmp_2_1_reg_6586.read();
    } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0)) || 
                (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
                 esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read()) && 
                 esl_seteq<1,1,1>(ap_block_pp1_stage23.read(), ap_const_boolean_0)))) {
        grp_fu_770_p0 = reg_851.read();
    } else {
        grp_fu_770_p0 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_grp_fu_770_p1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter1.read())) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage23.read()) && 
             esl_seteq<1,1,1>(ap_block_pp1_stage23.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_15_fu_3172_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage22.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage22.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_13_fu_3102_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage20.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage20.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_11_fu_3090_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage19.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage19.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_9_fu_3078_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage18.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage18.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_7_fu_3066_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage17.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage17.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_5_fu_3054_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_3_fu_3042_p1.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0))) {
            grp_fu_770_p1 = bitcast_ln512_1_fu_3030_p1.read();
        } else {
            grp_fu_770_p1 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
    } else {
        grp_fu_770_p1 =  (sc_lv<64>) ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_i_V_1_fu_5186_p2() {
    i_V_1_fu_5186_p2 = (!t_V_3_reg_750.read().is_01() || !ap_const_lv16_1.is_01())? sc_lv<16>(): (sc_biguint<16>(t_V_3_reg_750.read()) + sc_biguint<16>(ap_const_lv16_1));
}

void sobel_filter::thread_i_V_fu_911_p2() {
    i_V_fu_911_p2 = (!t_V_1_reg_703.read().is_01() || !ap_const_lv16_1.is_01())? sc_lv<16>(): (sc_biguint<16>(t_V_1_reg_703.read()) + sc_biguint<16>(ap_const_lv16_1));
}

void sobel_filter::thread_icmp_ln29_fu_905_p2() {
    icmp_ln29_fu_905_p2 = (!t_V_1_reg_703.read().is_01() || !p_0767_0_reg_679.read().is_01())? sc_lv<1>(): sc_lv<1>(t_V_1_reg_703.read() == p_0767_0_reg_679.read());
}

void sobel_filter::thread_icmp_ln30_1_fu_954_p2() {
    icmp_ln30_1_fu_954_p2 = (!or_ln700_fu_948_p2.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(or_ln700_fu_948_p2.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln30_2_fu_991_p2() {
    icmp_ln30_2_fu_991_p2 = (!or_ln700_1_fu_985_p2.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(or_ln700_1_fu_985_p2.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln30_3_fu_1028_p2() {
    icmp_ln30_3_fu_1028_p2 = (!or_ln700_2_fu_1022_p2.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(or_ln700_2_fu_1022_p2.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln30_fu_917_p2() {
    icmp_ln30_fu_917_p2 = (!ap_phi_mux_t_V_2_0_phi_fu_718_p4.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_t_V_2_0_phi_fu_718_p4.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln326_1_fu_3311_p2() {
    icmp_ln326_1_fu_3311_p2 = (!trunc_ln311_1_fu_3281_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_1_fu_3281_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_2_fu_3469_p2() {
    icmp_ln326_2_fu_3469_p2 = (!trunc_ln311_2_fu_3439_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_2_fu_3439_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_3_fu_3627_p2() {
    icmp_ln326_3_fu_3627_p2 = (!trunc_ln311_3_fu_3597_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_3_fu_3597_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_4_fu_3785_p2() {
    icmp_ln326_4_fu_3785_p2 = (!trunc_ln311_4_fu_3755_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_4_fu_3755_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_5_fu_3943_p2() {
    icmp_ln326_5_fu_3943_p2 = (!trunc_ln311_5_fu_3913_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_5_fu_3913_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_6_fu_4176_p2() {
    icmp_ln326_6_fu_4176_p2 = (!trunc_ln311_6_fu_4146_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_6_fu_4146_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_7_fu_4350_p2() {
    icmp_ln326_7_fu_4350_p2 = (!trunc_ln311_7_fu_4320_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_7_fu_4320_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln326_fu_3141_p2() {
    icmp_ln326_fu_3141_p2 = (!trunc_ln311_fu_3111_p1.read().is_01() || !ap_const_lv63_0.is_01())? sc_lv<1>(): sc_lv<1>(trunc_ln311_fu_3111_p1.read() == ap_const_lv63_0);
}

void sobel_filter::thread_icmp_ln330_1_fu_3323_p2() {
    icmp_ln330_1_fu_3323_p2 = (!p_Result_s_13_fu_3293_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_13_fu_3293_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_2_fu_3481_p2() {
    icmp_ln330_2_fu_3481_p2 = (!p_Result_2_fu_3451_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_2_fu_3451_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_3_fu_3639_p2() {
    icmp_ln330_3_fu_3639_p2 = (!p_Result_3_fu_3609_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_3_fu_3609_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_4_fu_3797_p2() {
    icmp_ln330_4_fu_3797_p2 = (!p_Result_4_fu_3767_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_4_fu_3767_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_5_fu_3955_p2() {
    icmp_ln330_5_fu_3955_p2 = (!p_Result_5_fu_3925_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_5_fu_3925_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_6_fu_4188_p2() {
    icmp_ln330_6_fu_4188_p2 = (!p_Result_6_fu_4158_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_6_fu_4158_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_7_fu_4362_p2() {
    icmp_ln330_7_fu_4362_p2 = (!p_Result_7_fu_4332_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_7_fu_4332_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln330_fu_3153_p2() {
    icmp_ln330_fu_3153_p2 = (!p_Result_s_fu_3123_p4.read().is_01() || !ap_const_lv11_433.is_01())? sc_lv<1>(): sc_lv<1>(p_Result_s_fu_3123_p4.read() == ap_const_lv11_433);
}

void sobel_filter::thread_icmp_ln332_1_fu_3329_p2() {
    icmp_ln332_1_fu_3329_p2 = (!sub_ln329_1_fu_3317_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_1_fu_3317_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_2_fu_3487_p2() {
    icmp_ln332_2_fu_3487_p2 = (!sub_ln329_2_fu_3475_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_2_fu_3475_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_3_fu_3645_p2() {
    icmp_ln332_3_fu_3645_p2 = (!sub_ln329_3_fu_3633_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_3_fu_3633_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_4_fu_3803_p2() {
    icmp_ln332_4_fu_3803_p2 = (!sub_ln329_4_fu_3791_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_4_fu_3791_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_5_fu_3961_p2() {
    icmp_ln332_5_fu_3961_p2 = (!sub_ln329_5_fu_3949_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_5_fu_3949_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_6_fu_4194_p2() {
    icmp_ln332_6_fu_4194_p2 = (!sub_ln329_6_fu_4182_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_6_fu_4182_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_7_fu_4368_p2() {
    icmp_ln332_7_fu_4368_p2 = (!sub_ln329_7_fu_4356_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_7_fu_4356_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln332_fu_3159_p2() {
    icmp_ln332_fu_3159_p2 = (!sub_ln329_fu_3147_p2.read().is_01() || !ap_const_lv12_0.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_fu_3147_p2.read()) > sc_bigint<12>(ap_const_lv12_0));
}

void sobel_filter::thread_icmp_ln333_1_fu_3349_p2() {
    icmp_ln333_1_fu_3349_p2 = (!sub_ln329_1_reg_6743.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_1_reg_6743.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_2_fu_3507_p2() {
    icmp_ln333_2_fu_3507_p2 = (!sub_ln329_2_reg_6811.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_2_reg_6811.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_3_fu_3665_p2() {
    icmp_ln333_3_fu_3665_p2 = (!sub_ln329_3_reg_6879.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_3_reg_6879.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_4_fu_3823_p2() {
    icmp_ln333_4_fu_3823_p2 = (!sub_ln329_4_reg_6947.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_4_reg_6947.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_5_fu_3981_p2() {
    icmp_ln333_5_fu_3981_p2 = (!sub_ln329_5_reg_7015.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_5_reg_7015.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_6_fu_4230_p2() {
    icmp_ln333_6_fu_4230_p2 = (!sub_ln329_6_reg_7089.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_6_reg_7089.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_7_fu_4392_p2() {
    icmp_ln333_7_fu_4392_p2 = (!sub_ln329_7_reg_7167.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_7_reg_7167.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln333_fu_3191_p2() {
    icmp_ln333_fu_3191_p2 = (!sub_ln329_reg_6670.read().is_01() || !ap_const_lv12_36.is_01())? sc_lv<1>(): (sc_bigint<12>(sub_ln329_reg_6670.read()) < sc_bigint<12>(ap_const_lv12_36));
}

void sobel_filter::thread_icmp_ln343_1_fu_3369_p2() {
    icmp_ln343_1_fu_3369_p2 = (!tmp_12_fu_3359_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_12_fu_3359_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_2_fu_3527_p2() {
    icmp_ln343_2_fu_3527_p2 = (!tmp_21_fu_3517_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_21_fu_3517_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_3_fu_3685_p2() {
    icmp_ln343_3_fu_3685_p2 = (!tmp_27_fu_3675_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_27_fu_3675_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_4_fu_3843_p2() {
    icmp_ln343_4_fu_3843_p2 = (!tmp_30_fu_3833_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_30_fu_3833_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_5_fu_4001_p2() {
    icmp_ln343_5_fu_4001_p2 = (!tmp_33_fu_3991_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_33_fu_3991_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_6_fu_4250_p2() {
    icmp_ln343_6_fu_4250_p2 = (!tmp_36_fu_4240_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_36_fu_4240_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_7_fu_4412_p2() {
    icmp_ln343_7_fu_4412_p2 = (!tmp_39_fu_4402_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_39_fu_4402_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln343_fu_3211_p2() {
    icmp_ln343_fu_3211_p2 = (!tmp_6_fu_3201_p4.read().is_01() || !ap_const_lv8_1.is_01())? sc_lv<1>(): (sc_bigint<8>(tmp_6_fu_3201_p4.read()) < sc_bigint<8>(ap_const_lv8_1));
}

void sobel_filter::thread_icmp_ln79_1_fu_5101_p2() {
    icmp_ln79_1_fu_5101_p2 = (!or_ln700_3_fu_5095_p2.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(or_ln700_3_fu_5095_p2.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln79_2_fu_5130_p2() {
    icmp_ln79_2_fu_5130_p2 = (!or_ln700_4_fu_5124_p2.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(or_ln700_4_fu_5124_p2.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln79_3_fu_5151_p2() {
    icmp_ln79_3_fu_5151_p2 = (!or_ln700_5_fu_5145_p2.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(or_ln700_5_fu_5145_p2.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln79_fu_5080_p2() {
    icmp_ln79_fu_5080_p2 = (!ap_phi_mux_t_V_3_0_phi_fu_742_p4.read().is_01() || !tmp_V_8_reg_5198.read().is_01())? sc_lv<1>(): sc_lv<1>(ap_phi_mux_t_V_3_0_phi_fu_742_p4.read() == tmp_V_8_reg_5198.read());
}

void sobel_filter::thread_icmp_ln87_fu_5180_p2() {
    icmp_ln87_fu_5180_p2 = (!t_V_3_reg_750.read().is_01() || !reg_774.read().is_01())? sc_lv<1>(): sc_lv<1>(t_V_3_reg_750.read() == reg_774.read());
}

void sobel_filter::thread_icmp_ln887_1_fu_900_p2() {
    icmp_ln887_1_fu_900_p2 = (!p_0243_0_reg_691.read().is_01() || !tmp_V_9_reg_5211.read().is_01())? sc_lv<1>(): (sc_biguint<16>(p_0243_0_reg_691.read()) < sc_biguint<16>(tmp_V_9_reg_5211.read()));
}

void sobel_filter::thread_icmp_ln887_2_fu_1086_p2() {
    icmp_ln887_2_fu_1086_p2 = (!zext_ln887_1_fu_1082_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_1_fu_1082_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_3_fu_1127_p2() {
    icmp_ln887_3_fu_1127_p2 = (!zext_ln887_2_fu_1124_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_2_fu_1124_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_4_fu_1163_p2() {
    icmp_ln887_4_fu_1163_p2 = (!zext_ln887_3_fu_1160_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_3_fu_1160_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_5_fu_1199_p2() {
    icmp_ln887_5_fu_1199_p2 = (!zext_ln887_4_fu_1196_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_4_fu_1196_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_6_fu_1235_p2() {
    icmp_ln887_6_fu_1235_p2 = (!zext_ln887_5_fu_1232_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_5_fu_1232_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_7_fu_1271_p2() {
    icmp_ln887_7_fu_1271_p2 = (!zext_ln887_6_fu_1268_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_6_fu_1268_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_8_fu_1311_p2() {
    icmp_ln887_8_fu_1311_p2 = (!zext_ln887_7_fu_1308_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_7_fu_1308_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_9_fu_1347_p2() {
    icmp_ln887_9_fu_1347_p2 = (!zext_ln887_8_fu_1344_p1.read().is_01() || !ret_V_1_reg_5234.read().is_01())? sc_lv<1>(): (sc_bigint<17>(zext_ln887_8_fu_1344_p1.read()) < sc_bigint<17>(ret_V_1_reg_5234.read()));
}

void sobel_filter::thread_icmp_ln887_fu_880_p2() {
    icmp_ln887_fu_880_p2 = (!zext_ln887_fu_876_p1.read().is_01() || !ret_V_reg_5221.read().is_01())? sc_lv<1>(): (sc_biguint<17>(zext_ln887_fu_876_p1.read()) < sc_biguint<17>(ret_V_reg_5221.read()));
}

void sobel_filter::thread_k_V_fu_885_p2() {
    k_V_fu_885_p2 = (!t_V_reg_668.read().is_01() || !ap_const_lv16_1.is_01())? sc_lv<16>(): (sc_biguint<16>(t_V_reg_668.read()) + sc_biguint<16>(ap_const_lv16_1));
}

void sobel_filter::thread_lshr_ln334_1_fu_3379_p2() {
    lshr_ln334_1_fu_3379_p2 = (!zext_ln334_1_fu_3375_p1.read().is_01())? sc_lv<53>(): tmp_10_fu_3338_p3.read() >> (unsigned short)zext_ln334_1_fu_3375_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_2_fu_3537_p2() {
    lshr_ln334_2_fu_3537_p2 = (!zext_ln334_2_fu_3533_p1.read().is_01())? sc_lv<53>(): tmp_13_fu_3496_p3.read() >> (unsigned short)zext_ln334_2_fu_3533_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_3_fu_3695_p2() {
    lshr_ln334_3_fu_3695_p2 = (!zext_ln334_3_fu_3691_p1.read().is_01())? sc_lv<53>(): tmp_15_fu_3654_p3.read() >> (unsigned short)zext_ln334_3_fu_3691_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_4_fu_3853_p2() {
    lshr_ln334_4_fu_3853_p2 = (!zext_ln334_4_fu_3849_p1.read().is_01())? sc_lv<53>(): tmp_17_fu_3812_p3.read() >> (unsigned short)zext_ln334_4_fu_3849_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_5_fu_4011_p2() {
    lshr_ln334_5_fu_4011_p2 = (!zext_ln334_5_fu_4007_p1.read().is_01())? sc_lv<53>(): tmp_18_fu_3970_p3.read() >> (unsigned short)zext_ln334_5_fu_4007_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_6_fu_4260_p2() {
    lshr_ln334_6_fu_4260_p2 = (!zext_ln334_6_fu_4256_p1.read().is_01())? sc_lv<53>(): tmp_19_fu_4219_p3.read() >> (unsigned short)zext_ln334_6_fu_4256_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_7_fu_4422_p2() {
    lshr_ln334_7_fu_4422_p2 = (!zext_ln334_7_fu_4418_p1.read().is_01())? sc_lv<53>(): tmp_20_fu_4381_p3.read() >> (unsigned short)zext_ln334_7_fu_4418_p1.read().to_uint();
}

void sobel_filter::thread_lshr_ln334_fu_3221_p2() {
    lshr_ln334_fu_3221_p2 = (!zext_ln334_fu_3217_p1.read().is_01())? sc_lv<53>(): tmp_8_fu_3180_p3.read() >> (unsigned short)zext_ln334_fu_3217_p1.read().to_uint();
}

void sobel_filter::thread_or_ln330_1_fu_3389_p2() {
    or_ln330_1_fu_3389_p2 = (icmp_ln326_1_reg_6736.read() | icmp_ln330_1_reg_6750.read());
}

void sobel_filter::thread_or_ln330_2_fu_3547_p2() {
    or_ln330_2_fu_3547_p2 = (icmp_ln326_2_reg_6804.read() | icmp_ln330_2_reg_6818.read());
}

void sobel_filter::thread_or_ln330_3_fu_3705_p2() {
    or_ln330_3_fu_3705_p2 = (icmp_ln326_3_reg_6872.read() | icmp_ln330_3_reg_6886.read());
}

void sobel_filter::thread_or_ln330_4_fu_3863_p2() {
    or_ln330_4_fu_3863_p2 = (icmp_ln326_4_reg_6940.read() | icmp_ln330_4_reg_6954.read());
}

void sobel_filter::thread_or_ln330_5_fu_4021_p2() {
    or_ln330_5_fu_4021_p2 = (icmp_ln326_5_reg_7008.read() | icmp_ln330_5_reg_7022.read());
}

void sobel_filter::thread_or_ln330_6_fu_4270_p2() {
    or_ln330_6_fu_4270_p2 = (icmp_ln326_6_reg_7082.read() | icmp_ln330_6_reg_7096.read());
}

void sobel_filter::thread_or_ln330_7_fu_4432_p2() {
    or_ln330_7_fu_4432_p2 = (icmp_ln326_7_reg_7160.read() | icmp_ln330_7_reg_7174.read());
}

void sobel_filter::thread_or_ln330_fu_3231_p2() {
    or_ln330_fu_3231_p2 = (icmp_ln326_reg_6663.read() | icmp_ln330_reg_6677.read());
}

void sobel_filter::thread_or_ln332_1_fu_3418_p2() {
    or_ln332_1_fu_3418_p2 = (or_ln330_1_fu_3389_p2.read() | icmp_ln332_1_reg_6756.read());
}

void sobel_filter::thread_or_ln332_2_fu_3576_p2() {
    or_ln332_2_fu_3576_p2 = (or_ln330_2_fu_3547_p2.read() | icmp_ln332_2_reg_6824.read());
}

void sobel_filter::thread_or_ln332_3_fu_3734_p2() {
    or_ln332_3_fu_3734_p2 = (or_ln330_3_fu_3705_p2.read() | icmp_ln332_3_reg_6892.read());
}

void sobel_filter::thread_or_ln332_4_fu_3892_p2() {
    or_ln332_4_fu_3892_p2 = (or_ln330_4_fu_3863_p2.read() | icmp_ln332_4_reg_6960.read());
}

void sobel_filter::thread_or_ln332_5_fu_4050_p2() {
    or_ln332_5_fu_4050_p2 = (or_ln330_5_fu_4021_p2.read() | icmp_ln332_5_reg_7028.read());
}

void sobel_filter::thread_or_ln332_6_fu_4299_p2() {
    or_ln332_6_fu_4299_p2 = (or_ln330_6_fu_4270_p2.read() | icmp_ln332_6_reg_7102.read());
}

void sobel_filter::thread_or_ln332_7_fu_4461_p2() {
    or_ln332_7_fu_4461_p2 = (or_ln330_7_fu_4432_p2.read() | icmp_ln332_7_reg_7180.read());
}

void sobel_filter::thread_or_ln332_fu_3260_p2() {
    or_ln332_fu_3260_p2 = (or_ln330_fu_3231_p2.read() | icmp_ln332_reg_6683.read());
}

void sobel_filter::thread_or_ln700_1_fu_985_p2() {
    or_ln700_1_fu_985_p2 = (t_V_2_0_reg_714.read() | ap_const_lv16_2);
}

void sobel_filter::thread_or_ln700_2_fu_1022_p2() {
    or_ln700_2_fu_1022_p2 = (t_V_2_0_reg_714.read() | ap_const_lv16_3);
}

void sobel_filter::thread_or_ln700_3_fu_5095_p2() {
    or_ln700_3_fu_5095_p2 = (ap_phi_mux_t_V_3_0_phi_fu_742_p4.read() | ap_const_lv16_1);
}

void sobel_filter::thread_or_ln700_4_fu_5124_p2() {
    or_ln700_4_fu_5124_p2 = (t_V_3_0_reg_738.read() | ap_const_lv16_2);
}

void sobel_filter::thread_or_ln700_5_fu_5145_p2() {
    or_ln700_5_fu_5145_p2 = (t_V_3_0_reg_738.read() | ap_const_lv16_3);
}

void sobel_filter::thread_or_ln700_fu_948_p2() {
    or_ln700_fu_948_p2 = (t_V_2_0_reg_714.read() | ap_const_lv16_1);
}

void sobel_filter::thread_p_Result_23_fu_2213_p3() {
    p_Result_23_fu_2213_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_reg_6106.read());
}

void sobel_filter::thread_p_Result_24_fu_3023_p3() {
    p_Result_24_fu_3023_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_1_reg_6521.read());
}

void sobel_filter::thread_p_Result_25_fu_2685_p3() {
    p_Result_25_fu_2685_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_2_reg_6336.read());
}

void sobel_filter::thread_p_Result_26_fu_3035_p3() {
    p_Result_26_fu_3035_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_3_reg_6541.read());
}

void sobel_filter::thread_p_Result_27_fu_2757_p3() {
    p_Result_27_fu_2757_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_4_reg_6386.read());
}

void sobel_filter::thread_p_Result_28_fu_3047_p3() {
    p_Result_28_fu_3047_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_5_reg_6551.read());
}

void sobel_filter::thread_p_Result_29_fu_2825_p3() {
    p_Result_29_fu_2825_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_6_reg_6421.read());
}

void sobel_filter::thread_p_Result_2_fu_3451_p4() {
    p_Result_2_fu_3451_p4 = bitcast_ln696_2_fu_3435_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_30_fu_3059_p3() {
    p_Result_30_fu_3059_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_7_reg_6556.read());
}

void sobel_filter::thread_p_Result_31_fu_2871_p3() {
    p_Result_31_fu_2871_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_8_reg_6451.read());
}

void sobel_filter::thread_p_Result_32_fu_3071_p3() {
    p_Result_32_fu_3071_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_9_reg_6561.read());
}

void sobel_filter::thread_p_Result_33_fu_2895_p3() {
    p_Result_33_fu_2895_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_10_reg_6476.read());
}

void sobel_filter::thread_p_Result_34_fu_3083_p3() {
    p_Result_34_fu_3083_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_11_reg_6566.read());
}

void sobel_filter::thread_p_Result_35_fu_2919_p3() {
    p_Result_35_fu_2919_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_12_reg_6496.read());
}

void sobel_filter::thread_p_Result_36_fu_3095_p3() {
    p_Result_36_fu_3095_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_13_reg_6571.read());
}

void sobel_filter::thread_p_Result_37_fu_2951_p3() {
    p_Result_37_fu_2951_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_14_reg_6516.read());
}

void sobel_filter::thread_p_Result_38_fu_3165_p3() {
    p_Result_38_fu_3165_p3 = esl_concat<1,63>(ap_const_lv1_0, trunc_ln368_15_reg_6576.read());
}

void sobel_filter::thread_p_Result_3_fu_3609_p4() {
    p_Result_3_fu_3609_p4 = bitcast_ln696_3_fu_3593_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_4_fu_3767_p4() {
    p_Result_4_fu_3767_p4 = bitcast_ln696_4_fu_3751_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_5_fu_3925_p4() {
    p_Result_5_fu_3925_p4 = bitcast_ln696_5_fu_3909_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_6_fu_4158_p4() {
    p_Result_6_fu_4158_p4 = bitcast_ln696_6_fu_4142_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_7_fu_4332_p4() {
    p_Result_7_fu_4332_p4 = bitcast_ln696_7_fu_4316_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_s_13_fu_3293_p4() {
    p_Result_s_13_fu_3293_p4 = bitcast_ln696_1_fu_3277_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Result_s_fu_3123_p4() {
    p_Result_s_fu_3123_p4 = bitcast_ln696_fu_3107_p1.read().range(62, 52);
}

void sobel_filter::thread_p_Val2_10_fu_2887_p1() {
    p_Val2_10_fu_2887_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_11_fu_2999_p1() {
    p_Val2_11_fu_2999_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_12_fu_2911_p1() {
    p_Val2_12_fu_2911_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_13_fu_3007_p1() {
    p_Val2_13_fu_3007_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_14_fu_2935_p1() {
    p_Val2_14_fu_2935_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_15_fu_3015_p1() {
    p_Val2_15_fu_3015_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_1_fu_2943_p1() {
    p_Val2_1_fu_2943_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_2_fu_2620_p1() {
    p_Val2_2_fu_2620_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_3_fu_2967_p1() {
    p_Val2_3_fu_2967_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_4_fu_2701_p1() {
    p_Val2_4_fu_2701_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_5_fu_2975_p1() {
    p_Val2_5_fu_2975_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_6_fu_2773_p1() {
    p_Val2_6_fu_2773_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_7_fu_2983_p1() {
    p_Val2_7_fu_2983_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_8_fu_2841_p1() {
    p_Val2_8_fu_2841_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_9_fu_2991_p1() {
    p_Val2_9_fu_2991_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_p_Val2_s_fu_2035_p1() {
    p_Val2_s_fu_2035_p1 = grp_fu_767_p1.read();
}

void sobel_filter::thread_result_lb_V_address0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
         esl_seteq<1,1,1>(ap_block_pp2_stage2.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln162_3_fu_5170_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
                esl_seteq<1,1,1>(ap_block_pp2_stage1.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln162_fu_5116_p1.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_28_reg_6361_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_24_reg_6351_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage14.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_20_reg_6301_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage13.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_16_reg_6291_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage12.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_12_reg_6241_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage11.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_8_reg_6231_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage10.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln215_4_reg_6161_pp1_iter1_reg.read());
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
                esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
                esl_seteq<1,1,1>(ap_block_pp1_stage7.read(), ap_const_boolean_0))) {
        result_lb_V_address0 =  (sc_lv<13>) (sext_ln321_fu_4374_p1.read());
    } else {
        result_lb_V_address0 = "XXXXXXXXXXXXX";
    }
}

void sobel_filter::thread_result_lb_V_address1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read())) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
             esl_seteq<1,1,1>(ap_block_pp2_stage2.read(), ap_const_boolean_0))) {
            result_lb_V_address1 =  (sc_lv<13>) (sext_ln162_2_fu_5166_p1.read());
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp2_stage1.read(), ap_const_boolean_0))) {
            result_lb_V_address1 =  (sc_lv<13>) (sext_ln162_1_fu_5120_p1.read());
        } else {
            result_lb_V_address1 = "XXXXXXXXXXXXX";
        }
    } else {
        result_lb_V_address1 = "XXXXXXXXXXXXX";
    }
}

void sobel_filter::thread_result_lb_V_ce0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage13_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage15_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage16_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage1_11001.read(), ap_const_boolean_0)))) {
        result_lb_V_ce0 = ap_const_logic_1;
    } else {
        result_lb_V_ce0 = ap_const_logic_0;
    }
}

void sobel_filter::thread_result_lb_V_ce1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage2.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage2_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp2_iter0.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp2_stage1.read()) && 
          esl_seteq<1,1,1>(ap_block_pp2_stage1_11001.read(), ap_const_boolean_0)))) {
        result_lb_V_ce1 = ap_const_logic_1;
    } else {
        result_lb_V_ce1 = ap_const_logic_0;
    }
}

void sobel_filter::thread_result_lb_V_d0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read())) {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
             esl_seteq<1,1,1>(ap_block_pp1_stage16.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_7_reg_7283.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage15.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_6_reg_7272.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage14.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_5_reg_7261.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage13.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_4_reg_7250.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage12.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_3_reg_7239.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage11.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_2_reg_7228.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage10.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_1_reg_7217.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
                    esl_seteq<1,1,1>(ap_block_pp1_stage7.read(), ap_const_boolean_0))) {
            result_lb_V_d0 = select_ln351_reg_7113.read();
        } else {
            result_lb_V_d0 =  (sc_lv<16>) ("XXXXXXXXXXXXXXXX");
        }
    } else {
        result_lb_V_d0 =  (sc_lv<16>) ("XXXXXXXXXXXXXXXX");
    }
}

void sobel_filter::thread_result_lb_V_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage7.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage7_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage10.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage10_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage11.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage11_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418_pp1_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage12.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage12_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_5_reg_5455_pp1_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_5_reg_5455_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_6_reg_5492_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage13.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage13_11001.read(), ap_const_boolean_0)) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage14.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage14_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_5_reg_5455_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_6_reg_5492_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_7_reg_5536_pp1_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage15.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage15_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_5_reg_5455_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_6_reg_5492_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_7_reg_5536_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_8_reg_5586_pp1_iter2_reg.read())) || 
         (esl_seteq<1,1,1>(ap_const_logic_1, ap_CS_fsm_pp1_stage16.read()) && 
          esl_seteq<1,1,1>(ap_block_pp1_stage16_11001.read(), ap_const_boolean_0) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_enable_reg_pp1_iter2.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_2_reg_5339_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_3_reg_5381_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_4_reg_5418_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_5_reg_5455_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_6_reg_5492_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_7_reg_5536_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_8_reg_5586_pp1_iter2_reg.read()) && 
          esl_seteq<1,1,1>(ap_const_lv1_1, icmp_ln887_9_reg_5635_pp1_iter2_reg.read())))) {
        result_lb_V_we0 = ap_const_logic_1;
    } else {
        result_lb_V_we0 = ap_const_logic_0;
    }
}

void sobel_filter::thread_ret_V_1_fu_894_p2() {
    ret_V_1_fu_894_p2 = (!zext_ln1354_fu_891_p1.read().is_01() || !ap_const_lv17_1FFFF.is_01())? sc_lv<17>(): (sc_biguint<17>(zext_ln1354_fu_891_p1.read()) + sc_bigint<17>(ap_const_lv17_1FFFF));
}

void sobel_filter::thread_ret_V_fu_870_p2() {
    ret_V_fu_870_p2 = (!zext_ln1353_fu_866_p1.read().is_01() || !ap_const_lv17_3.is_01())? sc_lv<17>(): (sc_biguint<17>(zext_ln1353_fu_866_p1.read()) + sc_biguint<17>(ap_const_lv17_3));
}

void sobel_filter::thread_select_ln326_1_fu_4511_p3() {
    select_ln326_1_fu_4511_p3 = (!icmp_ln326_1_reg_6736.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_1_reg_6736.read()[0].to_bool())? ap_const_lv16_0: select_ln343_1_fu_4505_p3.read());
}

void sobel_filter::thread_select_ln326_2_fu_4597_p3() {
    select_ln326_2_fu_4597_p3 = (!icmp_ln326_2_reg_6804.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_2_reg_6804.read()[0].to_bool())? ap_const_lv16_0: select_ln343_2_fu_4591_p3.read());
}

void sobel_filter::thread_select_ln326_3_fu_4683_p3() {
    select_ln326_3_fu_4683_p3 = (!icmp_ln326_3_reg_6872.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_3_reg_6872.read()[0].to_bool())? ap_const_lv16_0: select_ln343_3_fu_4677_p3.read());
}

void sobel_filter::thread_select_ln326_4_fu_4769_p3() {
    select_ln326_4_fu_4769_p3 = (!icmp_ln326_4_reg_6940.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_4_reg_6940.read()[0].to_bool())? ap_const_lv16_0: select_ln343_4_fu_4763_p3.read());
}

void sobel_filter::thread_select_ln326_5_fu_4855_p3() {
    select_ln326_5_fu_4855_p3 = (!icmp_ln326_5_reg_7008.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_5_reg_7008.read()[0].to_bool())? ap_const_lv16_0: select_ln343_5_fu_4849_p3.read());
}

void sobel_filter::thread_select_ln326_6_fu_4941_p3() {
    select_ln326_6_fu_4941_p3 = (!icmp_ln326_6_reg_7082.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_6_reg_7082.read()[0].to_bool())? ap_const_lv16_0: select_ln343_6_fu_4935_p3.read());
}

void sobel_filter::thread_select_ln326_7_fu_5027_p3() {
    select_ln326_7_fu_5027_p3 = (!icmp_ln326_7_reg_7160.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_7_reg_7160.read()[0].to_bool())? ap_const_lv16_0: select_ln343_7_fu_5021_p3.read());
}

void sobel_filter::thread_select_ln326_fu_4100_p3() {
    select_ln326_fu_4100_p3 = (!icmp_ln326_reg_6663.read()[0].is_01())? sc_lv<16>(): ((icmp_ln326_reg_6663.read()[0].to_bool())? ap_const_lv16_0: select_ln343_fu_4094_p3.read());
}

void sobel_filter::thread_select_ln330_1_fu_4546_p3() {
    select_ln330_1_fu_4546_p3 = (!and_ln330_1_fu_4541_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_1_fu_4541_p2.read()[0].to_bool())? trunc_ln344_1_reg_6730.read(): select_ln333_3_fu_4528_p3.read());
}

void sobel_filter::thread_select_ln330_2_fu_4632_p3() {
    select_ln330_2_fu_4632_p3 = (!and_ln330_2_fu_4627_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_2_fu_4627_p2.read()[0].to_bool())? trunc_ln344_2_reg_6798.read(): select_ln333_5_fu_4614_p3.read());
}

void sobel_filter::thread_select_ln330_3_fu_4718_p3() {
    select_ln330_3_fu_4718_p3 = (!and_ln330_3_fu_4713_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_3_fu_4713_p2.read()[0].to_bool())? trunc_ln344_3_reg_6866.read(): select_ln333_7_fu_4700_p3.read());
}

void sobel_filter::thread_select_ln330_4_fu_4804_p3() {
    select_ln330_4_fu_4804_p3 = (!and_ln330_4_fu_4799_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_4_fu_4799_p2.read()[0].to_bool())? trunc_ln344_4_reg_6934.read(): select_ln333_9_fu_4786_p3.read());
}

void sobel_filter::thread_select_ln330_5_fu_4890_p3() {
    select_ln330_5_fu_4890_p3 = (!and_ln330_5_fu_4885_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_5_fu_4885_p2.read()[0].to_bool())? trunc_ln344_5_reg_7002.read(): select_ln333_11_fu_4872_p3.read());
}

void sobel_filter::thread_select_ln330_6_fu_4976_p3() {
    select_ln330_6_fu_4976_p3 = (!and_ln330_6_fu_4971_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_6_fu_4971_p2.read()[0].to_bool())? trunc_ln344_6_reg_7076.read(): select_ln333_13_fu_4958_p3.read());
}

void sobel_filter::thread_select_ln330_7_fu_5062_p3() {
    select_ln330_7_fu_5062_p3 = (!and_ln330_7_fu_5057_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_7_fu_5057_p2.read()[0].to_bool())? trunc_ln344_7_reg_7154.read(): select_ln333_15_fu_5044_p3.read());
}

void sobel_filter::thread_select_ln330_fu_4135_p3() {
    select_ln330_fu_4135_p3 = (!and_ln330_fu_4130_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln330_fu_4130_p2.read()[0].to_bool())? trunc_ln344_reg_6657.read(): select_ln333_1_fu_4117_p3.read());
}

void sobel_filter::thread_select_ln333_10_fu_4042_p3() {
    select_ln333_10_fu_4042_p3 = (!and_ln333_10_fu_4036_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_10_fu_4036_p2.read()[0].to_bool())? trunc_ln334_5_fu_4017_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_11_fu_4872_p3() {
    select_ln333_11_fu_4872_p3 = (!and_ln333_11_fu_4867_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_11_fu_4867_p2.read()[0].to_bool())? select_ln336_5_fu_4832_p3.read(): select_ln326_5_fu_4855_p3.read());
}

void sobel_filter::thread_select_ln333_12_fu_4291_p3() {
    select_ln333_12_fu_4291_p3 = (!and_ln333_12_fu_4285_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_12_fu_4285_p2.read()[0].to_bool())? trunc_ln334_6_fu_4266_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_13_fu_4958_p3() {
    select_ln333_13_fu_4958_p3 = (!and_ln333_13_fu_4953_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_13_fu_4953_p2.read()[0].to_bool())? select_ln336_6_fu_4918_p3.read(): select_ln326_6_fu_4941_p3.read());
}

void sobel_filter::thread_select_ln333_14_fu_4453_p3() {
    select_ln333_14_fu_4453_p3 = (!and_ln333_14_fu_4447_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_14_fu_4447_p2.read()[0].to_bool())? trunc_ln334_7_fu_4428_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_15_fu_5044_p3() {
    select_ln333_15_fu_5044_p3 = (!and_ln333_15_fu_5039_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_15_fu_5039_p2.read()[0].to_bool())? select_ln336_7_fu_5004_p3.read(): select_ln326_7_fu_5027_p3.read());
}

void sobel_filter::thread_select_ln333_1_fu_4117_p3() {
    select_ln333_1_fu_4117_p3 = (!and_ln333_1_fu_4112_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_1_fu_4112_p2.read()[0].to_bool())? select_ln336_fu_4077_p3.read(): select_ln326_fu_4100_p3.read());
}

void sobel_filter::thread_select_ln333_2_fu_3410_p3() {
    select_ln333_2_fu_3410_p3 = (!and_ln333_2_fu_3404_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_2_fu_3404_p2.read()[0].to_bool())? trunc_ln334_1_fu_3385_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_3_fu_4528_p3() {
    select_ln333_3_fu_4528_p3 = (!and_ln333_3_fu_4523_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_3_fu_4523_p2.read()[0].to_bool())? select_ln336_1_fu_4488_p3.read(): select_ln326_1_fu_4511_p3.read());
}

void sobel_filter::thread_select_ln333_4_fu_3568_p3() {
    select_ln333_4_fu_3568_p3 = (!and_ln333_4_fu_3562_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_4_fu_3562_p2.read()[0].to_bool())? trunc_ln334_2_fu_3543_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_5_fu_4614_p3() {
    select_ln333_5_fu_4614_p3 = (!and_ln333_5_fu_4609_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_5_fu_4609_p2.read()[0].to_bool())? select_ln336_2_fu_4574_p3.read(): select_ln326_2_fu_4597_p3.read());
}

void sobel_filter::thread_select_ln333_6_fu_3726_p3() {
    select_ln333_6_fu_3726_p3 = (!and_ln333_6_fu_3720_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_6_fu_3720_p2.read()[0].to_bool())? trunc_ln334_3_fu_3701_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_7_fu_4700_p3() {
    select_ln333_7_fu_4700_p3 = (!and_ln333_7_fu_4695_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_7_fu_4695_p2.read()[0].to_bool())? select_ln336_3_fu_4660_p3.read(): select_ln326_3_fu_4683_p3.read());
}

void sobel_filter::thread_select_ln333_8_fu_3884_p3() {
    select_ln333_8_fu_3884_p3 = (!and_ln333_8_fu_3878_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_8_fu_3878_p2.read()[0].to_bool())? trunc_ln334_4_fu_3859_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln333_9_fu_4786_p3() {
    select_ln333_9_fu_4786_p3 = (!and_ln333_9_fu_4781_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_9_fu_4781_p2.read()[0].to_bool())? select_ln336_4_fu_4746_p3.read(): select_ln326_4_fu_4769_p3.read());
}

void sobel_filter::thread_select_ln333_fu_3252_p3() {
    select_ln333_fu_3252_p3 = (!and_ln333_fu_3246_p2.read()[0].is_01())? sc_lv<16>(): ((and_ln333_fu_3246_p2.read()[0].to_bool())? trunc_ln334_fu_3227_p1.read(): ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_1_fu_4488_p3() {
    select_ln336_1_fu_4488_p3 = (!tmp_14_fu_4481_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_14_fu_4481_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_2_fu_4574_p3() {
    select_ln336_2_fu_4574_p3 = (!tmp_25_fu_4567_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_25_fu_4567_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_3_fu_4660_p3() {
    select_ln336_3_fu_4660_p3 = (!tmp_28_fu_4653_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_28_fu_4653_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_4_fu_4746_p3() {
    select_ln336_4_fu_4746_p3 = (!tmp_31_fu_4739_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_31_fu_4739_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_5_fu_4832_p3() {
    select_ln336_5_fu_4832_p3 = (!tmp_34_fu_4825_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_34_fu_4825_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_6_fu_4918_p3() {
    select_ln336_6_fu_4918_p3 = (!tmp_37_fu_4911_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_37_fu_4911_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_7_fu_5004_p3() {
    select_ln336_7_fu_5004_p3 = (!tmp_40_fu_4997_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_40_fu_4997_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln336_fu_4077_p3() {
    select_ln336_fu_4077_p3 = (!tmp_7_fu_4070_p3.read()[0].is_01())? sc_lv<16>(): ((tmp_7_fu_4070_p3.read()[0].to_bool())? ap_const_lv16_FFFF: ap_const_lv16_0);
}

void sobel_filter::thread_select_ln343_1_fu_4505_p3() {
    select_ln343_1_fu_4505_p3 = (!and_ln343_1_reg_6782.read()[0].is_01())? sc_lv<16>(): ((and_ln343_1_reg_6782.read()[0].to_bool())? shl_ln345_1_fu_4500_p2.read(): select_ln333_2_reg_6777.read());
}

void sobel_filter::thread_select_ln343_2_fu_4591_p3() {
    select_ln343_2_fu_4591_p3 = (!and_ln343_2_reg_6850.read()[0].is_01())? sc_lv<16>(): ((and_ln343_2_reg_6850.read()[0].to_bool())? shl_ln345_2_fu_4586_p2.read(): select_ln333_4_reg_6845.read());
}

void sobel_filter::thread_select_ln343_3_fu_4677_p3() {
    select_ln343_3_fu_4677_p3 = (!and_ln343_3_reg_6918.read()[0].is_01())? sc_lv<16>(): ((and_ln343_3_reg_6918.read()[0].to_bool())? shl_ln345_3_fu_4672_p2.read(): select_ln333_6_reg_6913.read());
}

void sobel_filter::thread_select_ln343_4_fu_4763_p3() {
    select_ln343_4_fu_4763_p3 = (!and_ln343_4_reg_6986.read()[0].is_01())? sc_lv<16>(): ((and_ln343_4_reg_6986.read()[0].to_bool())? shl_ln345_4_fu_4758_p2.read(): select_ln333_8_reg_6981.read());
}

void sobel_filter::thread_select_ln343_5_fu_4849_p3() {
    select_ln343_5_fu_4849_p3 = (!and_ln343_5_reg_7054.read()[0].is_01())? sc_lv<16>(): ((and_ln343_5_reg_7054.read()[0].to_bool())? shl_ln345_5_fu_4844_p2.read(): select_ln333_10_reg_7049.read());
}

void sobel_filter::thread_select_ln343_6_fu_4935_p3() {
    select_ln343_6_fu_4935_p3 = (!and_ln343_6_reg_7138.read()[0].is_01())? sc_lv<16>(): ((and_ln343_6_reg_7138.read()[0].to_bool())? shl_ln345_6_fu_4930_p2.read(): select_ln333_12_reg_7133.read());
}

void sobel_filter::thread_select_ln343_7_fu_5021_p3() {
    select_ln343_7_fu_5021_p3 = (!and_ln343_7_reg_7206.read()[0].is_01())? sc_lv<16>(): ((and_ln343_7_reg_7206.read()[0].to_bool())? shl_ln345_7_fu_5016_p2.read(): select_ln333_14_reg_7201.read());
}

void sobel_filter::thread_select_ln343_fu_4094_p3() {
    select_ln343_fu_4094_p3 = (!and_ln343_reg_6714.read()[0].is_01())? sc_lv<16>(): ((and_ln343_reg_6714.read()[0].to_bool())? shl_ln345_fu_4089_p2.read(): select_ln333_reg_6709.read());
}

void sobel_filter::thread_select_ln351_1_fu_4558_p3() {
    select_ln351_1_fu_4558_p3 = (!tmp_11_reg_6725.read()[0].is_01())? sc_lv<16>(): ((tmp_11_reg_6725.read()[0].to_bool())? sub_ln461_1_fu_4553_p2.read(): select_ln330_1_reg_7211.read());
}

void sobel_filter::thread_select_ln351_2_fu_4644_p3() {
    select_ln351_2_fu_4644_p3 = (!tmp_16_reg_6793.read()[0].is_01())? sc_lv<16>(): ((tmp_16_reg_6793.read()[0].to_bool())? sub_ln461_2_fu_4639_p2.read(): select_ln330_2_reg_7222.read());
}

void sobel_filter::thread_select_ln351_3_fu_4730_p3() {
    select_ln351_3_fu_4730_p3 = (!tmp_26_reg_6861.read()[0].is_01())? sc_lv<16>(): ((tmp_26_reg_6861.read()[0].to_bool())? sub_ln461_3_fu_4725_p2.read(): select_ln330_3_reg_7233.read());
}

void sobel_filter::thread_select_ln351_4_fu_4816_p3() {
    select_ln351_4_fu_4816_p3 = (!tmp_29_reg_6929.read()[0].is_01())? sc_lv<16>(): ((tmp_29_reg_6929.read()[0].to_bool())? sub_ln461_4_fu_4811_p2.read(): select_ln330_4_reg_7244.read());
}

void sobel_filter::thread_select_ln351_5_fu_4902_p3() {
    select_ln351_5_fu_4902_p3 = (!tmp_32_reg_6997.read()[0].is_01())? sc_lv<16>(): ((tmp_32_reg_6997.read()[0].to_bool())? sub_ln461_5_fu_4897_p2.read(): select_ln330_5_reg_7255.read());
}

void sobel_filter::thread_select_ln351_6_fu_4988_p3() {
    select_ln351_6_fu_4988_p3 = (!tmp_35_reg_7071.read()[0].is_01())? sc_lv<16>(): ((tmp_35_reg_7071.read()[0].to_bool())? sub_ln461_6_fu_4983_p2.read(): select_ln330_6_reg_7266.read());
}

void sobel_filter::thread_select_ln351_7_fu_5074_p3() {
    select_ln351_7_fu_5074_p3 = (!tmp_38_reg_7149.read()[0].is_01())? sc_lv<16>(): ((tmp_38_reg_7149.read()[0].to_bool())? sub_ln461_7_fu_5069_p2.read(): select_ln330_7_reg_7277.read());
}

void sobel_filter::thread_select_ln351_fu_4210_p3() {
    select_ln351_fu_4210_p3 = (!tmp_4_reg_6652.read()[0].is_01())? sc_lv<16>(): ((tmp_4_reg_6652.read()[0].to_bool())? sub_ln461_fu_4205_p2.read(): select_ln330_reg_7059.read());
}

void sobel_filter::thread_sext_ln162_1_fu_5120_p1() {
    sext_ln162_1_fu_5120_p1 = esl_sext<64,14>(add_ln162_1_reg_7301.read());
}

void sobel_filter::thread_sext_ln162_2_fu_5166_p1() {
    sext_ln162_2_fu_5166_p1 = esl_sext<64,14>(add_ln162_2_reg_7320.read());
}

void sobel_filter::thread_sext_ln162_3_fu_5170_p1() {
    sext_ln162_3_fu_5170_p1 = esl_sext<64,14>(add_ln162_3_reg_7329.read());
}

void sobel_filter::thread_sext_ln162_fu_5116_p1() {
    sext_ln162_fu_5116_p1 = esl_sext<64,14>(add_ln162_reg_7292.read());
}

void sobel_filter::thread_sext_ln215_10_fu_1812_p1() {
    sext_ln215_10_fu_1812_p1 = esl_sext<64,14>(add_ln215_10_reg_5976.read());
}

void sobel_filter::thread_sext_ln215_11_fu_1458_p1() {
    sext_ln215_11_fu_1458_p1 = esl_sext<64,14>(add_ln215_11_reg_5741.read());
}

void sobel_filter::thread_sext_ln215_12_fu_2395_p1() {
    sext_ln215_12_fu_2395_p1 = esl_sext<64,14>(add_ln215_12_reg_6181.read());
}

void sobel_filter::thread_sext_ln215_13_fu_1606_p1() {
    sext_ln215_13_fu_1606_p1 = esl_sext<64,14>(add_ln215_13_reg_5864.read());
}

void sobel_filter::thread_sext_ln215_14_fu_1940_p1() {
    sext_ln215_14_fu_1940_p1 = esl_sext<64,14>(add_ln215_14_reg_6021.read());
}

void sobel_filter::thread_sext_ln215_15_fu_1462_p1() {
    sext_ln215_15_fu_1462_p1 = esl_sext<64,14>(add_ln215_15_reg_5746.read());
}

void sobel_filter::thread_sext_ln215_16_fu_2554_p1() {
    sext_ln215_16_fu_2554_p1 = esl_sext<64,14>(add_ln215_16_reg_6251.read());
}

void sobel_filter::thread_sext_ln215_17_fu_1610_p1() {
    sext_ln215_17_fu_1610_p1 = esl_sext<64,14>(add_ln215_17_reg_5869.read());
}

void sobel_filter::thread_sext_ln215_18_fu_1948_p1() {
    sext_ln215_18_fu_1948_p1 = esl_sext<64,14>(add_ln215_18_reg_6036.read());
}

void sobel_filter::thread_sext_ln215_19_fu_1516_p1() {
    sext_ln215_19_fu_1516_p1 = esl_sext<64,14>(add_ln215_19_reg_5771.read());
}

void sobel_filter::thread_sext_ln215_1_fu_1715_p1() {
    sext_ln215_1_fu_1715_p1 = esl_sext<64,14>(add_ln215_1_reg_5921.read());
}

void sobel_filter::thread_sext_ln215_20_fu_2558_p1() {
    sext_ln215_20_fu_2558_p1 = esl_sext<64,14>(add_ln215_20_reg_6256.read());
}

void sobel_filter::thread_sext_ln215_21_fu_1702_p1() {
    sext_ln215_21_fu_1702_p1 = esl_sext<64,14>(add_ln215_21_reg_5911.read());
}

void sobel_filter::thread_sext_ln215_22_fu_2128_p1() {
    sext_ln215_22_fu_2128_p1 = esl_sext<64,14>(add_ln215_22_reg_6081.read());
}

void sobel_filter::thread_sext_ln215_23_fu_1520_p1() {
    sext_ln215_23_fu_1520_p1 = esl_sext<64,14>(add_ln215_23_reg_5776.read());
}

void sobel_filter::thread_sext_ln215_24_fu_2672_p1() {
    sext_ln215_24_fu_2672_p1 = esl_sext<64,14>(add_ln215_24_reg_6311.read());
}

void sobel_filter::thread_sext_ln215_25_fu_1706_p1() {
    sext_ln215_25_fu_1706_p1 = esl_sext<64,14>(add_ln215_25_reg_5916.read());
}

void sobel_filter::thread_sext_ln215_26_fu_2171_p1() {
    sext_ln215_26_fu_2171_p1 = esl_sext<64,14>(add_ln215_26_reg_6096.read());
}

void sobel_filter::thread_sext_ln215_27_fu_1548_p1() {
    sext_ln215_27_fu_1548_p1 = esl_sext<64,14>(add_ln215_27_reg_5807.read());
}

void sobel_filter::thread_sext_ln215_28_fu_2676_p1() {
    sext_ln215_28_fu_2676_p1 = esl_sext<64,14>(add_ln215_28_reg_6316.read());
}

void sobel_filter::thread_sext_ln215_29_fu_1804_p1() {
    sext_ln215_29_fu_1804_p1 = esl_sext<64,14>(add_ln215_29_reg_5956.read());
}

void sobel_filter::thread_sext_ln215_2_fu_1394_p1() {
    sext_ln215_2_fu_1394_p1 = esl_sext<64,14>(add_ln215_2_reg_5667.read());
}

void sobel_filter::thread_sext_ln215_30_fu_2358_p1() {
    sext_ln215_30_fu_2358_p1 = esl_sext<64,14>(add_ln215_30_reg_6156.read());
}

void sobel_filter::thread_sext_ln215_31_fu_1552_p1() {
    sext_ln215_31_fu_1552_p1 = esl_sext<64,14>(add_ln215_31_reg_5812.read());
}

void sobel_filter::thread_sext_ln215_32_fu_2753_p1() {
    sext_ln215_32_fu_2753_p1 = esl_sext<64,14>(add_ln215_32_reg_6371.read());
}

void sobel_filter::thread_sext_ln215_3_fu_1408_p1() {
    sext_ln215_3_fu_1408_p1 = esl_sext<64,14>(add_ln215_3_reg_5704.read());
}

void sobel_filter::thread_sext_ln215_4_fu_2209_p1() {
    sext_ln215_4_fu_2209_p1 = esl_sext<64,14>(add_ln215_4_reg_6101.read());
}

void sobel_filter::thread_sext_ln215_5_fu_1556_p1() {
    sext_ln215_5_fu_1556_p1 = esl_sext<64,14>(add_ln215_5_reg_5822.read());
}

void sobel_filter::thread_sext_ln215_6_fu_1808_p1() {
    sext_ln215_6_fu_1808_p1 = esl_sext<64,14>(add_ln215_6_reg_5966.read());
}

void sobel_filter::thread_sext_ln215_7_fu_1412_p1() {
    sext_ln215_7_fu_1412_p1 = esl_sext<64,14>(add_ln215_7_reg_5709.read());
}

void sobel_filter::thread_sext_ln215_8_fu_2391_p1() {
    sext_ln215_8_fu_2391_p1 = esl_sext<64,14>(add_ln215_8_reg_6176.read());
}

void sobel_filter::thread_sext_ln215_9_fu_1560_p1() {
    sext_ln215_9_fu_1560_p1 = esl_sext<64,14>(add_ln215_9_reg_5827.read());
}

void sobel_filter::thread_sext_ln215_fu_1390_p1() {
    sext_ln215_fu_1390_p1 = esl_sext<64,14>(add_ln215_reg_5662.read());
}

void sobel_filter::thread_sext_ln321_1_fu_932_p1() {
    sext_ln321_1_fu_932_p1 = esl_sext<64,14>(add_ln321_1_fu_926_p2.read());
}

void sobel_filter::thread_sext_ln321_2_fu_943_p1() {
    sext_ln321_2_fu_943_p1 = esl_sext<64,14>(add_ln321_2_fu_937_p2.read());
}

void sobel_filter::thread_sext_ln321_3_fu_969_p1() {
    sext_ln321_3_fu_969_p1 = esl_sext<64,14>(add_ln321_3_fu_963_p2.read());
}

void sobel_filter::thread_sext_ln321_4_fu_980_p1() {
    sext_ln321_4_fu_980_p1 = esl_sext<64,14>(add_ln321_4_fu_974_p2.read());
}

void sobel_filter::thread_sext_ln321_5_fu_1006_p1() {
    sext_ln321_5_fu_1006_p1 = esl_sext<64,14>(add_ln321_5_fu_1000_p2.read());
}

void sobel_filter::thread_sext_ln321_6_fu_1017_p1() {
    sext_ln321_6_fu_1017_p1 = esl_sext<64,14>(add_ln321_6_fu_1011_p2.read());
}

void sobel_filter::thread_sext_ln321_7_fu_1043_p1() {
    sext_ln321_7_fu_1043_p1 = esl_sext<64,14>(add_ln321_7_fu_1037_p2.read());
}

void sobel_filter::thread_sext_ln321_8_fu_1054_p1() {
    sext_ln321_8_fu_1054_p1 = esl_sext<64,14>(add_ln321_8_fu_1048_p2.read());
}

void sobel_filter::thread_sext_ln321_fu_4374_p1() {
    sext_ln321_fu_4374_p1 = esl_sext<64,14>(add_ln321_reg_7108.read());
}

void sobel_filter::thread_sext_ln329_1_fu_3346_p1() {
    sext_ln329_1_fu_3346_p1 = esl_sext<32,12>(sub_ln329_1_reg_6743.read());
}

void sobel_filter::thread_sext_ln329_2_fu_3504_p1() {
    sext_ln329_2_fu_3504_p1 = esl_sext<32,12>(sub_ln329_2_reg_6811.read());
}

void sobel_filter::thread_sext_ln329_3_fu_3662_p1() {
    sext_ln329_3_fu_3662_p1 = esl_sext<32,12>(sub_ln329_3_reg_6879.read());
}

void sobel_filter::thread_sext_ln329_4_fu_3820_p1() {
    sext_ln329_4_fu_3820_p1 = esl_sext<32,12>(sub_ln329_4_reg_6947.read());
}

void sobel_filter::thread_sext_ln329_5_fu_3978_p1() {
    sext_ln329_5_fu_3978_p1 = esl_sext<32,12>(sub_ln329_5_reg_7015.read());
}

void sobel_filter::thread_sext_ln329_6_fu_4227_p1() {
    sext_ln329_6_fu_4227_p1 = esl_sext<32,12>(sub_ln329_6_reg_7089.read());
}

void sobel_filter::thread_sext_ln329_7_fu_4389_p1() {
    sext_ln329_7_fu_4389_p1 = esl_sext<32,12>(sub_ln329_7_reg_7167.read());
}

void sobel_filter::thread_sext_ln329_fu_3188_p1() {
    sext_ln329_fu_3188_p1 = esl_sext<32,12>(sub_ln329_reg_6670.read());
}

void sobel_filter::thread_sext_ln342_1_fu_4478_p1() {
    sext_ln342_1_fu_4478_p1 = esl_sext<32,12>(sub_ln342_1_reg_6767.read());
}

void sobel_filter::thread_sext_ln342_1cast_fu_4496_p1() {
    sext_ln342_1cast_fu_4496_p1 = sext_ln342_1_fu_4478_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_2_fu_4564_p1() {
    sext_ln342_2_fu_4564_p1 = esl_sext<32,12>(sub_ln342_2_reg_6835.read());
}

void sobel_filter::thread_sext_ln342_2cast_fu_4582_p1() {
    sext_ln342_2cast_fu_4582_p1 = sext_ln342_2_fu_4564_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_3_fu_4650_p1() {
    sext_ln342_3_fu_4650_p1 = esl_sext<32,12>(sub_ln342_3_reg_6903.read());
}

void sobel_filter::thread_sext_ln342_3cast_fu_4668_p1() {
    sext_ln342_3cast_fu_4668_p1 = sext_ln342_3_fu_4650_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_4_fu_4736_p1() {
    sext_ln342_4_fu_4736_p1 = esl_sext<32,12>(sub_ln342_4_reg_6971.read());
}

void sobel_filter::thread_sext_ln342_4cast_fu_4754_p1() {
    sext_ln342_4cast_fu_4754_p1 = sext_ln342_4_fu_4736_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_5_fu_4822_p1() {
    sext_ln342_5_fu_4822_p1 = esl_sext<32,12>(sub_ln342_5_reg_7039.read());
}

void sobel_filter::thread_sext_ln342_5cast_fu_4840_p1() {
    sext_ln342_5cast_fu_4840_p1 = sext_ln342_5_fu_4822_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_6_fu_4908_p1() {
    sext_ln342_6_fu_4908_p1 = esl_sext<32,12>(sub_ln342_6_reg_7123.read());
}

void sobel_filter::thread_sext_ln342_6cast_fu_4926_p1() {
    sext_ln342_6cast_fu_4926_p1 = sext_ln342_6_fu_4908_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_7_fu_4994_p1() {
    sext_ln342_7_fu_4994_p1 = esl_sext<32,12>(sub_ln342_7_reg_7191.read());
}

void sobel_filter::thread_sext_ln342_7cast_fu_5012_p1() {
    sext_ln342_7cast_fu_5012_p1 = sext_ln342_7_fu_4994_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln342_fu_4067_p1() {
    sext_ln342_fu_4067_p1 = esl_sext<32,12>(sub_ln342_reg_6699.read());
}

void sobel_filter::thread_sext_ln342cast_fu_4085_p1() {
    sext_ln342cast_fu_4085_p1 = sext_ln342_fu_4067_p1.read().range(16-1, 0);
}

void sobel_filter::thread_sext_ln47_1_fu_1724_p1() {
    sext_ln47_1_fu_1724_p1 = esl_sext<32,19>(sub_ln1354_8_reg_5931.read());
}

void sobel_filter::thread_sext_ln47_2_fu_1816_p1() {
    sext_ln47_2_fu_1816_p1 = esl_sext<32,19>(sub_ln1354_14_reg_5986.read());
}

void sobel_filter::thread_sext_ln47_3_fu_1944_p1() {
    sext_ln47_3_fu_1944_p1 = esl_sext<32,19>(sub_ln1354_20_reg_6031.read());
}

void sobel_filter::thread_sext_ln47_4_fu_2124_p1() {
    sext_ln47_4_fu_2124_p1 = esl_sext<32,19>(sub_ln1354_26_reg_6076.read());
}

void sobel_filter::thread_sext_ln47_5_fu_2315_p1() {
    sext_ln47_5_fu_2315_p1 = esl_sext<32,19>(sub_ln1354_32_reg_6136.read());
}

void sobel_filter::thread_sext_ln47_6_fu_2449_p1() {
    sext_ln47_6_fu_2449_p1 = esl_sext<32,19>(sub_ln1354_38_reg_6206.read());
}

void sobel_filter::thread_sext_ln47_7_fu_2572_p1() {
    sext_ln47_7_fu_2572_p1 = esl_sext<32,19>(sub_ln1354_44_reg_6281.read());
}

void sobel_filter::thread_sext_ln47_fu_1534_p1() {
    sext_ln47_fu_1534_p1 = esl_sext<32,19>(sub_ln1354_2_reg_5786.read());
}

void sobel_filter::thread_sext_ln49_1_fu_2697_p1() {
    sext_ln49_1_fu_2697_p1 = esl_sext<32,19>(sub_ln1354_11_reg_6341.read());
}

void sobel_filter::thread_sext_ln49_2_fu_2769_p1() {
    sext_ln49_2_fu_2769_p1 = esl_sext<32,19>(sub_ln1354_17_reg_6346.read());
}

void sobel_filter::thread_sext_ln49_3_fu_2837_p1() {
    sext_ln49_3_fu_2837_p1 = esl_sext<32,19>(sub_ln1354_23_reg_6391.read());
}

void sobel_filter::thread_sext_ln49_4_fu_2883_p1() {
    sext_ln49_4_fu_2883_p1 = esl_sext<32,19>(sub_ln1354_29_reg_6396.read());
}

void sobel_filter::thread_sext_ln49_5_fu_2907_p1() {
    sext_ln49_5_fu_2907_p1 = esl_sext<32,19>(sub_ln1354_35_reg_6426.read());
}

void sobel_filter::thread_sext_ln49_6_fu_2931_p1() {
    sext_ln49_6_fu_2931_p1 = esl_sext<32,19>(sub_ln1354_41_reg_6431.read());
}

void sobel_filter::thread_sext_ln49_7_fu_2963_p1() {
    sext_ln49_7_fu_2963_p1 = esl_sext<32,19>(sub_ln1354_47_reg_6456.read());
}

void sobel_filter::thread_sext_ln49_fu_2616_p1() {
    sext_ln49_fu_2616_p1 = esl_sext<32,19>(sub_ln1354_5_reg_6286.read());
}

void sobel_filter::thread_sext_ln544_1_fu_1152_p1() {
    sext_ln544_1_fu_1152_p1 = esl_sext<64,17>(add_ln1354_1_reg_5385.read());
}

void sobel_filter::thread_sext_ln544_2_fu_1188_p1() {
    sext_ln544_2_fu_1188_p1 = esl_sext<64,17>(add_ln1354_2_reg_5422.read());
}

void sobel_filter::thread_sext_ln544_3_fu_1224_p1() {
    sext_ln544_3_fu_1224_p1 = esl_sext<64,17>(add_ln1354_3_reg_5459.read());
}

void sobel_filter::thread_sext_ln544_4_fu_1260_p1() {
    sext_ln544_4_fu_1260_p1 = esl_sext<64,17>(add_ln1354_4_reg_5496.read());
}

void sobel_filter::thread_sext_ln544_5_fu_1300_p1() {
    sext_ln544_5_fu_1300_p1 = esl_sext<64,17>(add_ln1354_5_reg_5540.read());
}

void sobel_filter::thread_sext_ln544_6_fu_1336_p1() {
    sext_ln544_6_fu_1336_p1 = esl_sext<64,17>(add_ln1354_6_reg_5590.read());
}

void sobel_filter::thread_sext_ln544_7_fu_1382_p1() {
    sext_ln544_7_fu_1382_p1 = esl_sext<64,17>(add_ln1354_7_reg_5639.read());
}

void sobel_filter::thread_sext_ln544_fu_1116_p1() {
    sext_ln544_fu_1116_p1 = esl_sext<64,17>(add_ln1354_reg_5343.read());
}

void sobel_filter::thread_shl_ln1352_10_fu_2650_p3() {
    shl_ln1352_10_fu_2650_p3 = esl_concat<16,1>(reg_783.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_11_fu_1773_p3() {
    shl_ln1352_11_fu_1773_p3 = esl_concat<16,1>(reg_819.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_12_fu_1838_p3() {
    shl_ln1352_12_fu_1838_p3 = esl_concat<16,1>(reg_836.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_13_fu_2235_p3() {
    shl_ln1352_13_fu_2235_p3 = esl_concat<16,1>(reg_789.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_14_fu_2709_p3() {
    shl_ln1352_14_fu_2709_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_15_fu_1870_p3() {
    shl_ln1352_15_fu_1870_p3 = esl_concat<16,1>(LineBuffer_V_load_29_reg_5519.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_16_fu_1965_p3() {
    shl_ln1352_16_fu_1965_p3 = esl_concat<16,1>(LineBuffer_V_load_31_reg_5791.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_17_fu_2275_p3() {
    shl_ln1352_17_fu_2275_p3 = esl_concat<16,1>(reg_795.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_18_fu_2731_p3() {
    shl_ln1352_18_fu_2731_p3 = esl_concat<16,1>(reg_783.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_19_fu_1996_p3() {
    shl_ln1352_19_fu_1996_p3 = esl_concat<16,1>(LineBuffer_V_load_35_reg_5569.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_1_fu_1488_p3() {
    shl_ln1352_1_fu_1488_p3 = esl_concat<16,1>(reg_836.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_20_fu_2145_p3() {
    shl_ln1352_20_fu_2145_p3 = esl_concat<16,1>(LineBuffer_V_load_37_reg_5832.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_21_fu_2409_p3() {
    shl_ln1352_21_fu_2409_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_22_fu_2781_p3() {
    shl_ln1352_22_fu_2781_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_23_fu_2175_p3() {
    shl_ln1352_23_fu_2175_p3 = esl_concat<16,1>(LineBuffer_V_load_41_reg_5618.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_24_fu_2332_p3() {
    shl_ln1352_24_fu_2332_p3 = esl_concat<16,1>(LineBuffer_V_load_43_reg_5838.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_25_fu_2453_p3() {
    shl_ln1352_25_fu_2453_p3 = esl_concat<16,1>(reg_789.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_26_fu_2803_p3() {
    shl_ln1352_26_fu_2803_p3 = esl_concat<16,1>(reg_783.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_27_fu_2362_p3() {
    shl_ln1352_27_fu_2362_p3 = esl_concat<16,1>(LineBuffer_V_load_47_reg_5677.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_28_fu_2506_p3() {
    shl_ln1352_28_fu_2506_p3 = esl_concat<16,1>(LineBuffer_V_load_49_reg_5874.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_29_fu_2576_p3() {
    shl_ln1352_29_fu_2576_p3 = esl_concat<16,1>(reg_783.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_2_fu_1900_p3() {
    shl_ln1352_2_fu_1900_p3 = esl_concat<16,1>(reg_831.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_30_fu_2849_p3() {
    shl_ln1352_30_fu_2849_p3 = esl_concat<16,1>(reg_783.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_3_fu_2532_p3() {
    shl_ln1352_3_fu_2532_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_4_fu_1574_p3() {
    shl_ln1352_4_fu_1574_p3 = esl_concat<16,1>(reg_789.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_5_fu_1642_p3() {
    shl_ln1352_5_fu_1642_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_6_fu_2043_p3() {
    shl_ln1352_6_fu_2043_p3 = esl_concat<16,1>(reg_789.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_7_fu_2628_p3() {
    shl_ln1352_7_fu_2628_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_8_fu_1670_p3() {
    shl_ln1352_8_fu_1670_p3 = esl_concat<16,1>(reg_807.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_9_fu_1745_p3() {
    shl_ln1352_9_fu_1745_p3 = esl_concat<16,1>(reg_783.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln1352_s_fu_2084_p3() {
    shl_ln1352_s_fu_2084_p3 = esl_concat<16,1>(reg_831.read(), ap_const_lv1_0);
}

void sobel_filter::thread_shl_ln345_1_fu_4500_p2() {
    shl_ln345_1_fu_4500_p2 = (!sext_ln342_1cast_fu_4496_p1.read().is_01())? sc_lv<16>(): trunc_ln344_1_reg_6730.read() << (unsigned short)sext_ln342_1cast_fu_4496_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_2_fu_4586_p2() {
    shl_ln345_2_fu_4586_p2 = (!sext_ln342_2cast_fu_4582_p1.read().is_01())? sc_lv<16>(): trunc_ln344_2_reg_6798.read() << (unsigned short)sext_ln342_2cast_fu_4582_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_3_fu_4672_p2() {
    shl_ln345_3_fu_4672_p2 = (!sext_ln342_3cast_fu_4668_p1.read().is_01())? sc_lv<16>(): trunc_ln344_3_reg_6866.read() << (unsigned short)sext_ln342_3cast_fu_4668_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_4_fu_4758_p2() {
    shl_ln345_4_fu_4758_p2 = (!sext_ln342_4cast_fu_4754_p1.read().is_01())? sc_lv<16>(): trunc_ln344_4_reg_6934.read() << (unsigned short)sext_ln342_4cast_fu_4754_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_5_fu_4844_p2() {
    shl_ln345_5_fu_4844_p2 = (!sext_ln342_5cast_fu_4840_p1.read().is_01())? sc_lv<16>(): trunc_ln344_5_reg_7002.read() << (unsigned short)sext_ln342_5cast_fu_4840_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_6_fu_4930_p2() {
    shl_ln345_6_fu_4930_p2 = (!sext_ln342_6cast_fu_4926_p1.read().is_01())? sc_lv<16>(): trunc_ln344_6_reg_7076.read() << (unsigned short)sext_ln342_6cast_fu_4926_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_7_fu_5016_p2() {
    shl_ln345_7_fu_5016_p2 = (!sext_ln342_7cast_fu_5012_p1.read().is_01())? sc_lv<16>(): trunc_ln344_7_reg_7154.read() << (unsigned short)sext_ln342_7cast_fu_5012_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln345_fu_4089_p2() {
    shl_ln345_fu_4089_p2 = (!sext_ln342cast_fu_4085_p1.read().is_01())? sc_lv<16>(): trunc_ln344_reg_6657.read() << (unsigned short)sext_ln342cast_fu_4085_p1.read().to_uint();
}

void sobel_filter::thread_shl_ln_fu_1426_p3() {
    shl_ln_fu_1426_p3 = esl_concat<16,1>(reg_778.read(), ap_const_lv1_0);
}

void sobel_filter::thread_sub_ln1354_10_fu_2640_p2() {
    sub_ln1354_10_fu_2640_p2 = (!sub_ln1354_9_reg_6111.read().is_01() || !zext_ln1354_10_fu_2636_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_9_reg_6111.read()) - sc_biguint<19>(zext_ln1354_10_fu_2636_p1.read()));
}

void sobel_filter::thread_sub_ln1354_11_fu_2645_p2() {
    sub_ln1354_11_fu_2645_p2 = (!sub_ln1354_10_fu_2640_p2.read().is_01() || !zext_ln1354_8_reg_5926.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_10_fu_2640_p2.read()) - sc_biguint<19>(zext_ln1354_8_reg_5926.read()));
}

void sobel_filter::thread_sub_ln1354_12_fu_1739_p2() {
    sub_ln1354_12_fu_1739_p2 = (!zext_ln215_7_fu_1733_p1.read().is_01() || !zext_ln1354_11_fu_1736_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_7_fu_1733_p1.read()) - sc_biguint<19>(zext_ln1354_11_fu_1736_p1.read()));
}

void sobel_filter::thread_sub_ln1354_13_fu_1757_p2() {
    sub_ln1354_13_fu_1757_p2 = (!sub_ln1354_12_fu_1739_p2.read().is_01() || !zext_ln1354_12_fu_1753_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_12_fu_1739_p2.read()) - sc_biguint<19>(zext_ln1354_12_fu_1753_p1.read()));
}

void sobel_filter::thread_sub_ln1354_14_fu_1767_p2() {
    sub_ln1354_14_fu_1767_p2 = (!sub_ln1354_13_fu_1757_p2.read().is_01() || !zext_ln1354_13_fu_1763_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_13_fu_1757_p2.read()) - sc_biguint<19>(zext_ln1354_13_fu_1763_p1.read()));
}

void sobel_filter::thread_sub_ln1354_15_fu_2118_p2() {
    sub_ln1354_15_fu_2118_p2 = (!zext_ln215_8_fu_2110_p1.read().is_01() || !zext_ln1354_14_fu_2114_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_8_fu_2110_p1.read()) - sc_biguint<19>(zext_ln1354_14_fu_2114_p1.read()));
}

void sobel_filter::thread_sub_ln1354_16_fu_2662_p2() {
    sub_ln1354_16_fu_2662_p2 = (!sub_ln1354_15_reg_6116.read().is_01() || !zext_ln1354_15_fu_2658_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_15_reg_6116.read()) - sc_biguint<19>(zext_ln1354_15_fu_2658_p1.read()));
}

void sobel_filter::thread_sub_ln1354_17_fu_2667_p2() {
    sub_ln1354_17_fu_2667_p2 = (!sub_ln1354_16_fu_2662_p2.read().is_01() || !zext_ln1354_13_reg_5981.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_16_fu_2662_p2.read()) - sc_biguint<19>(zext_ln1354_13_reg_5981.read()));
}

void sobel_filter::thread_sub_ln1354_18_fu_1832_p2() {
    sub_ln1354_18_fu_1832_p2 = (!zext_ln215_10_fu_1825_p1.read().is_01() || !zext_ln1354_16_fu_1828_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_10_fu_1825_p1.read()) - sc_biguint<19>(zext_ln1354_16_fu_1828_p1.read()));
}

void sobel_filter::thread_sub_ln1354_19_fu_1850_p2() {
    sub_ln1354_19_fu_1850_p2 = (!sub_ln1354_18_fu_1832_p2.read().is_01() || !zext_ln1354_17_fu_1846_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_18_fu_1832_p2.read()) - sc_biguint<19>(zext_ln1354_17_fu_1846_p1.read()));
}

void sobel_filter::thread_sub_ln1354_1_fu_1500_p2() {
    sub_ln1354_1_fu_1500_p2 = (!sub_ln1354_fu_1482_p2.read().is_01() || !zext_ln1354_2_fu_1496_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_fu_1482_p2.read()) - sc_biguint<19>(zext_ln1354_2_fu_1496_p1.read()));
}

void sobel_filter::thread_sub_ln1354_20_fu_1859_p2() {
    sub_ln1354_20_fu_1859_p2 = (!sub_ln1354_19_fu_1850_p2.read().is_01() || !zext_ln1354_18_fu_1856_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_19_fu_1850_p2.read()) - sc_biguint<19>(zext_ln1354_18_fu_1856_p1.read()));
}

void sobel_filter::thread_sub_ln1354_21_fu_2269_p2() {
    sub_ln1354_21_fu_2269_p2 = (!zext_ln215_11_fu_2262_p1.read().is_01() || !zext_ln1354_19_fu_2266_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_11_fu_2262_p1.read()) - sc_biguint<19>(zext_ln1354_19_fu_2266_p1.read()));
}

void sobel_filter::thread_sub_ln1354_22_fu_2721_p2() {
    sub_ln1354_22_fu_2721_p2 = (!sub_ln1354_21_reg_6186.read().is_01() || !zext_ln1354_20_fu_2717_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_21_reg_6186.read()) - sc_biguint<19>(zext_ln1354_20_fu_2717_p1.read()));
}

void sobel_filter::thread_sub_ln1354_23_fu_2726_p2() {
    sub_ln1354_23_fu_2726_p2 = (!sub_ln1354_22_fu_2721_p2.read().is_01() || !zext_ln1354_18_reg_6026.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_22_fu_2721_p2.read()) - sc_biguint<19>(zext_ln1354_18_reg_6026.read()));
}

void sobel_filter::thread_sub_ln1354_24_fu_1959_p2() {
    sub_ln1354_24_fu_1959_p2 = (!zext_ln215_13_fu_1952_p1.read().is_01() || !zext_ln1354_21_fu_1955_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_13_fu_1952_p1.read()) - sc_biguint<19>(zext_ln1354_21_fu_1955_p1.read()));
}

void sobel_filter::thread_sub_ln1354_25_fu_1976_p2() {
    sub_ln1354_25_fu_1976_p2 = (!sub_ln1354_24_fu_1959_p2.read().is_01() || !zext_ln1354_22_fu_1972_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_24_fu_1959_p2.read()) - sc_biguint<19>(zext_ln1354_22_fu_1972_p1.read()));
}

void sobel_filter::thread_sub_ln1354_26_fu_1985_p2() {
    sub_ln1354_26_fu_1985_p2 = (!sub_ln1354_25_fu_1976_p2.read().is_01() || !zext_ln1354_23_fu_1982_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_25_fu_1976_p2.read()) - sc_biguint<19>(zext_ln1354_23_fu_1982_p1.read()));
}

void sobel_filter::thread_sub_ln1354_27_fu_2309_p2() {
    sub_ln1354_27_fu_2309_p2 = (!zext_ln215_14_fu_2302_p1.read().is_01() || !zext_ln1354_24_fu_2306_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_14_fu_2302_p1.read()) - sc_biguint<19>(zext_ln1354_24_fu_2306_p1.read()));
}

void sobel_filter::thread_sub_ln1354_28_fu_2743_p2() {
    sub_ln1354_28_fu_2743_p2 = (!sub_ln1354_27_reg_6191.read().is_01() || !zext_ln1354_25_fu_2739_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_27_reg_6191.read()) - sc_biguint<19>(zext_ln1354_25_fu_2739_p1.read()));
}

void sobel_filter::thread_sub_ln1354_29_fu_2748_p2() {
    sub_ln1354_29_fu_2748_p2 = (!sub_ln1354_28_fu_2743_p2.read().is_01() || !zext_ln1354_23_reg_6071.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_28_fu_2743_p2.read()) - sc_biguint<19>(zext_ln1354_23_reg_6071.read()));
}

void sobel_filter::thread_sub_ln1354_2_fu_1510_p2() {
    sub_ln1354_2_fu_1510_p2 = (!sub_ln1354_1_fu_1500_p2.read().is_01() || !zext_ln1354_3_fu_1506_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_1_fu_1500_p2.read()) - sc_biguint<19>(zext_ln1354_3_fu_1506_p1.read()));
}

void sobel_filter::thread_sub_ln1354_30_fu_2139_p2() {
    sub_ln1354_30_fu_2139_p2 = (!zext_ln215_16_fu_2132_p1.read().is_01() || !zext_ln1354_26_fu_2135_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_16_fu_2132_p1.read()) - sc_biguint<19>(zext_ln1354_26_fu_2135_p1.read()));
}

void sobel_filter::thread_sub_ln1354_31_fu_2156_p2() {
    sub_ln1354_31_fu_2156_p2 = (!sub_ln1354_30_fu_2139_p2.read().is_01() || !zext_ln1354_27_fu_2152_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_30_fu_2139_p2.read()) - sc_biguint<19>(zext_ln1354_27_fu_2152_p1.read()));
}

void sobel_filter::thread_sub_ln1354_32_fu_2165_p2() {
    sub_ln1354_32_fu_2165_p2 = (!sub_ln1354_31_fu_2156_p2.read().is_01() || !zext_ln1354_28_fu_2162_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_31_fu_2156_p2.read()) - sc_biguint<19>(zext_ln1354_28_fu_2162_p1.read()));
}

void sobel_filter::thread_sub_ln1354_33_fu_2443_p2() {
    sub_ln1354_33_fu_2443_p2 = (!zext_ln215_17_fu_2436_p1.read().is_01() || !zext_ln1354_29_fu_2440_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_17_fu_2436_p1.read()) - sc_biguint<19>(zext_ln1354_29_fu_2440_p1.read()));
}

void sobel_filter::thread_sub_ln1354_34_fu_2793_p2() {
    sub_ln1354_34_fu_2793_p2 = (!sub_ln1354_33_reg_6261.read().is_01() || !zext_ln1354_30_fu_2789_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_33_reg_6261.read()) - sc_biguint<19>(zext_ln1354_30_fu_2789_p1.read()));
}

void sobel_filter::thread_sub_ln1354_35_fu_2798_p2() {
    sub_ln1354_35_fu_2798_p2 = (!sub_ln1354_34_fu_2793_p2.read().is_01() || !zext_ln1354_28_reg_6131.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_34_fu_2793_p2.read()) - sc_biguint<19>(zext_ln1354_28_reg_6131.read()));
}

void sobel_filter::thread_sub_ln1354_36_fu_2326_p2() {
    sub_ln1354_36_fu_2326_p2 = (!zext_ln215_19_fu_2319_p1.read().is_01() || !zext_ln1354_31_fu_2322_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_19_fu_2319_p1.read()) - sc_biguint<19>(zext_ln1354_31_fu_2322_p1.read()));
}

void sobel_filter::thread_sub_ln1354_37_fu_2343_p2() {
    sub_ln1354_37_fu_2343_p2 = (!sub_ln1354_36_fu_2326_p2.read().is_01() || !zext_ln1354_32_fu_2339_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_36_fu_2326_p2.read()) - sc_biguint<19>(zext_ln1354_32_fu_2339_p1.read()));
}

void sobel_filter::thread_sub_ln1354_38_fu_2352_p2() {
    sub_ln1354_38_fu_2352_p2 = (!sub_ln1354_37_fu_2343_p2.read().is_01() || !zext_ln1354_33_fu_2349_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_37_fu_2343_p2.read()) - sc_biguint<19>(zext_ln1354_33_fu_2349_p1.read()));
}

void sobel_filter::thread_sub_ln1354_39_fu_2487_p2() {
    sub_ln1354_39_fu_2487_p2 = (!zext_ln215_20_fu_2480_p1.read().is_01() || !zext_ln1354_34_fu_2484_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_20_fu_2480_p1.read()) - sc_biguint<19>(zext_ln1354_34_fu_2484_p1.read()));
}

void sobel_filter::thread_sub_ln1354_3_fu_1934_p2() {
    sub_ln1354_3_fu_1934_p2 = (!zext_ln215_2_fu_1926_p1.read().is_01() || !zext_ln1354_4_fu_1930_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_2_fu_1926_p1.read()) - sc_biguint<19>(zext_ln1354_4_fu_1930_p1.read()));
}

void sobel_filter::thread_sub_ln1354_40_fu_2815_p2() {
    sub_ln1354_40_fu_2815_p2 = (!sub_ln1354_39_reg_6271.read().is_01() || !zext_ln1354_35_fu_2811_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_39_reg_6271.read()) - sc_biguint<19>(zext_ln1354_35_fu_2811_p1.read()));
}

void sobel_filter::thread_sub_ln1354_41_fu_2820_p2() {
    sub_ln1354_41_fu_2820_p2 = (!sub_ln1354_40_fu_2815_p2.read().is_01() || !zext_ln1354_33_reg_6201.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_40_fu_2815_p2.read()) - sc_biguint<19>(zext_ln1354_33_reg_6201.read()));
}

void sobel_filter::thread_sub_ln1354_42_fu_2500_p2() {
    sub_ln1354_42_fu_2500_p2 = (!zext_ln215_22_fu_2493_p1.read().is_01() || !zext_ln1354_36_fu_2496_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_22_fu_2493_p1.read()) - sc_biguint<19>(zext_ln1354_36_fu_2496_p1.read()));
}

void sobel_filter::thread_sub_ln1354_43_fu_2517_p2() {
    sub_ln1354_43_fu_2517_p2 = (!sub_ln1354_42_fu_2500_p2.read().is_01() || !zext_ln1354_37_fu_2513_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_42_fu_2500_p2.read()) - sc_biguint<19>(zext_ln1354_37_fu_2513_p1.read()));
}

void sobel_filter::thread_sub_ln1354_44_fu_2526_p2() {
    sub_ln1354_44_fu_2526_p2 = (!sub_ln1354_43_fu_2517_p2.read().is_01() || !zext_ln1354_38_fu_2523_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_43_fu_2517_p2.read()) - sc_biguint<19>(zext_ln1354_38_fu_2523_p1.read()));
}

void sobel_filter::thread_sub_ln1354_45_fu_2610_p2() {
    sub_ln1354_45_fu_2610_p2 = (!zext_ln215_23_fu_2603_p1.read().is_01() || !zext_ln1354_39_fu_2607_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_23_fu_2603_p1.read()) - sc_biguint<19>(zext_ln1354_39_fu_2607_p1.read()));
}

void sobel_filter::thread_sub_ln1354_46_fu_2861_p2() {
    sub_ln1354_46_fu_2861_p2 = (!sub_ln1354_45_reg_6326.read().is_01() || !zext_ln1354_40_fu_2857_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_45_reg_6326.read()) - sc_biguint<19>(zext_ln1354_40_fu_2857_p1.read()));
}

void sobel_filter::thread_sub_ln1354_47_fu_2866_p2() {
    sub_ln1354_47_fu_2866_p2 = (!sub_ln1354_46_fu_2861_p2.read().is_01() || !zext_ln1354_38_reg_6276.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_46_fu_2861_p2.read()) - sc_biguint<19>(zext_ln1354_38_reg_6276.read()));
}

void sobel_filter::thread_sub_ln1354_4_fu_2544_p2() {
    sub_ln1354_4_fu_2544_p2 = (!sub_ln1354_3_reg_6051.read().is_01() || !zext_ln1354_5_fu_2540_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_3_reg_6051.read()) - sc_biguint<19>(zext_ln1354_5_fu_2540_p1.read()));
}

void sobel_filter::thread_sub_ln1354_5_fu_2549_p2() {
    sub_ln1354_5_fu_2549_p2 = (!sub_ln1354_4_fu_2544_p2.read().is_01() || !zext_ln1354_3_reg_5781.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_4_fu_2544_p2.read()) - sc_biguint<19>(zext_ln1354_3_reg_5781.read()));
}

void sobel_filter::thread_sub_ln1354_6_fu_1636_p2() {
    sub_ln1354_6_fu_1636_p2 = (!zext_ln215_4_fu_1629_p1.read().is_01() || !zext_ln1354_6_fu_1632_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_4_fu_1629_p1.read()) - sc_biguint<19>(zext_ln1354_6_fu_1632_p1.read()));
}

void sobel_filter::thread_sub_ln1354_7_fu_1654_p2() {
    sub_ln1354_7_fu_1654_p2 = (!sub_ln1354_6_fu_1636_p2.read().is_01() || !zext_ln1354_7_fu_1650_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_6_fu_1636_p2.read()) - sc_biguint<19>(zext_ln1354_7_fu_1650_p1.read()));
}

void sobel_filter::thread_sub_ln1354_8_fu_1664_p2() {
    sub_ln1354_8_fu_1664_p2 = (!sub_ln1354_7_fu_1654_p2.read().is_01() || !zext_ln1354_8_fu_1660_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(sub_ln1354_7_fu_1654_p2.read()) - sc_biguint<19>(zext_ln1354_8_fu_1660_p1.read()));
}

void sobel_filter::thread_sub_ln1354_9_fu_2078_p2() {
    sub_ln1354_9_fu_2078_p2 = (!zext_ln215_5_fu_2070_p1.read().is_01() || !zext_ln1354_9_fu_2074_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_5_fu_2070_p1.read()) - sc_biguint<19>(zext_ln1354_9_fu_2074_p1.read()));
}

void sobel_filter::thread_sub_ln1354_fu_1482_p2() {
    sub_ln1354_fu_1482_p2 = (!zext_ln215_1_fu_1476_p1.read().is_01() || !zext_ln1354_1_fu_1479_p1.read().is_01())? sc_lv<19>(): (sc_biguint<19>(zext_ln215_1_fu_1476_p1.read()) - sc_biguint<19>(zext_ln1354_1_fu_1479_p1.read()));
}

void sobel_filter::thread_sub_ln329_1_fu_3317_p2() {
    sub_ln329_1_fu_3317_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_1_fu_3303_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_1_fu_3303_p1.read()));
}

void sobel_filter::thread_sub_ln329_2_fu_3475_p2() {
    sub_ln329_2_fu_3475_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_2_fu_3461_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_2_fu_3461_p1.read()));
}

void sobel_filter::thread_sub_ln329_3_fu_3633_p2() {
    sub_ln329_3_fu_3633_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_3_fu_3619_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_3_fu_3619_p1.read()));
}

void sobel_filter::thread_sub_ln329_4_fu_3791_p2() {
    sub_ln329_4_fu_3791_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_4_fu_3777_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_4_fu_3777_p1.read()));
}

void sobel_filter::thread_sub_ln329_5_fu_3949_p2() {
    sub_ln329_5_fu_3949_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_5_fu_3935_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_5_fu_3935_p1.read()));
}

void sobel_filter::thread_sub_ln329_6_fu_4182_p2() {
    sub_ln329_6_fu_4182_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_6_fu_4168_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_6_fu_4168_p1.read()));
}

void sobel_filter::thread_sub_ln329_7_fu_4356_p2() {
    sub_ln329_7_fu_4356_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_7_fu_4342_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_7_fu_4342_p1.read()));
}

void sobel_filter::thread_sub_ln329_fu_3147_p2() {
    sub_ln329_fu_3147_p2 = (!ap_const_lv12_433.is_01() || !zext_ln314_fu_3133_p1.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_433) - sc_biguint<12>(zext_ln314_fu_3133_p1.read()));
}

void sobel_filter::thread_sub_ln342_1_fu_3354_p2() {
    sub_ln342_1_fu_3354_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_1_reg_6743.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_1_reg_6743.read()));
}

void sobel_filter::thread_sub_ln342_2_fu_3512_p2() {
    sub_ln342_2_fu_3512_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_2_reg_6811.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_2_reg_6811.read()));
}

void sobel_filter::thread_sub_ln342_3_fu_3670_p2() {
    sub_ln342_3_fu_3670_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_3_reg_6879.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_3_reg_6879.read()));
}

void sobel_filter::thread_sub_ln342_4_fu_3828_p2() {
    sub_ln342_4_fu_3828_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_4_reg_6947.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_4_reg_6947.read()));
}

void sobel_filter::thread_sub_ln342_5_fu_3986_p2() {
    sub_ln342_5_fu_3986_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_5_reg_7015.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_5_reg_7015.read()));
}

void sobel_filter::thread_sub_ln342_6_fu_4235_p2() {
    sub_ln342_6_fu_4235_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_6_reg_7089.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_6_reg_7089.read()));
}

void sobel_filter::thread_sub_ln342_7_fu_4397_p2() {
    sub_ln342_7_fu_4397_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_7_reg_7167.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_7_reg_7167.read()));
}

void sobel_filter::thread_sub_ln342_fu_3196_p2() {
    sub_ln342_fu_3196_p2 = (!ap_const_lv12_0.is_01() || !sub_ln329_reg_6670.read().is_01())? sc_lv<12>(): (sc_biguint<12>(ap_const_lv12_0) - sc_bigint<12>(sub_ln329_reg_6670.read()));
}

void sobel_filter::thread_sub_ln461_1_fu_4553_p2() {
    sub_ln461_1_fu_4553_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_1_reg_7211.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_1_reg_7211.read()));
}

void sobel_filter::thread_sub_ln461_2_fu_4639_p2() {
    sub_ln461_2_fu_4639_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_2_reg_7222.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_2_reg_7222.read()));
}

void sobel_filter::thread_sub_ln461_3_fu_4725_p2() {
    sub_ln461_3_fu_4725_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_3_reg_7233.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_3_reg_7233.read()));
}

void sobel_filter::thread_sub_ln461_4_fu_4811_p2() {
    sub_ln461_4_fu_4811_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_4_reg_7244.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_4_reg_7244.read()));
}

void sobel_filter::thread_sub_ln461_5_fu_4897_p2() {
    sub_ln461_5_fu_4897_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_5_reg_7255.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_5_reg_7255.read()));
}

void sobel_filter::thread_sub_ln461_6_fu_4983_p2() {
    sub_ln461_6_fu_4983_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_6_reg_7266.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_6_reg_7266.read()));
}

void sobel_filter::thread_sub_ln461_7_fu_5069_p2() {
    sub_ln461_7_fu_5069_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_7_reg_7277.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_7_reg_7277.read()));
}

void sobel_filter::thread_sub_ln461_fu_4205_p2() {
    sub_ln461_fu_4205_p2 = (!ap_const_lv16_0.is_01() || !select_ln330_reg_7059.read().is_01())? sc_lv<16>(): (sc_biguint<16>(ap_const_lv16_0) - sc_biguint<16>(select_ln330_reg_7059.read()));
}

void sobel_filter::thread_tmp_10_fu_3338_p3() {
    tmp_10_fu_3338_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_1_fu_3335_p1.read());
}

void sobel_filter::thread_tmp_12_fu_3359_p4() {
    tmp_12_fu_3359_p4 = sub_ln342_1_fu_3354_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_13_fu_3496_p3() {
    tmp_13_fu_3496_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_2_fu_3493_p1.read());
}

void sobel_filter::thread_tmp_14_fu_4481_p3() {
    tmp_14_fu_4481_p3 = bitcast_ln696_1_reg_6719.read().range(63, 63);
}

void sobel_filter::thread_tmp_15_fu_3654_p3() {
    tmp_15_fu_3654_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_3_fu_3651_p1.read());
}

void sobel_filter::thread_tmp_17_fu_3812_p3() {
    tmp_17_fu_3812_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_4_fu_3809_p1.read());
}

void sobel_filter::thread_tmp_18_fu_3970_p3() {
    tmp_18_fu_3970_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_5_fu_3967_p1.read());
}

void sobel_filter::thread_tmp_19_fu_4219_p3() {
    tmp_19_fu_4219_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_6_fu_4216_p1.read());
}

void sobel_filter::thread_tmp_20_fu_4381_p3() {
    tmp_20_fu_4381_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_7_fu_4378_p1.read());
}

void sobel_filter::thread_tmp_21_fu_3517_p4() {
    tmp_21_fu_3517_p4 = sub_ln342_2_fu_3512_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_25_fu_4567_p3() {
    tmp_25_fu_4567_p3 = bitcast_ln696_2_reg_6787.read().range(63, 63);
}

void sobel_filter::thread_tmp_27_fu_3675_p4() {
    tmp_27_fu_3675_p4 = sub_ln342_3_fu_3670_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_28_fu_4653_p3() {
    tmp_28_fu_4653_p3 = bitcast_ln696_3_reg_6855.read().range(63, 63);
}

void sobel_filter::thread_tmp_30_fu_3833_p4() {
    tmp_30_fu_3833_p4 = sub_ln342_4_fu_3828_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_31_fu_4739_p3() {
    tmp_31_fu_4739_p3 = bitcast_ln696_4_reg_6923.read().range(63, 63);
}

void sobel_filter::thread_tmp_33_fu_3991_p4() {
    tmp_33_fu_3991_p4 = sub_ln342_5_fu_3986_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_34_fu_4825_p3() {
    tmp_34_fu_4825_p3 = bitcast_ln696_5_reg_6991.read().range(63, 63);
}

void sobel_filter::thread_tmp_36_fu_4240_p4() {
    tmp_36_fu_4240_p4 = sub_ln342_6_fu_4235_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_37_fu_4911_p3() {
    tmp_37_fu_4911_p3 = bitcast_ln696_6_reg_7065.read().range(63, 63);
}

void sobel_filter::thread_tmp_39_fu_4402_p4() {
    tmp_39_fu_4402_p4 = sub_ln342_7_fu_4397_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_40_fu_4997_p3() {
    tmp_40_fu_4997_p3 = bitcast_ln696_7_reg_7143.read().range(63, 63);
}

void sobel_filter::thread_tmp_6_fu_3201_p4() {
    tmp_6_fu_3201_p4 = sub_ln342_fu_3196_p2.read().range(11, 4);
}

void sobel_filter::thread_tmp_7_fu_4070_p3() {
    tmp_7_fu_4070_p3 = bitcast_ln696_reg_6646.read().range(63, 63);
}

void sobel_filter::thread_tmp_8_fu_3180_p3() {
    tmp_8_fu_3180_p3 = esl_concat<1,52>(ap_const_lv1_1, trunc_ln318_fu_3177_p1.read());
}

void sobel_filter::thread_trunc_ln162_1_fu_5106_p1() {
    trunc_ln162_1_fu_5106_p1 = or_ln700_3_fu_5095_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln162_2_fu_5135_p1() {
    trunc_ln162_2_fu_5135_p1 = or_ln700_4_fu_5124_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln162_3_fu_5156_p1() {
    trunc_ln162_3_fu_5156_p1 = or_ln700_5_fu_5145_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln162_fu_5085_p1() {
    trunc_ln162_fu_5085_p1 = ap_phi_mux_t_V_3_0_phi_fu_742_p4.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_10_fu_1256_p1() {
    trunc_ln215_10_fu_1256_p1 = add_ln1353_21_fu_1250_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_11_fu_1282_p1() {
    trunc_ln215_11_fu_1282_p1 = add_ln1354_5_fu_1276_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_12_fu_1292_p1() {
    trunc_ln215_12_fu_1292_p1 = add_ln1353_26_fu_1286_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_13_fu_1322_p1() {
    trunc_ln215_13_fu_1322_p1 = add_ln1354_6_fu_1316_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_14_fu_1332_p1() {
    trunc_ln215_14_fu_1332_p1 = add_ln1353_31_fu_1326_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_15_fu_1358_p1() {
    trunc_ln215_15_fu_1358_p1 = add_ln1354_7_fu_1352_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_16_fu_1368_p1() {
    trunc_ln215_16_fu_1368_p1 = add_ln1353_36_fu_1362_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_1_fu_1296_p1() {
    trunc_ln215_1_fu_1296_p1 = t_V_6_0_reg_726.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_2_fu_1112_p1() {
    trunc_ln215_2_fu_1112_p1 = add_ln1353_1_fu_1106_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_3_fu_1138_p1() {
    trunc_ln215_3_fu_1138_p1 = add_ln1354_1_fu_1132_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_4_fu_1148_p1() {
    trunc_ln215_4_fu_1148_p1 = add_ln1353_6_fu_1142_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_5_fu_1174_p1() {
    trunc_ln215_5_fu_1174_p1 = add_ln1354_2_fu_1168_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_6_fu_1184_p1() {
    trunc_ln215_6_fu_1184_p1 = add_ln1353_11_fu_1178_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_7_fu_1210_p1() {
    trunc_ln215_7_fu_1210_p1 = add_ln1354_3_fu_1204_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_8_fu_1220_p1() {
    trunc_ln215_8_fu_1220_p1 = add_ln1353_16_fu_1214_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_9_fu_1246_p1() {
    trunc_ln215_9_fu_1246_p1 = add_ln1354_4_fu_1240_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln215_fu_1097_p1() {
    trunc_ln215_fu_1097_p1 = add_ln1354_fu_1091_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln311_1_fu_3281_p1() {
    trunc_ln311_1_fu_3281_p1 = bitcast_ln696_1_fu_3277_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_2_fu_3439_p1() {
    trunc_ln311_2_fu_3439_p1 = bitcast_ln696_2_fu_3435_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_3_fu_3597_p1() {
    trunc_ln311_3_fu_3597_p1 = bitcast_ln696_3_fu_3593_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_4_fu_3755_p1() {
    trunc_ln311_4_fu_3755_p1 = bitcast_ln696_4_fu_3751_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_5_fu_3913_p1() {
    trunc_ln311_5_fu_3913_p1 = bitcast_ln696_5_fu_3909_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_6_fu_4146_p1() {
    trunc_ln311_6_fu_4146_p1 = bitcast_ln696_6_fu_4142_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_7_fu_4320_p1() {
    trunc_ln311_7_fu_4320_p1 = bitcast_ln696_7_fu_4316_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln311_fu_3111_p1() {
    trunc_ln311_fu_3111_p1 = bitcast_ln696_fu_3107_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln318_1_fu_3335_p1() {
    trunc_ln318_1_fu_3335_p1 = bitcast_ln696_1_reg_6719.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_2_fu_3493_p1() {
    trunc_ln318_2_fu_3493_p1 = bitcast_ln696_2_reg_6787.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_3_fu_3651_p1() {
    trunc_ln318_3_fu_3651_p1 = bitcast_ln696_3_reg_6855.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_4_fu_3809_p1() {
    trunc_ln318_4_fu_3809_p1 = bitcast_ln696_4_reg_6923.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_5_fu_3967_p1() {
    trunc_ln318_5_fu_3967_p1 = bitcast_ln696_5_reg_6991.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_6_fu_4216_p1() {
    trunc_ln318_6_fu_4216_p1 = bitcast_ln696_6_reg_7065.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_7_fu_4378_p1() {
    trunc_ln318_7_fu_4378_p1 = bitcast_ln696_7_reg_7143.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln318_fu_3177_p1() {
    trunc_ln318_fu_3177_p1 = bitcast_ln696_reg_6646.read().range(52-1, 0);
}

void sobel_filter::thread_trunc_ln321_1_fu_959_p1() {
    trunc_ln321_1_fu_959_p1 = or_ln700_fu_948_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln321_2_fu_996_p1() {
    trunc_ln321_2_fu_996_p1 = or_ln700_1_fu_985_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln321_3_fu_1033_p1() {
    trunc_ln321_3_fu_1033_p1 = or_ln700_2_fu_1022_p2.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln321_fu_922_p1() {
    trunc_ln321_fu_922_p1 = ap_phi_mux_t_V_2_0_phi_fu_718_p4.read().range(14-1, 0);
}

void sobel_filter::thread_trunc_ln334_1_fu_3385_p1() {
    trunc_ln334_1_fu_3385_p1 = lshr_ln334_1_fu_3379_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_2_fu_3543_p1() {
    trunc_ln334_2_fu_3543_p1 = lshr_ln334_2_fu_3537_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_3_fu_3701_p1() {
    trunc_ln334_3_fu_3701_p1 = lshr_ln334_3_fu_3695_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_4_fu_3859_p1() {
    trunc_ln334_4_fu_3859_p1 = lshr_ln334_4_fu_3853_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_5_fu_4017_p1() {
    trunc_ln334_5_fu_4017_p1 = lshr_ln334_5_fu_4011_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_6_fu_4266_p1() {
    trunc_ln334_6_fu_4266_p1 = lshr_ln334_6_fu_4260_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_7_fu_4428_p1() {
    trunc_ln334_7_fu_4428_p1 = lshr_ln334_7_fu_4422_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln334_fu_3227_p1() {
    trunc_ln334_fu_3227_p1 = lshr_ln334_fu_3221_p2.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_1_fu_3307_p1() {
    trunc_ln344_1_fu_3307_p1 = bitcast_ln696_1_fu_3277_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_2_fu_3465_p1() {
    trunc_ln344_2_fu_3465_p1 = bitcast_ln696_2_fu_3435_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_3_fu_3623_p1() {
    trunc_ln344_3_fu_3623_p1 = bitcast_ln696_3_fu_3593_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_4_fu_3781_p1() {
    trunc_ln344_4_fu_3781_p1 = bitcast_ln696_4_fu_3751_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_5_fu_3939_p1() {
    trunc_ln344_5_fu_3939_p1 = bitcast_ln696_5_fu_3909_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_6_fu_4172_p1() {
    trunc_ln344_6_fu_4172_p1 = bitcast_ln696_6_fu_4142_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_7_fu_4346_p1() {
    trunc_ln344_7_fu_4346_p1 = bitcast_ln696_7_fu_4316_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln344_fu_3137_p1() {
    trunc_ln344_fu_3137_p1 = bitcast_ln696_fu_3107_p1.read().range(16-1, 0);
}

void sobel_filter::thread_trunc_ln368_10_fu_2891_p1() {
    trunc_ln368_10_fu_2891_p1 = p_Val2_10_fu_2887_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_11_fu_3003_p1() {
    trunc_ln368_11_fu_3003_p1 = p_Val2_11_fu_2999_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_12_fu_2915_p1() {
    trunc_ln368_12_fu_2915_p1 = p_Val2_12_fu_2911_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_13_fu_3011_p1() {
    trunc_ln368_13_fu_3011_p1 = p_Val2_13_fu_3007_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_14_fu_2939_p1() {
    trunc_ln368_14_fu_2939_p1 = p_Val2_14_fu_2935_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_15_fu_3019_p1() {
    trunc_ln368_15_fu_3019_p1 = p_Val2_15_fu_3015_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_1_fu_2947_p1() {
    trunc_ln368_1_fu_2947_p1 = p_Val2_1_fu_2943_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_2_fu_2624_p1() {
    trunc_ln368_2_fu_2624_p1 = p_Val2_2_fu_2620_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_3_fu_2971_p1() {
    trunc_ln368_3_fu_2971_p1 = p_Val2_3_fu_2967_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_4_fu_2705_p1() {
    trunc_ln368_4_fu_2705_p1 = p_Val2_4_fu_2701_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_5_fu_2979_p1() {
    trunc_ln368_5_fu_2979_p1 = p_Val2_5_fu_2975_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_6_fu_2777_p1() {
    trunc_ln368_6_fu_2777_p1 = p_Val2_6_fu_2773_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_7_fu_2987_p1() {
    trunc_ln368_7_fu_2987_p1 = p_Val2_7_fu_2983_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_8_fu_2845_p1() {
    trunc_ln368_8_fu_2845_p1 = p_Val2_8_fu_2841_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_9_fu_2995_p1() {
    trunc_ln368_9_fu_2995_p1 = p_Val2_9_fu_2991_p1.read().range(63-1, 0);
}

void sobel_filter::thread_trunc_ln368_fu_2039_p1() {
    trunc_ln368_fu_2039_p1 = p_Val2_s_fu_2035_p1.read().range(63-1, 0);
}

void sobel_filter::thread_xor_ln326_1_fu_4536_p2() {
    xor_ln326_1_fu_4536_p2 = (icmp_ln326_1_reg_6736.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_2_fu_4622_p2() {
    xor_ln326_2_fu_4622_p2 = (icmp_ln326_2_reg_6804.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_3_fu_4708_p2() {
    xor_ln326_3_fu_4708_p2 = (icmp_ln326_3_reg_6872.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_4_fu_4794_p2() {
    xor_ln326_4_fu_4794_p2 = (icmp_ln326_4_reg_6940.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_5_fu_4880_p2() {
    xor_ln326_5_fu_4880_p2 = (icmp_ln326_5_reg_7008.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_6_fu_4966_p2() {
    xor_ln326_6_fu_4966_p2 = (icmp_ln326_6_reg_7082.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_7_fu_5052_p2() {
    xor_ln326_7_fu_5052_p2 = (icmp_ln326_7_reg_7160.read() ^ ap_const_lv1_1);
}

void sobel_filter::thread_xor_ln326_fu_4125_p2() {
    xor_ln326_fu_4125_p2 = (icmp_ln326_reg_6663.read() ^ ap_const_lv1_1);
}

}

