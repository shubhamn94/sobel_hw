############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project sobel_hls_final
set_top sobel_filter
add_files work/sobel/sobel.cpp
add_files work/sobel/sobel.hpp
add_files -tb work/sobel/sobel_test.cpp -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "sobel_hls_fin"
set_part {xc7vx690t-ffg1761-2} -tool vivado
create_clock -period 4 -name default
config_sdx -target none
config_export -format ip_catalog -rtl verilog -vivado_optimization_level 2 -vivado_phys_opt place -vivado_report_level 0
set_clock_uncertainty 12.5%
#source "./sobel_hls_final/sobel_hls_fin/directives.tcl"
csim_design -ldflags {-ljpeg -fpermissive} -clean
csynth_design
cosim_design -ldflags {-ljpeg -fpermissive}
export_design -rtl verilog -format ip_catalog
