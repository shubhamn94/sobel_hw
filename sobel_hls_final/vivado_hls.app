<project xmlns="com.autoesl.autopilot.project" name="sobel_hls_final" top="sobel_filter">
    <includePaths/>
    <libraryPaths/>
    <Simulation argv="">
        <SimFlow name="csim" ldflags="-ljpeg -fpermissive" clean="true" csimMode="0" lastCsimMode="0"/>
    </Simulation>
    <files xmlns="">
        <file name="../../work/sobel/sobel_test.cpp" sc="0" tb="1" cflags=" -Wno-unknown-pragmas" csimflags=" -Wno-unknown-pragmas" blackbox="false"/>
        <file name="work/sobel/sobel.hpp" sc="0" tb="false" cflags="" csimflags="" blackbox="false"/>
        <file name="work/sobel/sobel.cpp" sc="0" tb="false" cflags="" csimflags="" blackbox="false"/>
    </files>
    <solutions xmlns="">
        <solution name="sobel_hls_fin" status="active"/>
    </solutions>
</project>

