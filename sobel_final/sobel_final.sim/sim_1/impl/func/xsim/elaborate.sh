#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.1 (64-bit)
#
# Filename    : elaborate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for elaborating the compiled design
#
# Generated by Vivado on Mon Dec 30 07:31:19 EST 2019
# SW Build 2552052 on Fri May 24 14:47:09 MDT 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: elaborate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xelab -wto f0787d8c9d0244fab6377a55c622e064 --incr --debug typical --relax --mt 8 -L xil_defaultlib -L unisims_ver -L secureip --snapshot sobel_top_func_impl xil_defaultlib.sobel_top xil_defaultlib.glbl -log elaborate.log"
xelab -wto f0787d8c9d0244fab6377a55c622e064 --incr --debug typical --relax --mt 8 -L xil_defaultlib -L unisims_ver -L secureip --snapshot sobel_top_func_impl xil_defaultlib.sobel_top xil_defaultlib.glbl -log elaborate.log

