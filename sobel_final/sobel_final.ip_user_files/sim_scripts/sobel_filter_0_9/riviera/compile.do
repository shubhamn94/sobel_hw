vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm
vlib riviera/xbip_utils_v3_0_10
vlib riviera/axi_utils_v2_0_6
vlib riviera/xbip_pipe_v3_0_6
vlib riviera/xbip_dsp48_wrapper_v3_0_4
vlib riviera/xbip_dsp48_addsub_v3_0_6
vlib riviera/xbip_dsp48_multadd_v3_0_6
vlib riviera/xbip_bram18k_v3_0_6
vlib riviera/mult_gen_v12_0_15
vlib riviera/floating_point_v7_1_8

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm
vmap xbip_utils_v3_0_10 riviera/xbip_utils_v3_0_10
vmap axi_utils_v2_0_6 riviera/axi_utils_v2_0_6
vmap xbip_pipe_v3_0_6 riviera/xbip_pipe_v3_0_6
vmap xbip_dsp48_wrapper_v3_0_4 riviera/xbip_dsp48_wrapper_v3_0_4
vmap xbip_dsp48_addsub_v3_0_6 riviera/xbip_dsp48_addsub_v3_0_6
vmap xbip_dsp48_multadd_v3_0_6 riviera/xbip_dsp48_multadd_v3_0_6
vmap xbip_bram18k_v3_0_6 riviera/xbip_bram18k_v3_0_6
vmap mult_gen_v12_0_15 riviera/mult_gen_v12_0_15
vmap floating_point_v7_1_8 riviera/floating_point_v7_1_8

vlog -work xil_defaultlib  -sv2k12 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xbip_utils_v3_0_10 -93 \
"../../../ipstatic/hdl/xbip_utils_v3_0_vh_rfs.vhd" \

vcom -work axi_utils_v2_0_6 -93 \
"../../../ipstatic/hdl/axi_utils_v2_0_vh_rfs.vhd" \

vcom -work xbip_pipe_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_wrapper_v3_0_4 -93 \
"../../../ipstatic/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_addsub_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_multadd_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_dsp48_multadd_v3_0_vh_rfs.vhd" \

vcom -work xbip_bram18k_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \

vcom -work mult_gen_v12_0_15 -93 \
"../../../ipstatic/hdl/mult_gen_v12_0_vh_rfs.vhd" \

vcom -work floating_point_v7_1_8 -93 \
"../../../ipstatic/hdl/floating_point_v7_1_vh_rfs.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../ipstatic/hdl/verilog/pow_generic_double_s.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_9.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_12.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_13.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_14.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_15.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_16.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_17.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_18.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_19.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_20.v" \
"../../../ipstatic/hdl/verilog/pow_generic_double_s_pow_reduce_anonymo_21.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_dsqrt_64ns_64ns_64_57_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_fpext_32ns_64_2_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_LineBuffer_V.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mac_muladd_16ns_16s_19s_31_3_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_40ns_40ns_80_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_43ns_36ns_79_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_49ns_44ns_93_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_50ns_50ns_100_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_54s_6ns_54_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_71ns_4ns_75_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_72ns_13s_83_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_73ns_6ns_79_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_77ns_6ns_83_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_80ns_12s_90_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_82ns_6ns_88_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_83ns_6ns_89_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_87ns_6ns_93_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_mul_92ns_6ns_98_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_result_lb_V.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_sitofp_32s_32_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_uitofp_32ns_32_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter.v" \

vcom -work xil_defaultlib -93 \
"../../../ipstatic/hdl/ip/sobel_filter_ap_dsqrt_55_no_dsp_64.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_fpext_0_no_dsp_32.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_sitofp_4_no_dsp_32.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_uitofp_4_no_dsp_32.vhd" \
"../../../../../sobel_integ_design/sobel_filter_0_9/sim/sobel_filter_0.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

