-makelib ies_lib/xil_defaultlib -sv \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib ies_lib/xpm \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib ies_lib/xbip_utils_v3_0_10 \
  "../../../ipstatic/hdl/xbip_utils_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/axi_utils_v2_0_6 \
  "../../../ipstatic/hdl/axi_utils_v2_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xbip_pipe_v3_0_6 \
  "../../../ipstatic/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xbip_dsp48_wrapper_v3_0_4 \
  "../../../ipstatic/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xbip_dsp48_addsub_v3_0_6 \
  "../../../ipstatic/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xbip_dsp48_multadd_v3_0_6 \
  "../../../ipstatic/hdl/xbip_dsp48_multadd_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xbip_bram18k_v3_0_6 \
  "../../../ipstatic/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/mult_gen_v12_0_15 \
  "../../../ipstatic/hdl/mult_gen_v12_0_vh_rfs.vhd" \
-endlib
-makelib ies_lib/floating_point_v7_1_8 \
  "../../../ipstatic/hdl/floating_point_v7_1_vh_rfs.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../ipstatic/hdl/verilog/sobel_filter_dsqrt_64ns_64ns_64_57_1.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_fadd_32ns_32ns_32_9_full_dsp_1.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_fmul_32ns_32ns_32_5_max_dsp_1.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_fpext_32ns_64_2_1.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_LineBuffer_V.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_result_lb_V.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_sitofp_32s_32_6_1.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter_uitofp_32ns_32_6_1.v" \
  "../../../ipstatic/hdl/verilog/sobel_filter.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../ipstatic/hdl/ip/sobel_filter_ap_dsqrt_55_no_dsp_64.vhd" \
  "../../../ipstatic/hdl/ip/sobel_filter_ap_fadd_7_full_dsp_32.vhd" \
  "../../../ipstatic/hdl/ip/sobel_filter_ap_fmul_3_max_dsp_32.vhd" \
  "../../../ipstatic/hdl/ip/sobel_filter_ap_fpext_0_no_dsp_32.vhd" \
  "../../../ipstatic/hdl/ip/sobel_filter_ap_sitofp_4_no_dsp_32.vhd" \
  "../../../ipstatic/hdl/ip/sobel_filter_ap_uitofp_4_no_dsp_32.vhd" \
  "../../../../../sobel_integ_design/sobel_filter_0_27/sim/sobel_filter_0.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  glbl.v
-endlib

