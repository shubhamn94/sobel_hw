-makelib ies_lib/xil_defaultlib -sv \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib ies_lib/xpm \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_7vx.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_8k.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_16k.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_cpl.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_rep.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_rep_8k.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_req.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_init_ctrl_7vx.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_lane.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_misc.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_pipeline.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_top.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_force_adapt.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_clock.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_drp.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_eq.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_rate.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_reset.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_sync.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_user.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_wrapper.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_drp.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_reset.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_wrapper.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_rxeq_scan.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_wrapper.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_top.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_common.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gtx_cpllpd_ovrd.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_tlp_tph_tbl_7vx.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_3_0_7vx.v" \
  "../../../../sobel_final.srcs/sources_1/ip/PCIeGen3x4If128/sim/PCIeGen3x4If128.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  glbl.v
-endlib

