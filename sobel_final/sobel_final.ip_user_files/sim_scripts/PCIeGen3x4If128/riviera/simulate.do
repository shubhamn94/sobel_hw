onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+PCIeGen3x4If128 -L xil_defaultlib -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.PCIeGen3x4If128 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {PCIeGen3x4If128.udo}

run -all

endsim

quit -force
