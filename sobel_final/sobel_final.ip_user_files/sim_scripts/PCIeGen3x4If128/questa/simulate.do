onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib PCIeGen3x4If128_opt

do {wave.do}

view wave
view structure
view signals

do {PCIeGen3x4If128.udo}

run -all

quit -force
