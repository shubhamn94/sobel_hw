vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/xbip_utils_v3_0_10
vlib activehdl/axi_utils_v2_0_6
vlib activehdl/xbip_pipe_v3_0_6
vlib activehdl/xbip_dsp48_wrapper_v3_0_4
vlib activehdl/xbip_dsp48_addsub_v3_0_6
vlib activehdl/xbip_dsp48_multadd_v3_0_6
vlib activehdl/xbip_bram18k_v3_0_6
vlib activehdl/mult_gen_v12_0_15
vlib activehdl/floating_point_v7_1_8

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap xbip_utils_v3_0_10 activehdl/xbip_utils_v3_0_10
vmap axi_utils_v2_0_6 activehdl/axi_utils_v2_0_6
vmap xbip_pipe_v3_0_6 activehdl/xbip_pipe_v3_0_6
vmap xbip_dsp48_wrapper_v3_0_4 activehdl/xbip_dsp48_wrapper_v3_0_4
vmap xbip_dsp48_addsub_v3_0_6 activehdl/xbip_dsp48_addsub_v3_0_6
vmap xbip_dsp48_multadd_v3_0_6 activehdl/xbip_dsp48_multadd_v3_0_6
vmap xbip_bram18k_v3_0_6 activehdl/xbip_bram18k_v3_0_6
vmap mult_gen_v12_0_15 activehdl/mult_gen_v12_0_15
vmap floating_point_v7_1_8 activehdl/floating_point_v7_1_8

vlog -work xil_defaultlib  -sv2k12 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xbip_utils_v3_0_10 -93 \
"../../../ipstatic/hdl/xbip_utils_v3_0_vh_rfs.vhd" \

vcom -work axi_utils_v2_0_6 -93 \
"../../../ipstatic/hdl/axi_utils_v2_0_vh_rfs.vhd" \

vcom -work xbip_pipe_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_wrapper_v3_0_4 -93 \
"../../../ipstatic/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_addsub_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_multadd_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_dsp48_multadd_v3_0_vh_rfs.vhd" \

vcom -work xbip_bram18k_v3_0_6 -93 \
"../../../ipstatic/hdl/xbip_bram18k_v3_0_vh_rfs.vhd" \

vcom -work mult_gen_v12_0_15 -93 \
"../../../ipstatic/hdl/mult_gen_v12_0_vh_rfs.vhd" \

vcom -work floating_point_v7_1_8 -93 \
"../../../ipstatic/hdl/floating_point_v7_1_vh_rfs.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../ipstatic/hdl/verilog/sobel_filter_dsqrt_64ns_64ns_64_57_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_fadd_32ns_32ns_32_9_full_dsp_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_fmul_32ns_32ns_32_5_max_dsp_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_fpext_32ns_64_2_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_LineBuffer_V.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_result_lb_V.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_sitofp_32s_32_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter_uitofp_32ns_32_6_1.v" \
"../../../ipstatic/hdl/verilog/sobel_filter.v" \

vcom -work xil_defaultlib -93 \
"../../../ipstatic/hdl/ip/sobel_filter_ap_dsqrt_55_no_dsp_64.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_fadd_7_full_dsp_32.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_fmul_3_max_dsp_32.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_fpext_0_no_dsp_32.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_sitofp_4_no_dsp_32.vhd" \
"../../../ipstatic/hdl/ip/sobel_filter_ap_uitofp_4_no_dsp_32.vhd" \
"../../../../../sobel_integ_design/sobel_filter_0_25/sim/sobel_filter_0.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

