#!/bin/bash -f
#*********************************************************************************************************
# Vivado (TM) v2019.1 (64-bit)
#
# Filename    : VC709_Gen3x4If128_0.sh
# Simulator   : Synopsys Verilog Compiler Simulator
# Description : Simulation script for compiling, elaborating and verifying the project source files.
#               The script will automatically create the design libraries sub-directories in the run
#               directory, add the library logical mappings in the simulator setup file, create default
#               'do/prj' file, execute compilation, elaboration and simulation steps.
#
# Generated by Vivado on Wed Dec 11 05:29:33 EST 2019
# SW Build 2552052 on Fri May 24 14:47:09 MDT 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved. 
#
# usage: VC709_Gen3x4If128_0.sh [-help]
# usage: VC709_Gen3x4If128_0.sh [-lib_map_path]
# usage: VC709_Gen3x4If128_0.sh [-noclean_files]
# usage: VC709_Gen3x4If128_0.sh [-reset_run]
#
# Prerequisite:- To compile and run simulation, you must compile the Xilinx simulation libraries using the
# 'compile_simlib' TCL command. For more information about this command, run 'compile_simlib -help' in the
# Vivado Tcl Shell. Once the libraries have been compiled successfully, specify the -lib_map_path switch
# that points to these libraries and rerun export_simulation. For more information about this switch please
# type 'export_simulation -help' in the Tcl shell.
#
# You can also point to the simulation libraries by either replacing the <SPECIFY_COMPILED_LIB_PATH> in this
# script with the compiled library directory path or specify this path with the '-lib_map_path' switch when
# executing this script. Please type 'VC709_Gen3x4If128_0.sh -help' for more information.
#
# Additional references - 'Xilinx Vivado Design Suite User Guide:Logic simulation (UG900)'
#
#*********************************************************************************************************

# Directory path for design sources and include directories (if any) wrt this path
ref_dir="."

# Override directory with 'export_sim_ref_dir' env path value if set in the shell
if [[ (! -z "$export_sim_ref_dir") && ($export_sim_ref_dir != "") ]]; then
  ref_dir="$export_sim_ref_dir"
fi

# Command line options
vlogan_opts="-full64"
vhdlan_opts="-full64"
vcs_elab_opts="-full64 -debug_pp -t ps -licqueue -l elaborate.log"
vcs_sim_opts="-ucli -licqueue -l simulate.log"

# Design libraries
design_libs=(xil_defaultlib xpm)

# Simulation root library directory
sim_lib_dir="vcs_lib"

# Script info
echo -e "VC709_Gen3x4If128_0.sh - Script generated by export_simulation (Vivado v2019.1 (64-bit)-id)\n"

# Main steps
run()
{
  check_args $# $1
  setup $1 $2
  compile
  elaborate
  simulate
}

# RUN_STEP: <compile>
compile()
{
  # Compile design files
  vlogan -work xil_defaultlib $vlogan_opts -sverilog +incdir+"$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl" \
    "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
    "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
  2>&1 | tee -a vlogan.log

  vhdlan -work xpm $vhdlan_opts \
    "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
  2>&1 | tee -a vhdlan.log

  vlogan -work xil_defaultlib $vlogan_opts +v2k +incdir+"$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_7vx.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_8k.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_16k.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_cpl.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_rep.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_rep_8k.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_req.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_init_ctrl_7vx.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_lane.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_misc.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_pipeline.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_top.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_force_adapt.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_clock.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_drp.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_eq.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_rate.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_reset.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_sync.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_user.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_wrapper.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_drp.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_reset.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_wrapper.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_rxeq_scan.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_wrapper.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_top.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_common.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gtx_cpllpd_ovrd.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_tlp_tph_tbl_7vx.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_3_0_7vx.v" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/sim/PCIeGen3x4If128.v" \
  2>&1 | tee -a vlogan.log

  vlogan -work xil_defaultlib $vlogan_opts -sverilog +incdir+"$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/async_fifo.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/async_fifo_fwft.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/counter.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/cross_domain_signal.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/demux.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/engine_layer.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/ff.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo_packer_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo_packer_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo_packer_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/interrupt.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/interrupt_controller.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/mux.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/offset_flag_to_one_hot.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/offset_to_mask.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/one_hot_mux.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/shubham/pci_to_axis_converter.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/pipeline.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/ram_1clk_1w_1r.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/ram_2clk_1w_1r.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/recv_credit_flow_ctrl.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/register.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/registers.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reorder_queue.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reorder_queue_input.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reorder_queue_output.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reset_controller.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reset_extender.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/riffa.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/fpga/xilinx/vc709/riffa_wrapper_vc709.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rotate.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_engine_classic.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_engine_ultrascale.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_channel_gate.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_reader.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_requester_mux.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxc_engine_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxc_engine_classic.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxc_engine_ultrascale.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxr_engine_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxr_engine_classic.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxr_engine_ultrascale.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/scsdpram.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_reader_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_reader_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_reader_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_requester.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/shiftreg.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sync_fifo.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/syncff.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_alignment_pipeline.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_data_fifo.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_data_pipeline.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_data_shift.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine_classic.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine_selector.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine_ultrascale.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_hdr_fifo.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_buffer_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_buffer_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_buffer_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_channel_gate_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_channel_gate_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_channel_gate_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_monitor_128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_monitor_32.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_monitor_64.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_writer.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txc_engine_classic.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txc_engine_ultrascale.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txr_engine_classic.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txr_engine_ultrascale.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/fpga/xilinx/vc709/VC709_Gen3x4If128/hdl/VC709_Gen3x4If128.sv" \
    "$ref_dir/../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sim/VC709_Gen3x4If128_0.sv" \
  2>&1 | tee -a vlogan.log


  vlogan -work xil_defaultlib $vlogan_opts +v2k \
    glbl.v \
  2>&1 | tee -a vlogan.log

}

# RUN_STEP: <elaborate>
elaborate()
{
  vcs $vcs_elab_opts xil_defaultlib.VC709_Gen3x4If128_0 xil_defaultlib.glbl -o VC709_Gen3x4If128_0_simv
}

# RUN_STEP: <simulate>
simulate()
{
  ./VC709_Gen3x4If128_0_simv $vcs_sim_opts -do simulate.do
}

# STEP: setup
setup()
{
  case $1 in
    "-lib_map_path" )
      if [[ ($2 == "") ]]; then
        echo -e "ERROR: Simulation library directory path not specified (type \"./VC709_Gen3x4If128_0.sh -help\" for more information)\n"
        exit 1
      fi
      create_lib_mappings $2
    ;;
    "-reset_run" )
      reset_run
      echo -e "INFO: Simulation run files deleted.\n"
      exit 0
    ;;
    "-noclean_files" )
      # do not remove previous data
    ;;
    * )
      create_lib_mappings $2
  esac

  create_lib_dir

  # Add any setup/initialization commands here:-

  # <user specific commands>

}

# Define design library mappings
create_lib_mappings()
{
  file="synopsys_sim.setup"
  if [[ -e $file ]]; then
    if [[ ($1 == "") ]]; then
      return
    else
      rm -rf $file
    fi
  fi

  touch $file

  if [[ ($1 != "") ]]; then
    lib_map_path="$1"
  else
    lib_map_path="/home/shubham/sobel_final/sobel_final.cache/compile_simlib/vcs"
  fi

  for (( i=0; i<${#design_libs[*]}; i++ )); do
    lib="${design_libs[i]}"
    mapping="$lib:$sim_lib_dir/$lib"
    echo $mapping >> $file
  done

  if [[ ($lib_map_path != "") ]]; then
    incl_ref="OTHERS=$lib_map_path/synopsys_sim.setup"
    echo $incl_ref >> $file
  fi
}

# Create design library directory paths
create_lib_dir()
{
  if [[ -e $sim_lib_dir ]]; then
    rm -rf $sim_lib_dir
  fi

  for (( i=0; i<${#design_libs[*]}; i++ )); do
    lib="${design_libs[i]}"
    lib_dir="$sim_lib_dir/$lib"
    if [[ ! -e $lib_dir ]]; then
      mkdir -p $lib_dir
    fi
  done
}

# Delete generated data from the previous run
reset_run()
{
  files_to_remove=(ucli.key VC709_Gen3x4If128_0_simv vlogan.log vhdlan.log compile.log elaborate.log simulate.log .vlogansetup.env .vlogansetup.args .vcs_lib_lock scirocco_command.log 64 AN.DB csrc VC709_Gen3x4If128_0_simv.daidir)
  for (( i=0; i<${#files_to_remove[*]}; i++ )); do
    file="${files_to_remove[i]}"
    if [[ -e $file ]]; then
      rm -rf $file
    fi
  done

  create_lib_dir
}

# Check command line arguments
check_args()
{
  if [[ ($1 == 1 ) && ($2 != "-lib_map_path" && $2 != "-noclean_files" && $2 != "-reset_run" && $2 != "-help" && $2 != "-h") ]]; then
    echo -e "ERROR: Unknown option specified '$2' (type \"./VC709_Gen3x4If128_0.sh -help\" for more information)\n"
    exit 1
  fi

  if [[ ($2 == "-help" || $2 == "-h") ]]; then
    usage
  fi
}

# Script usage
usage()
{
  msg="Usage: VC709_Gen3x4If128_0.sh [-help]\n\
Usage: VC709_Gen3x4If128_0.sh [-lib_map_path]\n\
Usage: VC709_Gen3x4If128_0.sh [-reset_run]\n\
Usage: VC709_Gen3x4If128_0.sh [-noclean_files]\n\n\
[-help] -- Print help information for this script\n\n\
[-lib_map_path <path>] -- Compiled simulation library directory path. The simulation library is compiled\n\
using the compile_simlib tcl command. Please see 'compile_simlib -help' for more information.\n\n\
[-reset_run] -- Recreate simulator setup files and library mappings for a clean run. The generated files\n\
from the previous run will be removed. If you don't want to remove the simulator generated files, use the\n\
-noclean_files switch.\n\n\
[-noclean_files] -- Reset previous run, but do not remove simulator generated files from the previous run.\n\n"
  echo -e $msg
  exit 1
}

# Launch script
run $1 $2
