onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib VC709_Gen3x4If128_0_opt

do {wave.do}

view wave
view structure
view signals

do {VC709_Gen3x4If128_0.udo}

run -all

quit -force
