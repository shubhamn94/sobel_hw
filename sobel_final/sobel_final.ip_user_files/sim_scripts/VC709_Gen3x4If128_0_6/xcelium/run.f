-makelib xcelium_lib/xil_defaultlib -sv \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_7vx.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_8k.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_16k.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_cpl.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_rep.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_rep_8k.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_bram_7vx_req.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_init_ctrl_7vx.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_lane.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_misc.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_pipe_pipeline.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_top.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_force_adapt.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_clock.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_drp.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_eq.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_rate.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_reset.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_sync.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_user.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pipe_wrapper.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_drp.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_reset.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_qpll_wrapper.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_rxeq_scan.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_wrapper.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_top.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gt_common.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_gtx_cpllpd_ovrd.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_tlp_tph_tbl_7vx.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/source/PCIeGen3x4If128_pcie_3_0_7vx.v" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/PCIeGen3x4If128/ip/PCIeGen3x4If128/sim/PCIeGen3x4If128.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib -sv \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/async_fifo.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/async_fifo_fwft.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/channel_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/counter.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/cross_domain_signal.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/demux.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/engine_layer.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/ff.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo_packer_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo_packer_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/fifo_packer_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/interrupt.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/interrupt_controller.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/mux.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/offset_flag_to_one_hot.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/offset_to_mask.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/one_hot_mux.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/shubham/pci_to_axis_converter.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/pipeline.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/ram_1clk_1w_1r.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/ram_2clk_1w_1r.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/recv_credit_flow_ctrl.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/register.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/registers.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reorder_queue.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reorder_queue_input.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reorder_queue_output.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reset_controller.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/reset_extender.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/riffa.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/fpga/xilinx/vc709/riffa_wrapper_vc709.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rotate.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_engine_classic.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_engine_ultrascale.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_channel_gate.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_reader.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rx_port_requester_mux.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxc_engine_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxc_engine_classic.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxc_engine_ultrascale.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxr_engine_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxr_engine_classic.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/rxr_engine_ultrascale.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/scsdpram.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_reader_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_reader_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_reader_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sg_list_requester.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/shiftreg.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/sync_fifo.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/syncff.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_alignment_pipeline.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_data_fifo.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_data_pipeline.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_data_shift.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine_classic.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine_selector.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_engine_ultrascale.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_hdr_fifo.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_multiplexer_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_buffer_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_buffer_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_buffer_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_channel_gate_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_channel_gate_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_channel_gate_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_monitor_128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_monitor_32.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_monitor_64.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/tx_port_writer.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txc_engine_classic.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txc_engine_ultrascale.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txr_engine_classic.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/riffa_hdl/txr_engine_ultrascale.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sources_1/imports/fpga/xilinx/vc709/VC709_Gen3x4If128/hdl/VC709_Gen3x4If128.sv" \
  "../../../../../sobel_integ_design/VC709_Gen3x4If128_0_6/sim/VC709_Gen3x4If128_0.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

