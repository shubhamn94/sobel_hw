`timescale 1ns/1ns

module pci_to_axis_converter #(
    parameter C_PCI_DATA_WIDTH = 9'd128
)
    (
        input                        CLK                ,
        input                        RST                ,

        output                       CHNL_RX_CLK        ,
        input                        CHNL_RX            ,
        output                       CHNL_RX_ACK        ,
        input                        CHNL_RX_LAST       ,
        input [31:0]                 CHNL_RX_LEN        ,
        input [30:0]                 CHNL_RX_OFF        ,
        input [C_PCI_DATA_WIDTH-1:0] CHNL_RX_DATA       ,
        input                        CHNL_RX_DATA_VALID ,
        output                       CHNL_RX_DATA_REN   ,

        output                       CHNL_TX_CLK        ,
        output                       CHNL_TX            ,
        input                        CHNL_TX_ACK        ,
        output                       CHNL_TX_LAST       ,
        output[31:0]                 CHNL_TX_LEN        ,
        output[30:0]                 CHNL_TX_OFF        ,
        output[C_PCI_DATA_WIDTH-1:0] CHNL_TX_DATA       ,
        output                       CHNL_TX_DATA_VALID ,
        input                        CHNL_TX_DATA_REN   ,

        input[C_PCI_DATA_WIDTH-1:0] S_AXIS_tdata  ,
        output                      S_AXIS_tready ,
        input                       S_AXIS_tvalid ,

        output[C_PCI_DATA_WIDTH-1:0] M_AXIS_tdata  ,
        input                        M_AXIS_tready ,
        output                       M_AXIS_tvalid
    );

    localparam PREP_RX      = 3'd0;
    localparam RECEIVE_CMD  = 3'd1;
    localparam RECEIVE_DATA = 3'd2;
    localparam SEND_DATA    = 3'd3;
    localparam INVALID      = 3'd4;


    reg [31:0] dataCount = 0;
    reg [31:0] wLen      = 0;
    reg [2:0]  rState    = PREP_RX;
    wire[31:0] count     = CHNL_RX_DATA[31:0];

    assign CHNL_RX_CLK        = CLK;
    assign CHNL_RX_ACK        = (rState == RECEIVE_CMD);
    assign CHNL_RX_DATA_REN   = (rState  == RECEIVE_CMD) | (rState == RECEIVE_DATA && M_AXIS_tready);

    assign CHNL_TX_CLK        = CLK;
    assign CHNL_TX            = (rState == SEND_DATA);
    assign CHNL_TX_LAST       = 1'd1;
    assign CHNL_TX_LEN        = wLen; //in words
    assign CHNL_TX_OFF        = 0;
    assign CHNL_TX_DATA       = S_AXIS_tdata;
    assign CHNL_TX_DATA_VALID = (rState == SEND_DATA) && S_AXIS_tvalid;

    assign M_AXIS_tdata       = CHNL_RX_DATA;
    assign M_AXIS_tvalid      = CHNL_RX_DATA_VALID && (rState == RECEIVE_DATA);
    assign S_AXIS_tready      = CHNL_TX_DATA_REN && (rState == SEND_DATA);
    always @(posedge CLK or posedge RST) begin
        if (RST) begin
            dataCount <= 0;
            rState    <= PREP_RX;
            wLen      <= 0;
        end
        else begin
            case (rState)
                PREP_RX: begin // Wait for start of Rx, next state to receive command
                    if (CHNL_RX) begin
                        rState    <= RECEIVE_CMD;
                        dataCount <= 0;
                        wLen      <= 0;
                    end
                end

                RECEIVE_CMD: begin // receive command
                    if (CHNL_RX_DATA_VALID) begin
                        dataCount   <= count;
                        wLen        <= count;
                        if(count > 0) begin //else command is zero rd/wr. Hence stay for next cmd
                            rState <=  CHNL_RX_DATA[32] ? RECEIVE_DATA : SEND_DATA; //bit 32 high => PC to FPGA transfer
                        end
                        else begin
                            rState <= INVALID;
                        end
                    end
                end
                RECEIVE_DATA: begin //Rx
                    if (M_AXIS_tvalid && M_AXIS_tready) begin
                        dataCount <= dataCount - 4;       
                        if(dataCount == 4) begin //initial dataCount always > 0
                            rState <= PREP_RX; 
                        end
                    end
                end
                SEND_DATA: begin //Tx
                    if (S_AXIS_tready && S_AXIS_tvalid) begin
                        dataCount <= dataCount - 4;
                        if(dataCount == 4) begin //initial dataCount always > 0
                            rState <= PREP_RX;
                        end
                    end
                end
            endcase
        end
    end



endmodule
