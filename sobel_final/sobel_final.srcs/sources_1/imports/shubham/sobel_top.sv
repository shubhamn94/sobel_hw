`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.12.2019 01:08:27
// Design Name: 
// Module Name: sobel_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sobel_top
#(// Number of RIFFA Channels
        parameter C_NUM_CHNL = 2,
        // Number of PCIe Lanes
        parameter C_NUM_LANES =  4,
        // Settings from Vivado IP Generator
        parameter C_PCI_DATA_WIDTH = 128,
        parameter C_MAX_PAYLOAD_BYTES = 256,
        parameter C_LOG_NUM_TAGS = 5,
        parameter SOBEL_TDATA_WIDTH = 16
  ) 
  ( output [(C_NUM_LANES - 1) : 0] PCI_EXP_TXP_top,
    output [(C_NUM_LANES - 1) : 0] PCI_EXP_TXN_top,
    input [(C_NUM_LANES - 1) : 0] PCI_EXP_RXP_top,
    input [(C_NUM_LANES - 1) : 0] PCI_EXP_RXN_top,    
    output  [7:0] LED_top,
    input   PCIE_REFCLK_P_top,
    input   PCIE_REFCLK_N_top,
    input   PCIE_RESET_N_top);
       
reg    [C_PCI_DATA_WIDTH-1 : 0] M_AXIS_tdata_dc_1_to_S_AXIS_tdata_pcie;
wire   M_AXIS_tready_dc_1_to_S_AXIS_tready_pcie;
wire   M_AXIS_tvalid_dc_1_to_S_AXIS_tvalid_pcie;

reg    [C_PCI_DATA_WIDTH-1 : 0] M_AXIS_tdata_pcie_to_S_AXIS_tdata_dc_0;
wire   M_AXIS_tready_pcie_to_S_AXIS_tready_dc_0;
wire   M_AXIS_tvalid_pcie_to_S_AXIS_tvalid_dc_0;

reg    [SOBEL_TDATA_WIDTH-1 : 0] OUTPUT_STREAM_V_TDATA_to_S_AXIS_tdata_dc_1;
wire   OUTPUT_STREAM_V_TVALID_to_S_AXIS_tvalid_dc_1; 
wire   OUTPUT_STREAM_V_TREADY_to_S_AXIS_tready_dc_1;

reg    [SOBEL_TDATA_WIDTH-1 : 0] M_AXIS_tdata_dc_0_to_INPUT_STREAM_V_TDATA;
wire   M_AXIS_tvalid_dc_0_to_INPUT_STREAM_V_TVALID; 
wire   M_AXIS_tready_dc_0_to_INPUT_STREAM_V_TREADY;

reg   rst_out_riffa_pcie;
wire   clk_out_riffa_pcie;
                
VC709_Gen3x4If128 VC709_Gen3x4If128_inst (
    .S_AXIS_tdata (M_AXIS_tdata_dc_1_to_S_AXIS_tdata_pcie),
    .S_AXIS_tready (M_AXIS_tready_dc_1_to_S_AXIS_tready_pcie),
    .S_AXIS_tvalid (M_AXIS_tvalid_dc_1_to_S_AXIS_tvalid_pcie),
    .M_AXIS_tdata (M_AXIS_tdata_pcie_to_S_AXIS_tdata_dc_0),
    .M_AXIS_tready (M_AXIS_tready_pcie_to_S_AXIS_tready_dc_0),
    .M_AXIS_tvalid (M_AXIS_tvalid_pcie_to_S_AXIS_tvalid_dc_0),
    .rst_out (rst_out_riffa_pcie),
    .clk_out (clk_out_riffa_pcie),
    .PCI_EXP_TXP     (PCI_EXP_TXP_top),
    .PCI_EXP_TXN     (PCI_EXP_TXN_top),
    .PCI_EXP_RXP     (PCI_EXP_RXP_top),
    .PCI_EXP_RXN     (PCI_EXP_RXN_top),
    .LED             (LED_top),
    .PCIE_REFCLK_P   (PCIE_REFCLK_P_top),
    .PCIE_REFCLK_N   (PCIE_REFCLK_N_top),
    .PCIE_RESET_N    (PCIE_RESET_N_top)
 );
 
 sobel_filter_0 sobel_filter_inst ( 
    .INPUT_STREAM_V_V_TDATA (M_AXIS_tdata_dc_0_to_INPUT_STREAM_V_TDATA),
    .INPUT_STREAM_V_V_TVALID (M_AXIS_tvalid_dc_0_to_INPUT_STREAM_V_TVALID),
    .INPUT_STREAM_V_V_TREADY (M_AXIS_tready_dc_0_to_INPUT_STREAM_V_TREADY),
    .OUTPUT_STREAM_V_V_TDATA (OUTPUT_STREAM_V_TDATA_to_S_AXIS_tdata_dc_1),
    .OUTPUT_STREAM_V_V_TVALID (OUTPUT_STREAM_V_TVALID_to_S_AXIS_tvalid_dc_1),
    .OUTPUT_STREAM_V_V_TREADY (OUTPUT_STREAM_V_TREADY_to_S_AXIS_tready_dc_1),
    .ap_clk          (clk_out_riffa_pcie),
    .ap_rst_n        (~rst_out_riffa_pcie),
    .ap_start        (1)
 );
 
 axis_dwidth_converter_0 axis_dwidth_converter_0_inst (
    .aclk (clk_out_riffa_pcie),
    .aresetn (~rst_out_riffa_pcie),
    .s_axis_tvalid (M_AXIS_tvalid_pcie_to_S_AXIS_tvalid_dc_0),
    .s_axis_tready (M_AXIS_tready_pcie_to_S_AXIS_tready_dc_0),
    .s_axis_tdata (M_AXIS_tdata_pcie_to_S_AXIS_tdata_dc_0),
    .m_axis_tvalid (M_AXIS_tvalid_dc_0_to_INPUT_STREAM_V_TVALID),
    .m_axis_tready (M_AXIS_tready_dc_0_to_INPUT_STREAM_V_TREADY),
    .m_axis_tdata (M_AXIS_tdata_dc_0_to_INPUT_STREAM_V_TDATA)
 ); 
 
  axis_dwidth_converter_1 axis_dwidth_converter_1_inst (
     .aclk (clk_out_riffa_pcie), 
     .aresetn (~rst_out_riffa_pcie),
     .s_axis_tvalid (OUTPUT_STREAM_V_TVALID_to_S_AXIS_tvalid_dc_1),
     .s_axis_tready (OUTPUT_STREAM_V_TREADY_to_S_AXIS_tready_dc_1),
     .s_axis_tdata (OUTPUT_STREAM_V_TDATA_to_S_AXIS_tdata_dc_1),
     .m_axis_tvalid (M_AXIS_tvalid_dc_1_to_S_AXIS_tvalid_pcie),
     .m_axis_tready (M_AXIS_tready_dc_1_to_S_AXIS_tready_pcie),
     .m_axis_tdata (M_AXIS_tdata_dc_1_to_S_AXIS_tdata_pcie)
 ); 

endmodule
